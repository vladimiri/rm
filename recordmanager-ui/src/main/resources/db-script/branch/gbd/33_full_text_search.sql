CREATE OR REPLACE FUNCTION record_info_trigger()
  RETURNS trigger AS
$BODY$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.pts,'')),'A' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.pts),'')),'C' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.vehicle_no),'')),'C' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.vehicle_name),'')),'B' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.doc_no),'')),'A' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.doc_no,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.pts,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.doc_no,E'\\s*'),' '),'')),'D' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.pts,E'\\s*'),' '),'')),'D' );
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;