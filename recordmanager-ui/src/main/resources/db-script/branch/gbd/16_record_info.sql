alter table record_info add column trialdate character varying(50);

alter table record_info add column theorydate varchar(20);


CREATE OR REPLACE FUNCTION record_info_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.pts,'')),'A' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.pts),'')),'B' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.vehicle_no),'')),'B' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.vehicle_name),'')),'C' ) ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.doc_no),'')),'B' );
  return new;
end
$$;
/

