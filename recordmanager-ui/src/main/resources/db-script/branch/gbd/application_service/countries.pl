open F,'countries.csv';
while($line = <F>){
	my ($id,$code,undef,$isCountry,$name,) = split "\t",$line;
	$i++;
	if($isCountry eq 'true') {
		print <<EOF;
	insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'COUNTRY','','$name',$i,'$code');
EOF
	} else {
		print <<EOF;
	insert into generic_code (id,gen_type,name,gen_desc,sorter,external_id ) values (nextval('object_seq'),'REGION','','$name',$i,'$code');
EOF
	}
}