
COMMENT ON TABLE record_info IS '������';

drop table record_info;

CREATE TABLE record_info (
    id integer NOT NULL,
    pts character varying(20),
    vehicle_no character varying(10),
    vehicle_name character varying(50),
    doc_no varchar(100),
    tsv tsvector
);


ALTER TABLE record_info
    ADD CONSTRAINT record_info_pkey PRIMARY KEY (id);

CREATE INDEX record_info_1ix
   ON record_info (pts);

update record_info i set doc_no = (select r.doc_no from record r where r.id = i.id);


