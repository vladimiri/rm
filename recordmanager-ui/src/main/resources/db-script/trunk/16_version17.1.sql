alter table actions drop column custom_validator;

alter table actions drop column custom_validator_parameter;

alter table actions drop column custom_validator_error;

create table validator (
	id integer,
	name varchar(1024),
	clazz varchar(128),
	parameter varchar(2048),
	error varchar(2048)
);

create table action_validator (
	validator_id integer,
	action_id integer,
	sorter integer
);
