
CREATE TABLE action_object_type (
    action_id integer,
    object_type_id integer,
	sorter integer
);


CREATE TABLE actions (
    id integer NOT NULL,
    name character varying(100),
    security integer DEFAULT 1
);


COMMENT ON TABLE actions IS '������';


CREATE TABLE config (
    name character varying(100) NOT NULL,
    value character varying(2048),
    id integer
);


COMMENT ON TABLE config IS '���������';

CREATE TABLE customers (
    id integer NOT NULL,
    name character varying(40)
);


COMMENT ON TABLE customers IS '��� �����������';

CREATE TABLE duration (
    start_date timestamp without time zone,
    end_date timestamp without time zone,
    action_id integer DEFAULT 0,
    criteria_id integer DEFAULT 0,
    object_type character(1),
    object_id integer DEFAULT 0,
    min integer,
    id integer NOT NULL,
    customer_id integer
);

COMMENT ON TABLE duration IS '����������������� ������������';


CREATE TABLE groups (
    login character varying(15) NOT NULL,
    role character varying(15) NOT NULL
);


COMMENT ON TABLE groups IS '������ �������������';


CREATE TABLE object_num (
    object_id integer NOT NULL,
    num integer NOT NULL
);


CREATE SEQUENCE object_seq
    INCREMENT BY 1
    NO MAXVALUE
    MINVALUE 1
    CACHE 1;


CREATE TABLE object (
    id integer NOT NULL,
    name character varying(60),
    type character(1),
    office_id integer
);


COMMENT ON TABLE object IS '������������ ��������';

CREATE TABLE office (
    id integer NOT NULL,
    name character varying(100),
    schedule character varying(300),
    security integer DEFAULT 1,
    time_diff integer DEFAULT 20,
    okato character varying(11)
);

COMMENT ON TABLE office IS '����� ������ �������������';

CREATE SEQUENCE office_seq
    START WITH 10
    INCREMENT BY 1
    NO MAXVALUE
    MINVALUE 10
    CACHE 1;


CREATE TABLE priority (
    office_id integer,
    priority integer,
    id integer NOT NULL,
    action_id integer
);


CREATE TABLE record (
    object_id integer,
    day date,
    hour integer,
    start_time integer DEFAULT 0,
    end_time integer DEFAULT 0,
    id integer NOT NULL,
    action_id integer,
    customer_id integer,
    criteria_id integer,
    region_id integer,
    name character varying(120),
    doc_no character varying(22),
    email character varying(50),
    phone character varying(20),
    key character varying(20),
    status character varying(70),
    author character varying(60),
    creation_date timestamp without time zone,
    update_date timestamp without time zone,
    date_of_birth character(10),
    tsv tsvector
);


COMMENT ON TABLE record IS '�������� �������';


CREATE SEQUENCE record_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


CREATE TABLE region_office (
    id integer NOT NULL,
    regions character varying(300),
    office_id integer NOT NULL,
    security integer DEFAULT 0,
    customer_id integer DEFAULT 0,
    default_office_id integer default 0,
    actions character varying(30)
);



CREATE TABLE regions (
    id integer NOT NULL,
    name character varying(40)
);


COMMENT ON TABLE regions IS '������';


CREATE TABLE schedule (
    start_date date,
    end_date date,
    object_id integer DEFAULT 0,
    customer_id integer DEFAULT 0,
    security integer DEFAULT 0,
    id integer NOT NULL,
    schedule character varying(1000),
    criterias character varying(30),
    actions character varying(30)
);


COMMENT ON TABLE schedule IS '����������';


CREATE TABLE staff (
    code integer NOT NULL,
    name character varying(60),
    "position" character varying(100),
    level character varying(100)
);


COMMENT ON TABLE staff IS '���������';

CREATE TABLE users (
    login character varying(15) NOT NULL,
    pass character varying(60) NOT NULL,
    description character varying(255)
);


COMMENT ON TABLE users IS '������������';

CREATE TABLE criteria (
    id integer NOT NULL,
    name character varying(100),
    security integer
);


COMMENT ON TABLE criteria IS '��������';


CREATE TABLE weekend (
    start_date date NOT NULL,
    object_list character varying(1000) NOT NULL,
    schedule character varying(1000),
    id integer NOT NULL,
    end_date date NOT NULL
);


ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_pkey PRIMARY KEY (id);


ALTER TABLE ONLY config
    ADD CONSTRAINT config_pk PRIMARY KEY (name);


ALTER TABLE ONLY customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


ALTER TABLE ONLY duration
    ADD CONSTRAINT duration_pkey PRIMARY KEY (id);

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (login, role);


ALTER TABLE ONLY object_num
    ADD CONSTRAINT object_num_pk PRIMARY KEY (object_id, num);


ALTER TABLE ONLY object
    ADD CONSTRAINT object_pkey PRIMARY KEY (id);


ALTER TABLE ONLY office
    ADD CONSTRAINT office_pkey PRIMARY KEY (id);


ALTER TABLE ONLY record
    ADD CONSTRAINT record3_uq UNIQUE (day, object_id, hour, start_time, status);

ALTER TABLE ONLY config
    ADD CONSTRAINT config_uq UNIQUE (id);

ALTER TABLE ONLY record
    ADD CONSTRAINT record_pkey PRIMARY KEY (id);


ALTER TABLE ONLY region_office
    ADD CONSTRAINT region_office_pk PRIMARY KEY (id);


ALTER TABLE ONLY regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


ALTER TABLE ONLY schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (id);


ALTER TABLE ONLY staff
    ADD CONSTRAINT staff_pk PRIMARY KEY (code);


ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (login);


ALTER TABLE ONLY criteria
    ADD CONSTRAINT criteria_pk PRIMARY KEY (id);


ALTER TABLE ONLY weekend
    ADD CONSTRAINT weekend_pk PRIMARY KEY (id);



INSERT INTO config (name, value, id) VALUES ('intervalToPreventEarlyRestore', '40', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('formatDate', 'yyyy-MM-dd', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('mail.from.email', 'gibdd@gibdd.yar.ru', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('reportListDays', '20', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('freeSpaceFormatDate', 'yyyy.MM.dd EEEE', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('mainOfficeId', '1', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('display.threshold', '60', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('daysToRegister2', '20', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('daysToRegister1', '17', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('mail.smtp.host', 'smtp.yar.ru', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('template.key', 'habahaba', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('line.operator.filename', '/home/rm/operator.ini', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('mail.subject', '������������� ����������� �����������.', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('office.1.route', NULL, nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('operator.prefix', 'Monitor', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('currentDayRecord', 'true', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('display.picture.prefix', '/img/', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('differenceBetweenRecords', '30', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('daysToRegisterCheckFree', '12', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('mail.from.personel', '�����������', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('yearsToSelect', '100', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('freeSpaceFormatTime', 'HH:mm', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('freeSpaceFormat', 'yyyy.MM.dd EEEE HH:mm', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('yearsToAbility', '0', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('display.picture.folder', '/home/rm/webapps/img', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('template.text', '������������� �����������

${label.customer}:\\t\\t${customer}
${label.region}:\\t\\t${region}
${label.action}:\\t\\t${action}
${label.name}:\\t\\t${name}
${label.documentNum}:\\t\\t${documentNum}
${label.email}:\\t\\t${email}
${label.phone}:\\t\\t${phone}

<!-- $BeginBlock record -->
������ ����� ${office}, ������ ${object}, ����� ${space} ������������� ������ ${key}
<!-- $EndBlock record -->

���������� �� ������������� ����� �������.', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('display.backThreshold', '10', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('display.refresh', '60', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('title', '���������', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('keywords', '�������� �����', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('description', '��������', nextval('object_seq'));
INSERT INTO config (name, value, id) VALUES ('googleAccount', 'UA-21659488-', nextval('object_seq'));


INSERT INTO customers (id, name) VALUES (1, '���������� ����');
INSERT INTO customers (id, name) VALUES (2, '����������� ����');

CREATE TABLE object_type (
 id integer,
 "type" character(1),
 "name" character varying(200)
);

ALTER TABLE object_type ADD CONSTRAINT object_type_pk PRIMARY KEY (id);

insert into users values ('rm',md5('rm'),'Default admin');

insert into groups values ('rm','rm-admin');
insert into groups values ('rm','rm-oper');
insert into groups values ('rm','manager');

insert into role (id,name,description) values (nextval('object_seq'),'rm-admin','');
insert into role (id,name,description) values (nextval('object_seq'),'rm-oper','');
insert into role (id,name,description) values (nextval('object_seq'),'manager','');
