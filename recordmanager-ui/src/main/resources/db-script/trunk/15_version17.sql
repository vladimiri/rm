alter table actions add column description varchar(2048);

alter table actions add column final varchar(2048);

alter table actions add column warning varchar(2048);

alter table actions add column custom_validator varchar(2048);

alter table actions add column custom_validator_parameter varchar(2048);

alter table actions add column custom_validator_error varchar(2048);

alter table actions add column queue_length integer;

create table object_relation (
	parent_id integer,
	child_id integer,
	sorter integer
);

create table site (
	id integer,
	name varchar(1024),
	intro varchar(2048),
	info varchar(2048),
	description varchar(2048),
	http_keywords varchar(2048),
	http_description varchar(2048),
	http_title varchar(2048),
	http_template varchar(2048),
	final_text varchar(2048)
);

create table site_action (
	site_id integer,
	action_id integer,
	sorter integer
);

create table site_customer (
	site_id integer,
	customer_id integer,
	sorter integer
);

create table site_region (
	site_id integer,
	region_id integer,
	sorter integer
);

create table site_office (
	site_id integer,
	office_id integer,
	sorter integer
);

