CREATE TABLE generic_code (
    id integer,
    gen_type varchar(100),
    name varchar(200),
    gen_desc varchar(2048)
);

ALTER TABLE generic_code
    ADD CONSTRAINT generic_code_pkey PRIMARY KEY (id);
