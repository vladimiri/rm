CREATE TABLE record_info (
    id integer NOT NULL
);

ALTER TABLE record_info
    ADD CONSTRAINT record_info_pkey PRIMARY KEY (id);
    

CREATE INDEX record_lower_name_idx ON "record" (lower(name));
