CREATE OR REPLACE FUNCTION record_trigger()
  RETURNS trigger AS
$BODY$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.name, '')) , 'A') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.phone, '')) , 'B') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.email, '')) , 'B');
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;