alter table object add column type_id integer;

update object o set type_id = (select id from object_type t where t.type = o.type);

update duration o set type_id = (select id from object_type t where t.type = o.object_type);

alter table record_map_info add column     tsv tsvector;


CREATE INDEX record_map_info_ix
  ON record_map_info
  USING btree
  (id);


-- Trigger: tsvectorupdate_record_map_info on record_map_info

-- DROP TRIGGER tsvectorupdate_record_map_info ON record_map_info;

CREATE TRIGGER tsvectorupdate_record_map_info
  BEFORE INSERT OR UPDATE
  ON record_map_info
  FOR EACH ROW
  EXECUTE PROCEDURE record_map_info_trigger();

REATE OR REPLACE FUNCTION record_map_info_trigger()
  RETURNS trigger AS
$BODY$
begin
	IF new.name = 'pts' THEN
  		new.tsv := 	
  					setweight(to_tsvector('pg_catalog.russian', coalesce(new.value,'')),'A' ) ||
  					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.value,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.value,E'\\s*'),' '),'')),'D' );
	ELSIF  new.name = 'vehicle_no' THEN
		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'C' );
	ELSIF  new.name = 'vehicle_name' THEN
		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'B' );
	ELSIF  new.name = 'doc_no' THEN
  		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'A' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.value,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.value,E'\\s*'),' '),'')),'D' );
	ELSE
  		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'B' );
	
  	END IF;
	return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

  
CREATE OR REPLACE FUNCTION tsextcat(acc tsvector, instr tsvector)
  RETURNS tsvector AS
$BODY$
  BEGIN
      RETURN acc || instr;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION record_trigger()
  RETURNS trigger AS
$BODY$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.name, '')) , 'A') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.phone, '')) , 'B') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.email, '')) , 'B');
  return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
CREATE AGGREGATE textcat_all(tsvector) (
  SFUNC=tsextcat,
  STYPE=tsvector,
  INITCOND='to_tsvector'
);

update schedule set customer_id = null where customer_id = 0;


update duration set customer_id = null where customer_id = 0;

update duration set object_id = null where object_id = 0;

update duration set criteria_id = null where criteria_id = 0;

update duration set action_id = null where action_id = 0;
  
  