CREATE TABLE dlr (
    smsc character varying(40),
    ts character varying(40),
    destination character varying(40),
    source character varying(40),
    service character varying(40),
    url character varying(255),
    mask numeric,
    status numeric,
    boxc character varying(255)
);

--
-- Name: send_sms; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE send_sms
(
  sql_id serial NOT NULL,
  momt character varying(3) DEFAULT NULL::character varying,
  sender character varying(20),
  receiver character varying(20),
  udhdata character varying(255),
  msgdata text,
  "time" bigint,
  smsc_id character varying(255),
  service character varying(255),
  account character varying(255),
  id bigint,
  sms_type bigint,
  mclass bigint,
  mwi bigint,
  coding bigint,
  compress bigint,
  validity bigint,
  deferred bigint,
  dlr_mask bigint,
  dlr_url character varying(255),
  pid bigint,
  alt_dcs bigint,
  rpi bigint,
  charset character varying(255),
  boxc_id character varying(255),
  binfo character varying(255),
  meta_data text,
  CONSTRAINT send_sms_pkey PRIMARY KEY (sql_id),
  CONSTRAINT send_sms_momt_check CHECK (momt::text = ANY (ARRAY['MO'::character varying, 'MT'::character varying, NULL::character varying]::text[]))
);

--
-- Name: sent_sms; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sent_sms (
    sql_id integer NOT NULL,
    momt character varying(5) DEFAULT 'NULL'::character varying,
    sender character varying(20),
    receiver character varying(20),
    udhdata character varying(255),
    msgdata character varying(255),
    "time" bigint,
    smsc_id character varying(255),
    service character varying(255),
    account character varying(255),
    id bigint,
    sms_type bigint,
    mclass bigint,
    mwi bigint,
    coding bigint,
    compress bigint,
    validity bigint,
    deferred bigint,
    dlr_mask bigint,
    dlr_url character varying(255),
    pid bigint,
    alt_dcs bigint,
    rpi bigint,
    charset character varying(255),
    boxc_id character varying(255),
    binfo character varying(255),
    meta_data text,
    CONSTRAINT sent_sms_momt_check CHECK (((momt)::text = ANY ((ARRAY['MO'::character varying, 'MT'::character varying, 'NULL'::character varying])::text[])))
);



CREATE SEQUENCE sent_sms_sql_id_seq
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;



ALTER SEQUENCE sent_sms_sql_id_seq OWNED BY sent_sms.sql_id;

ALTER TABLE ONLY send_sms ALTER COLUMN sql_id SET DEFAULT nextval('send_sms_sql_id_seq'::regclass);

ALTER TABLE ONLY sent_sms ALTER COLUMN sql_id SET DEFAULT nextval('sent_sms_sql_id_seq'::regclass);

