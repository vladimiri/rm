insert into config (id,name,value) values (nextval('object_seq'),'template.sms.key','smarttemplate');

insert into config (id,name,value) values (nextval('object_seq'),'template.sms.text','');

insert into config (id,name,value) values (nextval('object_seq'),'sms.type','2'); 
-- possible values is 1 - for translit, 2 - for UTF 
insert into config (id,name,value) values (nextval('object_seq'),'sms.coding','2');
-- smsbox instance name for example - smarts.
insert into config (id,name,value) values (nextval('object_seq'),'sms.smsc_id',''); 
-- MOMT
insert into config (id,name,value) values (nextval('object_seq'),'sms.momt','MT');
-- 
insert into config (id,name,value) values (nextval('object_seq'),'sms.charset','');
-- 
insert into config (id,name,value) values (nextval('object_seq'),'sms.sender','');