CREATE OR REPLACE FUNCTION split_border(character varying)
  RETURNS character varying AS
$BODY$ select regexp_replace(regexp_replace($1, E'(\\d)(\\D)', E'\\1 \\2' , 'g'),E'(\\D)(\\d)',E'\\1 \\2','g') $BODY$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
  
CREATE OR REPLACE FUNCTION split_border_half(character varying)
  RETURNS character varying AS
$BODY$ select regexp_replace($1, E'(\\D)(\\d)', E'\\1 \\2' , 'g') $BODY$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
  
CREATE OR REPLACE FUNCTION record_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.tsv :=
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.name, '')) , 'A') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.phone, '')) , 'B') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.email, '')) , 'B') ||
     setweight(to_tsvector('pg_catalog.russian', coalesce(new.key, '')) , 'C');
  return new;
end
$$;


CREATE FUNCTION record_info_trigger() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
  new.tsv := null;
  return new;
end
$$;

CREATE TRIGGER tsvectorupdate_record
    BEFORE INSERT OR UPDATE ON record
    FOR EACH ROW
    EXECUTE PROCEDURE record_trigger();

CREATE TRIGGER tsvectorupdate_record_info
    BEFORE INSERT OR UPDATE ON record_info
    FOR EACH ROW
    EXECUTE PROCEDURE record_info_trigger();


update record set phone = phone;

update record_info set id = id;