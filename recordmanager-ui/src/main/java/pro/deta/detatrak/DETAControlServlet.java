package pro.deta.detatrak;

import java.io.IOException;
import java.io.PrintStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pro.deta.detatrak.validator.DynamicBlockValidator;

@WebServlet(urlPatterns = "/control")
public class DETAControlServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -623401601477809897L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/plain");
		String command = req.getParameter("command");
			switch (command ) {
			case "evictAll":
				JPAFilter.evictAll();
				resp.getOutputStream().print("evictAll - OK");
				break;
			case "dynamicBlockCleanUp":
				DynamicBlockValidator.cleanup();
				resp.getOutputStream().print("dynamicBlockCleanUp - OK");
				break;
			case "dynamicBlockStatus":
				resp.getOutputStream().print(DynamicBlockValidator.status());
				resp.getOutputStream().print("dynamicBlockStatus - OK");
				break;
			case "evictType":
				String type = req.getParameter("type");
				Class<?> cl = null;
				try {
					cl = Class.forName(type);
				} catch (ClassNotFoundException e) {
					e.printStackTrace(new PrintStream(resp.getOutputStream()));
					return;
				}
				JPAFilter.evict(cl);
				resp.getOutputStream().print("evictType "+cl.getCanonicalName()+" - OK");
				break;

			default:
				break;
			}
		
		resp.getOutputStream().print("OK");
	}
	
	
}