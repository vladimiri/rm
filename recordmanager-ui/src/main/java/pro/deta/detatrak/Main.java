package pro.deta.detatrak;

import com.google.gson.Gson;

import pro.deta.detatrak.validator.BlockingRule;
import pro.deta.detatrak.validator.DynamicBlockValidatorParameters;

public class Main {

	public static void main(String[] args) {
		DynamicBlockValidatorParameters p = new DynamicBlockValidatorParameters();
		BlockingRule rule = new BlockingRule();
		rule.setDuration(100*1000); // 100 sec
		rule.setInterval(10*1000);
		rule.setLimit(5);
		rule.setSubject("IP");
		BlockingRule rule1 = new BlockingRule();
		rule1.setDuration(100*1000); // 100 sec
		rule1.setInterval(10*1000);
		rule1.setLimit(5);
		rule1.setSubject("IP");
		p.getBlockingList().add(rule);
		p.getBlockingList().add(rule1);
		System.out.print(new Gson().toJson(p));
	}

}
