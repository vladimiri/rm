package pro.deta.detatrak.user.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;

public class CFMRecordForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5327811687379964672L;
	private SiteDO site = null;
	private int siteId;
	private int actionId;
	private int officeId;
	private StoredRecordDO record = null;
	private OfficeDO office = null;
	
	private boolean displayHead = true;

	public SiteDO getSite() {
		return site;
	}
	public void setSite(SiteDO site) {
		this.site = site;
	}
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		actionId = 0;
		record = null;
	}
	public int getOfficeId() {
		return officeId;
	}
	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}
	public StoredRecordDO getRecord() {
		return record;
	}
	public void setRecord(StoredRecordDO record) {
		this.record = record;
	}
	public boolean isDisplayHead() {
		return displayHead;
	}
	public void setDisplayHead(boolean displayHead) {
		this.displayHead = displayHead;
	}
	public OfficeDO getOffice() {
		return office;
	}
	public void setOffice(OfficeDO office) {
		this.office = office;
	}
	
}
