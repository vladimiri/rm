package pro.deta.detatrak.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.user.form.CFMRecordForm;
import pro.deta.detatrak.user.form.CFMStatusForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.OfficeDO;

public class CFMStatusAction  extends Action{
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMStatusForm qf = (CFMStatusForm) form;
		
			boolean res = populateSite(qf,req);
			if(!res) {
				ActionMessages ams = new ActionMessages();
				ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.wrongOfficeId"));
				saveErrors(req, ams);
				return mapping.getInputForward();
			}
		return mapping.findForward("default");
	}

	private boolean populateSite(CFMStatusForm qf, HttpServletRequest req) {
		qf.setSite(UserHelper.getSiteDO(req));
		if(qf.getSite()!= null) {
			qf.getSite().getActions().size();
			for (OfficeDO of : qf.getSite().getOffices()) {
				if(of.getId() == qf.getOfficeId())
					return true;
			}
		}
		// such Office in this Site not found. Process to error.
		qf.setSite(null);
		return false;
//		HDAO dao = new HDAO(req);
//		qf.setObjects(dao.getOfficeObjects(qf.getOfficeId()));
		
	}
}
