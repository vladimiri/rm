package pro.deta.detatrak.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;
import net.tanesha.recaptcha.ReCaptchaResponse;
import nl.captcha.Captcha;
import pro.deta.detatrak.DETAConfig;
import pro.deta.detatrak.DETATRAKFilter;
import ru.yar.vi.rm.user.form.UserForm;


public class CaptchaAction extends Action {
	public static final Logger logger = Logger.getLogger(CaptchaAction.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		String seccode = request.getParameter("recaptcha_response_field");
		if(seccode == null) {
			return mapping.getInputForward();
		}
		if("".equalsIgnoreCase(seccode))
			return getInputCaptchaError(request, mapping);

		//    request.setCharacterEncoding("UTF-8"); // Do this so we can capture non-Latin chars
		if(System.getProperty("detatrak.skip.captcha") == null && !checkReCaptcha(seccode, request) ) {
			logger.error("Captcha does not matched with " + seccode);
			if(request.getSession() != null) {
				Object userForm = request.getSession().getAttribute("UserForm");
				logger.error("Failure while checking captcha " + request.getRemoteAddr() + " for form " + userForm);
			}
			DETATRAKFilter.logRequest(request, response);
			DETATRAKFilter.logAfter(request, response);
			return getInputCaptchaError(request, mapping);
		}
		
		if(form instanceof UserForm) {
			UserForm uf = (UserForm) form;
			uf.captchaSuccess = true;
		}
		return mapping.findForward("success");
	}

	private boolean checkReCaptcha(String seccode, HttpServletRequest request) {
		DETAConfig config = new DETAConfig();
		if("simple".equalsIgnoreCase(config.getCaptchaType())) {
			if(seccode == null)
				return false;
			logger.error("Captcha entered value: " + seccode +" session: " + request.getSession().getId());
			if (!(seccode == null || seccode.equals("null"))) {
				Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
				if(captcha != null)
					logger.error("Captcha stored value: " + captcha.getAnswer() +" session: " + request.getSession().getId());
				else 
					logger.error("Captcha is empty in sesstion");
				if (captcha == null || !captcha.isCorrect(seccode)) {
					logger.error("Captcha stored value: " + captcha.getAnswer() +" session: " + request.getSession().getId());
					return false;
				}
				request.getSession().removeAttribute(Captcha.NAME);
				return true;
			} else {
				logger.error("Captcha does not matched. Reason = 2 " +" session: " + request.getSession().getId());
				return false;
			}
		} else {
			ReCaptcha captcha = ReCaptchaFactory.newReCaptcha("6LepyOsSAAAAAMLaBMW8raKSyeMKi32alHNLHueZ", "6LepyOsSAAAAAOH_RY9FkhvhapDuYs2G9x7q7UFd", false);
			ReCaptchaResponse response = captcha.checkAnswer(request.getRemoteAddr(), request.getParameter("recaptcha_challenge_field"), seccode);
			return response.isValid();
		}
	}

	public static boolean checkCaptcha(String seccode, HttpServletRequest request) {
		if(seccode == null)
			return false;
		logger.error("Captcha entered value: " + seccode);
		if (!(seccode == null || seccode.equals("null"))) {
			Captcha captcha = (Captcha) request.getSession().getAttribute(Captcha.NAME);
			if(captcha != null)
				logger.error("Captcha stored value: " + captcha.getAnswer() +" session: " + request.getSession().getId());
			else 
				logger.error("Captcha is empty in sesstion");
			if (captcha == null || !captcha.isCorrect(seccode))
				return false;
			request.getSession().removeAttribute(Captcha.NAME);
			return true;
		} else {
			logger.error("Captcha does not matched. Reason = 2");
			return false;
		}
	}

	public ActionForward getInputCaptchaError(HttpServletRequest request,ActionMapping mapping) {
		ActionMessages ams = new ActionMessages();
		ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_CAPTCHA"));
		saveErrors(request, ams);
		return mapping.getInputForward();
	}
}
