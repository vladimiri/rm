package pro.deta.detatrak.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.user.form.AdvReportForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.ComplexReportDO;

public class AdvReportAction  extends Action{
	private static final Logger logger = Logger.getLogger(AdvReportAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		AdvReportForm qf = (AdvReportForm) form;

		populateSite(qf,req);
		if(qf.getReport() == null) {
			logger.error("Report is null " + qf.getReportId());
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.wrongReport"));
			saveErrors(req, ams);
			return mapping.getInputForward();
		}
		
		if(qf.getForward() != null) {
			return mapping.findForward(qf.getForward());
		}
		return mapping.findForward("default");
	}

	private void populateSite(AdvReportForm qf, HttpServletRequest req) {
		if(qf.getReportId()> 0) {
			ComplexReportDO report = UserHelper.getHDAO(req).find(ComplexReportDO.class, qf.getReportId());
			qf.setReport(report);
		}
	}
}
