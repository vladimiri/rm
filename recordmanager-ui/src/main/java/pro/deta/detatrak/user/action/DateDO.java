package pro.deta.detatrak.user.action;

import java.util.Date;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.BaseDO;

public class DateDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2429820662466295527L;
	private Date date;
	
	public DateDO(int id,Date date) {
		this.setId(id);
		this.date = date;
		this.setName(UserHelper.formatDateOnly(date));
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
