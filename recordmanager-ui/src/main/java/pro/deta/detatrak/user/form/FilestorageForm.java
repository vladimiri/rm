package pro.deta.detatrak.user.form;

import org.apache.struts.action.ActionForm;

public class FilestorageForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 358338462525345514L;
	private int fileId;
	private String filePath;
	private String contentType;
	
	
	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
}
