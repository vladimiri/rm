package pro.deta.detatrak.user.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.GenCodeDO;

public class GenericCodeForm extends ActionForm  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7753207069705827532L;
	private String genType;
	private String genCode;
	private String arrayName;
	private List<GenCodeDO> list = null;
	
	public String getGenType() {
		return genType;
	}
	public void setGenType(String genType) {
		this.genType = genType;
	}
	public String getGenCode() {
		return genCode;
	}
	public void setGenCode(String genCode) {
		this.genCode = genCode;
	}
	public String getArrayName() {
		return arrayName;
	}
	public void setArrayName(String arrayName) {
		this.arrayName = arrayName;
	}
	public List<GenCodeDO> getList() {
		return list;
	}
	public void setList(List<GenCodeDO> list) {
		this.list = list;
	}
	
	public String getName() {
		if(arrayName != null && !"".equalsIgnoreCase(arrayName))
			return arrayName;
		else 
			return genType;
	}

}
