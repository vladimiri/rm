package pro.deta.detatrak.user.form;

import java.util.Date;
import java.util.List;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.DisplayRecordDO;

public class CFMObjectForm  extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8635696529432923376L;
	private int objectId;
	private int limit = 5;
	private Date date;
	private DisplayRecordDO displayRecord = new DisplayRecordDO();
	private List<String> pictures = null;
	private int random;
	private String displayParam = "name"; // Отображать параметр записи, по умолчанию name; 
	private int limitParam = 0; // Обрезать значение параметра до необходимого размера
	private boolean toUpperCase=true; // Переводить значение в верхний регистр
	private int hour = 0;
	private int min=0;
	private int mon=0;
	private int day=0;
	private int year=0;
	
	private int threshold = 60;
	private int backThreshold;
	
	
	public int getObjectId() {
		return objectId;
	}
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<String> getPictures() {
		return pictures;
	}
	public void setPictures(List<String> pictures) {
		this.pictures = pictures;
	}
	public int getRandom() {
		return random;
	}
	public void setRandom(int random) {
		this.random = random;
	}
	public String getDisplayParam() {
		return displayParam;
	}
	public void setDisplayParam(String displayParam) {
		this.displayParam = displayParam;
	}
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public int getMin() {
		return min;
	}
	public void setMin(int min) {
		this.min = min;
	}
	public int getMon() {
		return mon;
	}
	public void setMon(int mon) {
		this.mon = mon;
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getThreshold() {
		return threshold;
	}
	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}
	public int getBackThreshold() {
		return backThreshold;
	}
	public void setBackThreshold(int backThreshold) {
		this.backThreshold = backThreshold;
	}
	public int getLimitParam() {
		return limitParam;
	}
	public void setLimitParam(int limitParam) {
		this.limitParam = limitParam;
	}
	public boolean isToUpperCase() {
		return toUpperCase;
	}
	public void setToUpperCase(boolean toUpperCase) {
		this.toUpperCase = toUpperCase;
	}
	public DisplayRecordDO getDisplayRecord() {
		return displayRecord;
	}
	public void setDisplayRecord(DisplayRecordDO displayRecord) {
		this.displayRecord = displayRecord;
	}
	public String getRandomPicture() {
		return getPictures().get(getRandom());
	}
}
