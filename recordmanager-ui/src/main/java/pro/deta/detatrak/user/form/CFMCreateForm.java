package pro.deta.detatrak.user.form;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import freemarker.template.Configuration;
import freemarker.template.Template;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.StoredRecordDO;

public class CFMCreateForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1168012828298891942L;
	private int actionId;
	private int officeId;
	private StoredRecordDO record;
	private int queueSize;
	private int position;
	private Date currentDate;
	private Template template;
	private boolean debug = false;

	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public Template getTemplate() {
		return template;
	}
	public void setTemplate(Template template) {
		this.template = template;
	}
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public int getOfficeId() {
		return officeId;
	}
	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}
	public StoredRecordDO getRecord() {
		return record;
	}
	public void setRecord(StoredRecordDO record) {
		this.record = record;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// TODO Auto-generated method stub
		super.reset(mapping, request);
		actionId=0;
		officeId=0;
		record=null;
	}
	public int getQueueSize() {
		return queueSize;
	}
	public void setQueueSize(int queueSize) {
		this.queueSize = queueSize;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public int getPluralQueueSize() {
		return UserHelper.plurals(new Long(queueSize));
	}
}
