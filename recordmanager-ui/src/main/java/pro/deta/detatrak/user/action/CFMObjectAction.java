package pro.deta.detatrak.user.action;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.CacheContainer;
import pro.deta.detatrak.user.form.CFMObjectForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.ConfigDAO;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.DisplayRecordDO;
import ru.yar.vi.rm.data.DisplayValueDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.OperatorDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class CFMObjectAction extends Action {
	private final Logger logger = Logger.getLogger(CFMObjectAction.class);
	Random randomGenerator = new Random();


	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMObjectForm uf = (CFMObjectForm) form;
		RecordDAO dao = new RecordDAO(req);
		Calendar cal = UserHelper.getCurrentCalendar();
		uf.setDate(cal.getTime());
		if(uf.getHour() > 0)
			cal.set(Calendar.HOUR_OF_DAY,uf.getHour());
		if(uf.getMin() > 0)
			cal.set(Calendar.MINUTE,uf.getMin());
		if(uf.getDay() > 0)
			cal.set(Calendar.DAY_OF_MONTH, uf.getDay());
		if(uf.getMon() > 0)
			cal.set(Calendar.MONTH, uf.getMon());
		if(uf.getYear() > 0)
			cal.set(Calendar.YEAR, uf.getYear());

		int current = cal.get(Calendar.HOUR_OF_DAY)*60+cal.get(Calendar.MINUTE);
		int diff = uf.getThreshold();

		DisplayRecordDO objRecord = new DisplayRecordDO(); 
		
		populateObjectInfo(uf,objRecord,req);//cal.set(Calendar.HOUR_OF_DAY, 0);cal.set(Calendar.MINUTE, 0);cal.set(Calendar.SECOND,0);cal.getTime();
		List<StoredRecordDO> records = dao.getRecords(cal.getTime(), uf.getObjectId(), current-diff , current);
		if(uf.getLimit() > 0 && records.size() > uf.getLimit()) {
			records = records.subList(0, uf.getLimit());
		}
		for (StoredRecordDO rec : records) {
			DisplayValueDO value = formatValue(uf,rec);
			objRecord.getRecords().add(value);
		}
		uf.setDisplayRecord(objRecord);

		populatePictures(uf);
		return mapping.findForward("default");
	}

	private void populateObjectInfo(CFMObjectForm uf,
			DisplayRecordDO objRecord, HttpServletRequest req) {
		HDAO dict = UserHelper.getHDAO(req);
		DictDAO ddao = new DictDAO();
		try {
			ObjectDO obj = dict.getById(ObjectDO.class, uf.getObjectId());
			objRecord.setObject(obj);
			populateOperator(uf,objRecord,ddao);
		} finally {
			ddao.disconnect();
		}
	}

	private DisplayValueDO formatValue(CFMObjectForm uf, StoredRecordDO rec) {
		String[] params = uf.getDisplayParam().split(",");
		String value = "";
		for (int i = 0; i < params.length; i++) {
			try {
				value += (String) PropertyUtils.getProperty(rec, uf.getDisplayParam()) +" ";
			} catch (Exception e) {
				logger.error("Error while trying to get parameter named '"+uf.getDisplayParam()+"' from " + rec,e);
			}
		}

		DisplayValueDO displayValue = new DisplayValueDO();
		if(value != null) {

			if(value != null && uf.isToUpperCase())
				value = value.toUpperCase();

			if(uf.getLimitParam() > 0 && value != null && value.length() > uf.getLimitParam()) {
				value = value.substring(0, uf.getLimitParam());
			}
			displayValue.setValue(value);
		}
		displayValue.setDate(UserHelper.getDate(rec));
		return displayValue;
	}


	private void populateOperator(CFMObjectForm uf, DisplayRecordDO objRecord,DictDAO dict) {
			String operatorFileName = CacheContainer.getInstance().getConfig("line.operator.filename");
			try {
				
				if(operatorFileName != null) {
					File f = new File(operatorFileName);
					if(f.exists()) {
						BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
						String line =null;
						String prefix = CacheContainer.getInstance().getConfig("operator.prefix");
						HashMap hm = new HashMap();
						while((line = br.readLine()) != null) {
							line = line.trim();
							String[] arr = line.split("=");
							if(arr != null && arr.length > 1) {
								hm.put(arr[0], arr[1]);
							}
						}
						OperatorDO op = null;
						int num =dict.getNumByObject(uf.getObjectId());
						String operCode = (String) hm.get(prefix+num);
						if(operCode != null) {
							try {
								op = dict.getOperator(Integer.parseInt(operCode));
								objRecord.setOperName(op.getName());
								objRecord.setPosition(op.getPosition());
								objRecord.setLevel(op.getLevel());
							} catch (Exception e) {
								logger.error("Can't get operator name.", e);
							}
						}

						br.close();
					}
				} else
					objRecord.setOperName("");
			} catch (Exception e) {
				logger.error("Error while resolving operator name", e);
			}
	}

	
	public void populatePictures(CFMObjectForm uf) {
		String pictureFolder = CacheContainer.getInstance().getConfig("display.picture.folder");
		String picturePrefix = CacheContainer.getInstance().getConfig("display.picture.prefix");
		uf.setRandom(-1);
		if(pictureFolder != null) {
			File f = new File(pictureFolder);
			if(f.exists() && f.isDirectory()) {
				String list[] = f.list(new FilenameFilter() {
					public boolean accept(File dir, String n) {
						String f = new File(n).getName();
						String s1 = f.toLowerCase();
						return s1.endsWith(".jpg") || s1.endsWith(".gif") || s1.endsWith(".png");
					}
				});
				if(list != null && list.length > 0) {
					uf.setRandom(randomGenerator.nextInt(list.length));
					List<String> l1 = new ArrayList<String>();
					for (String string : list) {
						l1.add(picturePrefix+string);
					}
					uf.setPictures(l1);
				}
			}
		}
		
	}
}

