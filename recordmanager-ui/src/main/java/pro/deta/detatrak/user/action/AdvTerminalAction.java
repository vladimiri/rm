package pro.deta.detatrak.user.action;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.CacheContainer;
import pro.deta.detatrak.DataInitializerUtil;
import pro.deta.detatrak.data.OfficeStatDO;
import pro.deta.detatrak.user.form.AdvTerminalForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.NavigationDO;
import ru.yar.vi.rm.data.NotificationDO;
import ru.yar.vi.rm.data.TerminalLinkDO;
import freemarker.cache.StringTemplateLoader;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class AdvTerminalAction  extends Action{
	private static final Logger logger = Logger.getLogger(AdvTerminalAction.class);
	
	private static Configuration cfg = new Configuration();

	static {
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		{ cfg.setTemplateLoader(stringLoader);
		cfg.setObjectWrapper(new BeansWrapper()); };
	}

	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		AdvTerminalForm qf = (AdvTerminalForm) form;

		populateSite(qf,req);
		if(qf.getNav() == null) {
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("message.wrongReport"));
			saveErrors(req, ams);
			return mapping.getInputForward();
		}
		if(qf.getForward() != null) {
			return mapping.findForward(qf.getForward());
		}
		return mapping.findForward("default");
	}

	private void populateSite(AdvTerminalForm qf, HttpServletRequest req) {
		if((qf.getNavId()> 0 && qf.getNav() == null) || (qf.getNavId()> 0 && qf.getNav().getId() != qf.getNavId())) {
			try {
				NavigationDO report = UserHelper.getHDAO(req).find(NavigationDO.class, qf.getNavId());
				qf.setNav(report);
			} catch (Exception e) {
				logger.error("Error while looking for NavigationDO = " + qf.getNavId(),e);
			}
		}
		if(qf.getPageId() != null && qf.getPageId()> 0)
			qf.setPage(UserHelper.getHDAO(req).find(TerminalLinkDO.class, qf.getPageId()));
		else if(qf.getNav() != null && qf.getNav().getRoot() != null)
			qf.setPage(qf.getNav().getRoot());
		
		if(qf.getNav().getOffice() != null) {
			OfficeStatDO stat = CacheContainer.getInstance().getOfficeStat(qf.getNav().getOffice().getId());
			qf.setStat(stat);
			String scrolling = qf.getNav().getScrolling();
			NotificationDO notif = UserHelper.getHDAO(req).find(NotificationDO.class, DataInitializerUtil.STAT_SCROLLING_NOTIFICATION_ID);
			Template templ = null;
			try {
				try {
					templ = cfg.getTemplate("notification"+notif.getId());
				} catch (FileNotFoundException e) {
				}
				if(templ == null) {
					if (cfg.getTemplateLoader() instanceof StringTemplateLoader) {
						StringTemplateLoader stl = (StringTemplateLoader) cfg.getTemplateLoader();
						stl.putTemplate("notification"+notif.getId(), notif.getTemplate());
						templ = new Template("notification"+notif.getId(), new StringReader(notif.getTemplate()), cfg);
					}
				}
				Writer bsw = new StringWriter();
				templ.process(stat, bsw);
				qf.getNav().setScrolling(bsw.toString());
			} catch (IOException | TemplateException e) {
				e.printStackTrace();
			}
		}
	}
}
