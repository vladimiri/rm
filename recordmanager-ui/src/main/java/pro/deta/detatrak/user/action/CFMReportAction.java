package pro.deta.detatrak.user.action;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import pro.deta.detatrak.dao.data.CFMReportDO;
import pro.deta.detatrak.user.form.CFMReportForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionDO;



public class CFMReportAction extends Action {
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest req,
			HttpServletResponse response) throws Exception{
		CFMReportForm uf = (CFMReportForm) form;
		if(uf.getDates().isEmpty()) {
			int dateRange = 45;
			Calendar cal = UserHelper.getCurrentCalendar();
			cal.add(Calendar.DAY_OF_MONTH, -dateRange);
			for(int i=0;i<= dateRange;i++) {
				cal.add(Calendar.DAY_OF_MONTH, 1);
				uf.getDates().add(new DateDO(i,cal.getTime()));
			}
			Collections.reverse(uf.getDates());
			List<ActionDO> list = UserHelper.getHDAO(req).getAll(ActionDO.class);
			for (ActionDO actionDO : list) {
				uf.getActions().put(actionDO.getId(), actionDO);
			}
		}
		
		if(uf.getStartDate() > 0 && uf.getStartDate() <= uf.getEndDate()) {
			uf.getReport().clear();
			OnlineDAO dao = new OnlineDAO(); 
			try {
				dao.connect();
				int sz = uf.getDates().size()-1;
				for(int i=0;i+uf.getStartDate()<= uf.getEndDate();i++) {
					Date dt = uf.getDates().get(sz-i-uf.getStartDate()).getDate();
					List<CFMReportDO> report = dao.getReportDO(dt);
					if(report != null) {
						uf.getReport().addAll(report);
					}
				}
				Collections.reverse(uf.getReport());
			} finally {
				dao.disconnect();
			}

		}
		

		return mapping.findForward("success");
	}

}
