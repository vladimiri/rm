package pro.deta.detatrak;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.yar.vi.rm.model.NumberWrapper;

@WebServlet(urlPatterns = "/wschat/QueueControlServlet")
public class QueueControlServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -623401601477809897L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		Integer officeId = new NumberWrapper(req.getParameter("officeId")).getInteger();
		String message = req.getParameter("message");
		Integer objectId = new NumberWrapper(req.getParameter("objectId")).getInteger();
		Boolean distinct = new Boolean(req.getParameter("distinct"));
		CFMDispatchStatusServlet.sendMessage(new CFMMessage(message,officeId,objectId,distinct));
		resp.sendRedirect("/gbd/self/chatcontrol.html");
	}
	
	
}