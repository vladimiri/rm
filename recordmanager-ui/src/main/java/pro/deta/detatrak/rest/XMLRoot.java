package pro.deta.detatrak.rest;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class XMLRoot<T> {
	private T root;
	private String rootClass;
	
	public XMLRoot(T object) {
		this.root = object;
		this.rootClass = object.getClass().getCanonicalName();
	}

	public T getRoot() {
		return root;
	}

	public void setRoot(T root) {
		this.root = root;
	}

	public String getRootClass() {
		return rootClass;
	}

	public void setRootClass(String rootClass) {
		this.rootClass = rootClass;
	}
}
