package pro.deta.detatrak.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionStatisticDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.StoredRecordDO;

@Path("/action")
public class ActionResource extends BaseResource {
	
	@GET
	@Path("/{actionId}/office/{officeId}/statistic")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTodo(@PathParam("actionId")  int actionId,
			@PathParam("officeId")  int officeId) {
		OnlineDAO dao = new OnlineDAO();
		ActionStatisticDO stat;
		try {
			dao.connect();
			stat = dao.getCfmQueueLength(actionId, officeId, StoredRecordDO.RECORD_STATUS_WAITING);
			OfficeDO office = getEntityManager().find(OfficeDO.class, officeId);
			stat.setOfficeClosed(!UserHelper.checkOfficeSchedule(office.getSchedule()));
		} finally {
			if(dao != null)
				dao.disconnect();
		}
		
		return Response.ok(stat).build();
	}
}
