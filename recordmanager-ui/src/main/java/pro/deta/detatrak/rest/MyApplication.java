package pro.deta.detatrak.rest;

import java.util.HashMap;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationPath("rest")
public class MyApplication extends ResourceConfig {
	private static final Logger logger = LoggerFactory.getLogger(MyApplication.class);

	public MyApplication() {
		packages("pro.deta.detatrak.rest");
		register(MyObjectMapperProvider.class);
		addProperties(new HashMap<String, Object>(){{this.put("com.sun.jersey.config.feature.Trace", true);}});
		logger.error("Detatrack REST service started.");
	}
	
}