package pro.deta.detatrak.rest;

import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ru.yar.vi.rm.data.OfficeDO;

@Path("/office")
public class OfficeResource extends BaseResource {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getResponse() {
		return "Hello world";
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTodo(@PathParam("id")  int id) {
		OfficeDO office = getEntityManager().find(OfficeDO.class,id);
		if(office==null)
			throw new NotFoundException();
		return Response.ok(new XMLRoot<OfficeDO>(office)).build();
	}
	
	@Path("/html/{officeId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public OfficeDO getTodoHTML(@PathParam("officeId") int officeId) {
		OfficeDO office = getEntityManager().find(OfficeDO.class,officeId);
		if(office==null)
			throw new RuntimeException("Get: Office with " + officeId +  " not found");
		return office;
	}
	
}
