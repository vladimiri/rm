package pro.deta.detatrak.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.SiteDO;

@Path("/site")
public class SiteResource extends BaseResource {
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSite(int siteId) {
		ResponseBuilder response = Response.ok(getEntityManager().find(SiteDO.class, siteId));
		return response.build();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSite() {
		ResponseBuilder response = Response.ok(UserHelper.getSite(httpRequest));
		return response.build();
	}
}
