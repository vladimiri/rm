<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>


<fieldset><legend><h1><fmt:message key="label.vehicle.info"/></h1></legend>
 <div class="box">
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleDocNum"/><br/>
 <h:text property="vehicle.vehicleDocNum" styleId="vehicleDocNum"/>&nbsp;
 </div>
 <div class="field">
 <fmt:message key="label.vehicleDocDate"/><br/>
 <h:text property="vehicle.vehicleDocDate" styleId="vehicleDocDate"/>
 </div>
 
 </div>
 </fieldset>
 <input type="submit" name="prev" value='<fmt:message key="label.prev"/>'>
  <input type="submit" name="next" value='<fmt:message key="label.next"/>'>
  