<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<h:errors/>

<fmt:message key="label.online.enter">
<fmt:param value="${sessionScope.CFMControlForm.selectedObject.name}"/>
<fmt:param value="${sessionScope.CFMControlForm.login}"/>
</fmt:message>
<br/>

<h:form action="/cfmcall.do" method="post">
	<h:hidden property="action" value="processNext"/>

	<h:submit><fmt:message key="label.processNext"/></h:submit>
</h:form>


<table border=1>
<c:forEach items="${CFMControlForm.stat}" var="rec">
<jsp:useBean id="rec" class="ru.yar.vi.rm.data.ActionStatisticDO"/>
<tr>
<h:hidden property="actionId" value="${rec.action.id }"/>
	<td><c:out value="${rec.action.name}"/></td>
	<td><c:out value="${rec.cfmQueueSize}"/></td>
	<td><c:out value="${rec.lastCreationDate}"/></td>
	<td>
	<h:form action="/cfmcall.do" method="post">
	<h:hidden property="action" value="selectedRecord"/>
<h:select property="selectedRec">
<c:forEach items="${rec.queue}" var="r">
<option value="${r.id }"><c:out value="${r.name }"/></option>
</c:forEach>
</h:select>
	
	<h:submit><fmt:message key="label.processNext"/></h:submit>
</h:form>
	
	</td>
</tr>
</c:forEach>
</table>

<c:if test="${! empty CFMControlForm.currentRecord }">
<fmt:message key="label.cfminvite">
<fmt:param value="${CFMControlForm.currentRecord.name}"/>
</fmt:message>

<h:form action="/cfmcall.do" method="post">
	<h:hidden property="action" value="finish"/>
<h:submit><fmt:message key="label.cfmfinish"/></h:submit>
</h:form>

<fmt:message key="label.redirect"/>
<h:form action="/cfmcall.do" method="post">
	<h:hidden property="action" value="redirect"/>
<h:select property="redirectTo"><h:optionsCollection property="site.actions" label="name" value="id"/></h:select>
	<h:submit><fmt:message key="label.redirect"/></h:submit>

</h:form>
</c:if>




<a href="../self/status.do?officeId=${CFMControlForm.officeId}"><fmt:message key="label.cfmstatus"/></a>
<a href="../self/record.do?officeId=${CFMControlForm.officeId}"><fmt:message key="label.cfmrecord"/></a>

<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>