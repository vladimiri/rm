<%@page import="ru.yar.vi.rm.dao.HDAO"%>
<%@page import="ru.yar.vi.rm.data.GenCodeDO"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>

<div class="box">
<h:errors/>

<br/>

<h:form action="/applicationService.do" method="post">
<h:submit><fmt:message key="label.refresh"/></h:submit>
</h:form>

<c:if test="${! empty ApplicationServiceForm.forms }">
<table border=1>
<thead>
<tr>
<td><fmt:message key="label.appservice.id"/></td>
<td><fmt:message key="label.appservice.applicationAuthority"/></td>
<td><fmt:message key="label.appservice.name"/></td>
<td><fmt:message key="label.appservice.date"/></td>
</tr>
</thead>

<c:forEach items="${ApplicationServiceForm.forms}" var="rec">
<jsp:useBean id="rec" class="ru.yar.vi.rm.user.form.ApplicationServiceForm"/>
<tr>
<td><c:out value="${rec.id }"/></td>
<td><c:out value="${rec.applicationAuthorityText }"/></td>
<td><c:out value="${rec.owner.name }"/></td>
<td><c:out value="${rec.sysCreationDate }"/></td>
</tr>
</c:forEach>
</table>

</c:if>

<h:form action="/applicationService.do" method="post">
<h:hidden property="action" value="submit" styleId="action"/>
<fmt:message key="label.appservice.url"/> <h:text property="url"/><br/>
<fmt:message key="label.appservice.user"/> <h:text property="user"/><br/>
<fmt:message key="label.appservice.pass"/> <h:text property="pass"/><br/>
<fmt:message key="label.appservice.regionUser"/> <h:text property="regionUser" value="vilmov"/><br/>

<h:submit property="action"><fmt:message key="label.migrate"/></h:submit>
<h:submit property="action" onclick="return $('#action').val('refreshPerson');"><fmt:message key="label.refreshPerson"/></h:submit>
<h:submit property="action" onclick="return $('#action').val('refreshVehicle');"><fmt:message key="label.refreshVehicle"/></h:submit>
</h:form>

<br/>

<h:form action="/applicationService.do" method="post"  enctype="multipart/form-data">
<h:hidden property="action" value="submit" styleId="action1"/>

<fmt:message key="label.appservice.vehicleFile"/> <h:file property="vehicleFile" size="50" /><br/>
<h:errors property="vehicleFile"/>
<fmt:message key="label.appservice.personFile"/> <h:file property="personFile" size="50" /><br/>
<h:errors property="personFile"/>
<h:submit property="action" onclick="return $('#action1').val('uploadFiles');"><fmt:message key="label.upload"/></h:submit>

</h:form>


<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>