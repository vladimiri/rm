<%@page contentType="text/javascript" trimDirectiveWhitespaces="true"%>
<%response.setHeader("Cache-Control", "max-age=3600, must-revalidate");%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
var <c:out value="${GenericCodeForm.name}"/>_List = [
<c:forEach items="${GenericCodeForm.list }" var="item" varStatus="st">{id: "<c:out value="${item.id }"/>", name: "<c:out value="${item.genDesc }"/>"}<c:if test="${not st.last}">,</c:if>
</c:forEach>
];

var <c:out value="${GenericCodeForm.name}"/>AutocompleteValues = [];

for(var i = 0; i < <c:out value="${GenericCodeForm.name}"/>_List.length; i++)
	<c:out value="${GenericCodeForm.name}"/>AutocompleteValues.push(<c:out value="${GenericCodeForm.name}"/>_List[i].name);
	
function set<c:out value="${GenericCodeForm.name}"/>(colorId, colorName) {
	colorId = colorId || null;
	colorName = colorName || "";
	if(colorId) {
		for(var i = 0; i < <c:out value="${GenericCodeForm.name}"/>_List.length; i++) {
			if(<c:out value="${GenericCodeForm.name}"/>_List[i].id == colorId) {
				$('#<c:out value="${GenericCodeForm.name}"/>Input').val(<c:out value="${GenericCodeForm.name}"/>_List[i].name);
				return true;
			}
		}
	}
	$('#<c:out value="${GenericCodeForm.name}"/>Input').val(colorName);
}

function submit<c:out value="${GenericCodeForm.name}"/>() {
	for(var i = 0; i < <c:out value="${GenericCodeForm.name}"/>_List.length; i++) {
		if(<c:out value="${GenericCodeForm.name}"/>_List[i].name == $('#<c:out value="${GenericCodeForm.name}"/>Input').val()) {
			$('#<c:out value="${GenericCodeForm.name}"/>Id').val(<c:out value="${GenericCodeForm.name}"/>_List[i].id);
			return true;
		}
	}
	$('#<c:out value="${GenericCodeForm.name}"/>Name').val($('#<c:out value="${GenericCodeForm.name}"/>Input').val());
}
