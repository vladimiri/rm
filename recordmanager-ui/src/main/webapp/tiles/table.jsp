<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page import="ru.yar.vi.rm.data.ActionDO"%>
<%@page errorPage="map.jsp" %>

<c:if test="${UserForm.actionId == 0}">
<%
	session.setAttribute("UserForm", session.getAttribute("UserForm0"));
%>
</c:if>
<jsp:useBean id="UserForm" class="ru.yar.vi.rm.user.form.UserForm" scope="session"/>


	
<l:notEqual name="UserForm" property="module" value="/self">
<p class="noprint"><c:out value="${deta:textToHtml(SITE.intro) }" escapeXml="false"/>
<%=UserHelper.makeHtml(UserForm.getSelectedAction().getDescription()) %></p>
</l:notEqual>
<p class="noprint"><%=UserHelper.makeHtml(UserForm.getSelectedAction().getWarningText()) %></p>

<table class="info noprint">

<c:if test="${! empty UserForm.name }">
<tr>
<td class="name">
<c:forEach items="${UserForm.fields }" var="item">
	<c:if test="${item.field eq 'name' }"><c:out value="${item.name }"/></c:if>
</c:forEach></td>
<td><c:out value="${UserForm.name }"/></td>
</tr>
</c:if>
<c:if test="${!UserForm.currentDay }">
<tr>
<td class="name"><fmt:message key='label.action'/></td>
<td><c:out value="${UserForm.selectedAction.name}"/></td>
</tr>
<c:if test="${UserForm.module ne '/self' }">
<tr>
<td class="name"><fmt:message key='label.region'/></td>
<td><c:out value="${UserForm.selectedRegion.name}"/></td>
</tr>
<c:if test="${UserForm.criteria >0 }">
<!-- <tr>
<td><c:forEach items="${UserForm.fields }" var="item">
	<c:if test="${item.field eq 'info(vehicleName)' }"><c:out value="${item.name }"/></c:if>
</c:forEach></td>
<td><c:out value="${UserForm.infoMap.vehicleName }"/>&nbsp;<c:out value="${UserForm.infoMap.vehicleNum }"/></td>
</tr>
-->
<c:forEach items="${UserForm.fields }" var="item">
	<c:if test="${item.field eq 'criteria' }">
		<jsp:useBean id="item" class="ru.yar.vi.rm.data.CustomFieldDO"/>
		<tr>
			<td class="name"><c:out value="${item.name }"/></td>
			<td><%=UserHelper.getName(UserForm.getCriteria(), item.getCriteria())%></td>
		</tr>
	</c:if>
</c:forEach>
<tr>
<td class="name"><fmt:message key='label.contact'/></td>
<td><c:out value="${UserForm.email }"/>&nbsp;<c:out value="${UserForm.phone }"/></td>
</tr>
</c:if></c:if></c:if>
</table>

<div class="print">
<pre>
<%=UserHelper.format(new java.util.Date())%>

<b><c:out value="${UserForm.name }"/></b><c:if test="${!UserForm.currentDay }">
<b><%=UserHelper.print(UserHelper.getName(UserForm.getActionId(), UserForm.getActions()))%></b>
<b><%=UserHelper.print(UserHelper.getName(UserForm.getCriteria(), UserForm.getCriterias()),UserHelper.LINE_LENGTH)%></b>
<b><c:out value="${UserForm.infoMap.vehicleName }"/> <c:out value="${UserForm.infoMap.vehicleNum }"/></b>
<b><c:out value="${UserForm.phone }"/></b></c:if>
</pre>
</div>