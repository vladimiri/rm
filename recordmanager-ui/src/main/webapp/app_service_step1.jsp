<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>


<script src="gencode.do?genType=COUNTRY"></script>
<script src="gencode.do?genType=REGION"></script>
<script src="gencode.do?genType=REGION&arrayName=CREGION"></script>
<script>

$(document).ready(function() {
	$( "#COUNTRYInput" ).autocomplete({
		source: COUNTRYAutocompleteValues
	});

	$( "#REGIONInput" ).autocomplete({
		source: REGIONAutocompleteValues
	});

	$( "#СREGIONInput" ).autocomplete({
		source: CREGIONAutocompleteValues
	});

	$('#form1').submit(function() {
		submitCOUNTRY();
		submitREGION();
		submitСREGION();
		return true;
	});

	setCOUNTRY($('#COUNTRYId').val(), $('#COUNTRYName').val());
	
	setREGION($('#REGIONId').val(), $('#REGIONName').val());

	setСREGION($('#СREGIONId').val(), $('#СREGIONName').val());
	
});

</script>
<style>
<!--
select {
	width: 600px;
}
-->
</style>

 <fieldset><legend><h1><fmt:message key="label.owner.info"/></h1></legend>

 
 <div class="box">
 <!-- физик -->
   
 <div class="field">
 <fmt:message key="label.docwhere"/><br/>
 <h:text property="owner.docWhen" styleClass="docWhen" style="width: 80px;"/>&nbsp;
 <h:text property="owner.docWhere" styleClass="docWhere" style="width: 235px;"/>&nbsp;
 </div>
<h:errors property="owner.docWhere" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="owner.birthDate" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field">
 <fmt:message key="label.birthPlace"/><br/>
<h:text property="owner.birthPlace" styleClass="birthPlace" />
 </div>


<h:errors property="owner.birthPlace" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field">
 <fmt:message key="label.address"/><span class="star">*</span><br/>
 <input id="REGIONInput"/>
 <h:hidden property="owner.address.region" styleId="REGIONId"/> 
 <h:hidden property="owner.address.regionName" styleId="REGIONName"/> 
 <h:text property="owner.address.district" styleClass="address_district"/>
</div> 
 <div class="field">
 <h:text property="owner.address.town" styleClass="address_town"/>&nbsp;&nbsp;
 <fmt:message key="label.address.st"/><h:text property="owner.address.street" styleClass="address_street" style="width: 135px;"/>&nbsp;
 <fmt:message key="label.address.house"/><h:text property="owner.address.house" styleClass="address_house" style="width: 28px;"/>&nbsp;
 <fmt:message key="label.address.bld"/><h:text property="owner.address.building" styleClass="address_building" style="width: 26px;"/>&nbsp;
 <fmt:message key="label.address.fl"/><h:text property="owner.address.apartment" styleClass="address_apartment" style="width: 28px;"/>
 </div>


<h:errors property="owner.address" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.citizenship"/><br/>
 <input id="COUNTRYInput"/>
 <h:hidden property="owner.citizenship" styleId="COUNTRYId"/> 
 <h:hidden property="owner.citizenshipName" styleId="COUNTRYName"/> 
 </div>
 <div class="field"  style="width: 165; position: relative; float: left;">
 <fmt:message key="label.sex"/><br/>
 <h:select property="owner.sex" style="width: 160px;"><h:optionsCollection property="sexList" value="id" label="genDesc"/></h:select>
 </div>
  <div class="field">
  <fmt:message key="label.phone"/><br/>
 <h:text property="owner.phone" styleClass="phone"/>
 </div>

<h:errors property="owner.citizenship" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="owner.sex" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="owner.phone" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field">
 <fmt:message key="label.inn"/><fmt:message key="label.required"/><br/>
 <h:text property="owner.inn" styleClass="inn"/>
 </div>

 </div>
 
  </fieldset>
  <input type="submit" name="prev" value='<fmt:message key="label.prev"/>'>
  <input type="submit" name="next" value='<fmt:message key="label.next"/>'>
