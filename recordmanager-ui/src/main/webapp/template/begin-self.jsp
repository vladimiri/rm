<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page session="true" contentType="text/html; charset=windows-1251" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title><c:out value="${SITE.httpTitle }"/></title>
<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
<meta name="keywords" content="${SITE.httpKeywords }" />
<meta name="description" content="${SITE.httpDescription }" />
<meta http-equiv="Content-Language" content="ru">
<meta http-equiv="Content-Type" content="text/html;charset=windows-1251">
<link rel="shortcut icon" type="image/icon" href="/favicon.ico" />
<script type="text/javascript" src="../include/keyboard.js" charset="UTF-8" ></script>
<script type="text/javascript" src="../include/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../staticJavascript.jsp"></script>
<script type="text/javascript" src="record.js"></script>

<link rel="stylesheet" type="text/css" href="../include/common.css"/>
<link rel="stylesheet" type="text/css" href="../include/keyboard.css">
<link rel="stylesheet" type="text/css" href="../custom/self.jsp"/>
<link rel="stylesheet" type="text/css" href="../include/print.css" media="print"/>
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
