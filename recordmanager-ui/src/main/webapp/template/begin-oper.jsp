<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page session="true" contentType="text/html; charset=windows-1251" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title><c:out value="${SITE.httpTitle }"/></title>
<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
<meta name="keywords" content="${SITE.httpKeywords }" />
<meta name="description" content="${SITE.httpDescription }" />
<meta http-equiv="Content-Language" content="ru">
<meta http-equiv="Content-Type" content="text/html;charset=windows-1251">
<link rel="shortcut icon" type="image/icon" href="/favicon.ico" />
<script type="text/javascript" src="../staticJavascript.jsp"></script>

<link rel="stylesheet" type="text/css" href="../include/common.css"/>
<link rel="stylesheet" type="text/css" href="../include/oper.css" >
<link rel="stylesheet" type="text/css" href="../include/print.css" media="print"/>
<script type="text/javascript" src="../include/jquery.min.js"></script>
</head>

<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0">
