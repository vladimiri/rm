<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page session="true" contentType="text/html; charset=windows-1251" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
<title><c:out value="${SITE.httpTitle }"/></title>
<meta http-equiv="content-type" content="text/html;charset=windows-1251" />
<meta name="keywords" content="${SITE.httpKeywords }" />
<meta name="description" content="${SITE.httpDescription }" />
<meta name="rating" content="General" />
<meta name="ROBOTS" content="All" />
<meta name="revisit-after" content="3 days" />
<c:out value="${SITE.httpTemplate }" escapeXml="false"/>
<link rel="shortcut icon" type="image/icon" href="/favicon.ico" />
<script type="text/javascript" src="staticJavascript.jsp"></script>
<script src="custom/custom.js"></script>
<script src="include/jquery.min.js"></script>
<script src="include/jquery-ui-1.10.3.custom.min.js"></script>
<script src="include/jquery.ui.datepicker-ru.js"></script>

<link type="text/css" rel="stylesheet" href="include/common.css"/>
<link type="text/css" rel="stylesheet" href="include/jquery-ui-1.10.3.custom.min.css"/>
<link type="text/css" rel="stylesheet" href="custom/style.jsp" >
<link rel="stylesheet" type="text/css" href="include/print_main.css" media="print"/>
<link rel="shortcut icon" href="favicon.png" type="image/png">

</head>
<body>

