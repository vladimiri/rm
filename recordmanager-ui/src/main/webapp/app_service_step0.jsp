<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>

<script src="gencode.do?genType=COUNTRY"></script>
<script src="gencode.do?genType=REGION"></script>
<script src="gencode.do?genType=REGION&arrayName=CREGION"></script>
<script>

$(document).ready(function() {
		$( "#REGIONInput" ).autocomplete({
		source: REGIONAutocompleteValues
	});

	

	$('#form1').submit(function() {
		submitREGION();
		return true;
	});

	setREGION($('#REGIONId').val(), $('#REGIONName').val());

	
});

</script>

<p><fmt:message key="label.applicationAuthority"/><br/>
<h:select property="applicationAuthority"><h:optionsCollection property="applicationAuthorityList" value="id" label="genDesc"/></h:select>
</p>

<p>
<fmt:message key="label.service"/><br/>
<h:select property="action" styleId="action"><h:optionsCollection property="actionList" value="id" label="genDesc"/></h:select><br/>
<h:select property="subAction"><h:optionsCollection property="subActionList" value="id" label="genDesc"/></h:select>
</p>
		
<p><fmt:message key="label.docs"/><br/>

 <c:forEach items="${ApplicationServiceForm.documentList}" var="eachItem" varStatus="status" >
   <h:multibox property="docsArr"><c:out value="${eachItem.id}" /></h:multibox>&nbsp;<c:out value="${eachItem.genDesc}" />&nbsp;&nbsp;
 </c:forEach>
 
<fmt:message key="label.other"/>&nbsp;<h:text property="docsOther"/>
</p>
 
 <fieldset><legend><h1><fmt:message key="label.owner.info"/></h1></legend>
 <div class="box">
 <div class="field">
 <fmt:message key="label.ownership"/>
 <h:select property="ownership" styleId="ownership" style="width: 490px;"><h:optionsCollection property="ownershipList" value="id" label="genDesc"/></h:select>
 </div>

 <h:select property="customerType" styleId="customerType" style="width: 150px;"><h:optionsCollection property="customerTypeList" value="name" label="genDesc"/></h:select>
 </div>
 
 <div id="customerType1">
 <div class="box">
 <!-- физик -->
 <div class="field">
 
 <fmt:message key="label.applicant.name"/><br/>
 <h:text property="owner.lastName" styleClass="lastName"/>&nbsp;
 <h:text property="owner.firstName" styleClass="firstName"/>&nbsp;
 <h:text property="owner.middleName" styleClass="middleName"/>
 </div>
 <h:errors property="owner.name" prefix="error.prefix" suffix="error.suffix"/>
 
 <div class="field" style="width: 165; position: relative; float: left;">
  <br/>
  <h:select property="owner.docType" styleId="ownerDocType" style="width: 160px;"><h:optionsCollection property="docTypeList" value="id" label="genDesc"/></h:select>
 </div>
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.docnum"/> 
  <h:text property="owner.docNum" styleClass="docNum"/>
 </div>
 <div class="field">
 <fmt:message key="label.birthDate"/><br/>
  <h:text property="owner.birthDate"  styleClass="birthDate"/>
 </div>

<h:errors property="owner.docType" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="owner.docNum" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="owner" prefix="error.prefix" suffix="error.suffix"/>
  
 
 </div>
 <!-- /физик -->
 </div>
 
 <div id="customerType2">
 
 <div class="box">
  <!-- юрик -->
 <div class="field">
 <fmt:message key="label.applicant.companyName"/><br/>
 <h:text property="company.name" style="width: 100%;" styleId="companyName"/>
 </div>

<h:errors property="company.name" prefix="error.prefix" suffix="error.suffix"/>
 
 <div class="field" style="width: 170; position: relative; float: left;">
 <fmt:message key="label.inn" /><br/>
 <h:text property="company.inn" styleClass="inn"/>
 </div>
 <div class="field">
 <fmt:message key="label.ogrn"/><br/>
 <h:text property="company.ogrn" styleClass="ogrn"/>
 </div>
<input type="submit" name="act" value='<fmt:message key="label.check"/>'>	
 <h:errors property="company.inn" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field">
 <fmt:message key="label.address"/><br/>
  <input id="REGIONInput"/>
  <h:hidden property="company.address.region" styleId="REGIONId"/> 
  <h:hidden property="company.address.regionName" styleId="REGIONName"/>&nbsp;
 <h:text property="company.address.post" styleClass="address_post" style="width: 150px;"/>
</div> 
 <div class="field">
 <h:text property="company.address.town" styleClass="address_town"/>&nbsp;&nbsp;
 <fmt:message key="label.address.st"/><h:text property="company.address.street" styleClass="address_street" style="width: 135px;"/>&nbsp;
 <fmt:message key="label.address.house"/><h:text property="company.address.house" styleClass="address_house" style="width: 28px;"/>&nbsp;
 <fmt:message key="label.address.bld"/><h:text property="company.address.building" styleClass="address_building" style="width: 26px;"/>&nbsp;
 <fmt:message key="label.address.fl"/><h:text property="company.address.apartment" styleClass="address_apartment" style="width: 28px;"/>
 </div>


 <h:errors property="company.address" prefix="error.prefix" suffix="error.suffix"/>
 <!-- /юрик -->
 </div>
 </div>
 

  </fieldset>
  <input type="submit" name="next" value='<fmt:message key="label.next"/>'>
