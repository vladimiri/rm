<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>

<script src="gencode.do?genType=COUNTRY"></script>
<script src="gencode.do?genType=REGION"></script>
<script src="gencode.do?genType=REGION&arrayName=CREGION"></script>
<script>

$(document).ready(function() {
	$( "#COUNTRYInput" ).autocomplete({
		source: COUNTRYAutocompleteValues
	});

	$( "#REGIONInput" ).autocomplete({
		source: REGIONAutocompleteValues
	});

	$( "#СREGIONInput" ).autocomplete({
		source: CREGIONAutocompleteValues
	});

	$('#form1').submit(function() {
		submitCOUNTRY();
		submitREGION();
		submitСREGION();
		return true;
	});

	setCOUNTRY($('#COUNTRYId').val(), $('#COUNTRYName').val());
	
	setREGION($('#REGIONId').val(), $('#REGIONName').val());

	setСREGION($('#СREGIONId').val(), $('#СREGIONName').val());
	
});

</script>
 <fieldset><legend><h1><fmt:message key="label.personal.info"/></h1></legend>
   <div class="box">
   <fmt:message key="label.enableAgreement"/> <h:checkbox property="enableAgreement" styleId="enableAgreement"/>
 </div>
  
  
 <div class="box" id="personalAgreement">
 <!-- физик -->
 
  <div class="field" style="width: 330; position: relative; float: left;">
 <fmt:message key="label.docwhere"/><br/>
 <h:text property="personal.docWhen" styleClass="docWhen" style="width: 80px;"/>&nbsp;
 <h:text property="personal.docWhere" styleClass="docWhere" style="width: 235px;"/>&nbsp;
 </div>
 <div class="field">
 <fmt:message key="label.birthPlace"/><br/>
<h:text property="personal.birthPlace" styleClass="birthPlace" />
 </div>


<h:errors property="personal.docWhere" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="personal.birthDate" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="personal.birthPlace" prefix="error.prefix" suffix="error.suffix"/>


<div class="field">
 <fmt:message key="label.address"/><br/>
 <input id="REGIONInput"/>
 <h:hidden property="personal.address.region" styleId="REGIONId"/> 
 <h:hidden property="personal.address.regionName" styleId="REGIONName"/> 
 <h:text property="personal.address.district" styleClass="address_district"/>
</div> 

 <div class="field">
 <h:text property="personal.address.town" styleClass="address_town"/>&nbsp;&nbsp;
 <fmt:message key="label.address.st"/><h:text property="personal.address.street" styleClass="address_street" style="width: 135px;"/>&nbsp;
 <fmt:message key="label.address.house"/><h:text property="personal.address.house" styleClass="address_house" style="width: 28px;"/>&nbsp;
 <fmt:message key="label.address.bld"/><h:text property="personal.address.building" styleClass="address_building" style="width: 26px;"/>&nbsp;
 <fmt:message key="label.address.fl"/><h:text property="personal.address.apartment" styleClass="address_apartment" style="width: 28px;"/>
 </div>

<h:errors property="personal.address" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.citizenship"/><br/>
 <input id="COUNTRYInput"/>
 <h:hidden property="personal.citizenship" styleId="COUNTRYId"/> 
 <h:hidden property="personal.citizenshipName" styleId="COUNTRYName"/> 
 </div>
 <div class="field"  style="width: 165; position: relative; float: left;">
 <fmt:message key="label.sex"/><br/>
 <h:select property="personal.sex" style="width: 160px;"><h:optionsCollection property="sexList" value="id" label="genDesc"/></h:select>
 </div>
  <div class="field">
  <fmt:message key="label.phone"/><br/>
 <h:text property="personal.phone" styleClass="phone"/>
 </div>

<h:errors property="personal.citizenship" prefix="error.prefix" suffix="error.suffix"/>
<h:errors property="personal.sex" prefix="error.prefix" suffix="error.suffix"/>

 <div class="field">
 <fmt:message key="label.inn"/><fmt:message key="label.required"/><br/>
 <h:text property="personal.inn" styleClass="inn"/>
 </div>
<h:errors property="personal.phone" prefix="error.prefix" suffix="error.suffix"/>

 
  </fieldset>
  <input type="submit" name="prev" value='<fmt:message key="label.prev"/>'>
  <input type="submit" name="next" value='<fmt:message key="label.send"/>'>