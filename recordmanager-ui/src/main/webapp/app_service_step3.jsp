<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>

<script src="gencode.do?genType=COLOUR"></script>
<script src="gencode.do?genType=TRADEMARK"></script>
<script>

$(document).ready(function() {
	$('#vehiclePowerHp').bind('input',function() {
		$('#vehiclePowerVt').val(Math.round($( this ).val()*0.73549875* 100) / 100);
		return true;
	});

	$( "#COLOURInput" ).autocomplete({
		source: COLOURAutocompleteValues
	});

	$( "#TRADEMARKInput" ).autocomplete({
		source: TRADEMARKAutocompleteValues
	});

	setCOLOUR($('#COLOURId').val(), $('#COLOURName').val());
	
	setTRADEMARK($('#TRADEMARKId').val(), $('#TRADEMARKName').val());

	$('#form1').submit(function() {
		submitCOLOUR();	
		submitTRADEMARK();
		return true;
	});
	
	
	$( "#dateInput" ).datepicker({minDate: 0});
	
	
});

</script>

<fieldset><legend><h1><fmt:message key="label.vehicle.info"/></h1></legend>
 <div class="box">
 
  <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleManufacture"/><br/>
 <input id="TRADEMARKInput"/>
 <h:hidden property="vehicle.vehicleManufacture" styleId="TRADEMARKId"/> 
 <h:hidden property="vehicle.vehicleManufactureName" styleId="TRADEMARKName"/> 
  </div>
 
 <div class="field" >
 <fmt:message key="label.vehicleModel"/><br/>
 <h:text property="vehicle.vehicleModel" styleId="vehicleModel"/>
 </div>
 <div class="field" >
 <fmt:message key="label.vehicleSignType"/><br/>
 <h:select property="vehicle.vehicleSignType"><h:optionsCollection property="regFieldTypeList" value="id" label="genDesc"/></h:select>&nbsp;
 </div>
 
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleSign"/><br/>
 <h:text property="vehicle.vehicleSign" styleId="vehicleSign"/>
 </div>
 <div class="field"  style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleVIN"/><br/>
 <h:text property="vehicle.vehicleVIN" styleId="vehicleVIN"/>
 </div>
 
 <div class="field">
 <fmt:message key="label.vehicleType"/><br/>
 <h:select property="vehicle.vehicleType" style="width: 160px;"><h:optionsCollection property="vehicleTypeList" value="id" label="genDesc"/></h:select>
 </div><br/>
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleCategory"/><br/>
 <h:select property="vehicle.vehicleCategory" style="width: 150px;"><h:optionsCollection property="vehicleCategoryList" value="id" label="genDesc"/></h:select>
 </div>

 <h:errors property="vehicle.vehicleType" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleCategory" prefix="error.prefix" suffix="error.suffix"/>


 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleYear" /><br/>
 <h:text property="vehicle.vehicleYear" styleId="vehicleYear"/>
 </div>
 <div class="field">
 <fmt:message key="label.vehicleColor"/><br/>
 <input id="COLOURInput"/>
 <h:hidden property="vehicle.vehicleColor" styleId="COLOURId"/> 
 <h:hidden property="vehicle.vehicleColorName" styleId="COLOURName"/> 
 </div>
 <br/>
  <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleCorpType"/><br/>
 <h:select property="vehicle.vehicleCorpType" style="width: 150px;"><h:optionsCollection property="vehicleCorpTypeList" value="id" label="genDesc"/></h:select>
 </div>
 <div class="field" style="width: 165; position: relative; float: left;">
 <fmt:message key="label.vehicleBodyNum"/><br/>
 <h:text property="vehicle.vehicleBodyNum" styleId="vehicleBodyNum"/>
 </div>
<div class="field">
 <fmt:message key="label.vehiclePrice"/><br/>
 <h:text property="vehicle.vehiclePrice" styleId="vehiclePrice"/>
 </div> 
 
 
 
  <div class="field" style="width: 330; position: relative; float: left;">
 <fmt:message key="label.vehicleEngineType"/><br/>
 <h:select property="vehicle.vehicleEngineType" style="width: 320px;"><h:optionsCollection property="engineTypeList" value="id" label="genDesc"/></h:select>
 </div>
 <div class="field">
 <fmt:message key="label.vehicleEngineModel"/><br/>
 <h:text property="vehicle.vehicleEngineModel" styleId="vehicleEngineModel" style="width: 50"/> / <h:text property="vehicle.vehicleEngineNum" styleId="vehicleEngineNum" style="width: 90"/>
 </div>
 
 <h:errors property="vehicle.vehicleYear" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleColor" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleEngineModel" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleEngineNum" prefix="error.prefix" suffix="error.suffix"/>
 
 <div class="field" style="width: 220; position: relative; float: left;">
 <fmt:message key="label.vehiclePowerHpVt"/><br/>
 <h:text property="vehicle.vehiclePowerHp" styleId="vehiclePowerHp" style="width:50"/> / <h:text property="vehicle.vehiclePowerVt" styleId="vehiclePowerVt" style="width:90"/>
 </div>
 <div class="field" >
 <fmt:message key="label.vehicleEngineVol"/><br/>
 <h:text property="vehicle.vehicleEngineVol" styleId="vehicleEngineVol"/>
 </div>
  <div class="field" style="width: 220; position: relative; float: left;">
 <fmt:message key="label.vehicleWeightFull"/> <br/>
 <h:text property="vehicle.vehicleWeight" style="width: 50;" styleId="vehicleWeight"/> / <h:text property="vehicle.vehicleWeightMax" style="width: 90;" styleId="vehicleWeightMax"/>
 </div>
 
 <h:errors property="vehicle.vehiclePowerHp" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehiclePowerVt" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleEngineVol" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleWeight" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleWeightMax" prefix="error.prefix" suffix="error.suffix"/>
 
 
 <div class="field">
 <fmt:message key="label.vehicleChass"/><br/>
 <h:text property="vehicle.vehicleChassis" style="width: 270" styleId="vehicleChassis"/>
 </div>
 
 
 <h:errors property="vehicle.vehicleChassis" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleBodyNum" prefix="error.prefix" suffix="error.suffix"/>
 

 <div class="field" style="width: 220; position: relative; float: left;">
 <fmt:message key="label.vehicleRegDocName"/><br/>
 <h:select property="vehicle.vehicleRegDocType" styleId="vehicleRegDocName" style="width: 200"><h:optionsCollection property="regDocList" value="id" label="genDesc"/></h:select>
 </div>
 <div class="field">
 <fmt:message key="label.vehicleRegDocNum"/><br/>
 <h:text property="vehicle.vehicleRegDocNum" styleId="vehicleRegDocNum" style="width: 90"/> / <h:text property="vehicle.vehicleRegDocDate" styleId="vehicleRegDocDate" style="width: 120"/>
 </div>
 
 <h:errors property="vehicle.vehicleRegDocType" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleRegDocNameOther" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehiclePrice" prefix="error.prefix" suffix="error.suffix"/>
 

 <h:errors property="vehicle.vehicleRegDocNum" prefix="error.prefix" suffix="error.suffix"/>
 <h:errors property="vehicle.vehicleRegDocDate" prefix="error.prefix" suffix="error.suffix"/>

</div>

 </fieldset>
 <input type="submit" name="prev" value='<fmt:message key="label.prev"/>'>
  <input type="submit" name="next" value='<fmt:message key="label.next"/>'>
  