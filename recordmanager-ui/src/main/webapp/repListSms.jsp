<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@page import="ru.yar.vi.rm.data.BaseDO"%>

<div class="box">

<h:form action="/repList.do">
		<h:select property="dayId">
			<h:optionsCollection property="days" value="id" label="name"/>
		</h:select><h:select property="objectId">
			<h:optionsCollection property="objects" value="id" label="name"/>
		</h:select><h:submit><fmt:message key="label.report.retrieve"/></h:submit>
<div>
<p class="error">
		<h:errors/>
</p>
</div>

<l:notEmpty name="ReportForm" property="records">

<table border=1 cellspacing="0" id="repList">
<tr>
	<td></td>
	<td><fmt:message key="label.list.time"/></td>
	<td><fmt:message key="label.list.name"/></td>
	<td><fmt:message key="label.list.actionId"/></td>
	<!--<td><fmt:message key="label.list.docno"/></td>
	<td><fmt:message key="label.list.phone"/></td>-->
	<td><fmt:message key="label.info"/></td>
	<td><fmt:message key="label.phone"/></td>
</tr>
<jsp:useBean id="ReportForm" class="ru.yar.vi.rm.user.form.ReportForm" scope="session"/>
<c:forEach items="${ReportForm.records}" var="rec">
<jsp:useBean id="rec" class="ru.yar.vi.rm.data.StoredRecordDO"/>
<tr>
	<td><h:multibox property="selectedIds"><c:out value="${rec.id }"/></h:multibox></td>
	<td><%=UserHelper.formatTime(rec.getDay(),rec.getHour(),rec.getStart())%></td>
	<td><c:out value="${rec.name }"/></td>
	<td class="small"><%=rec.getAction().getName()%>&nbsp;</td>
	<td class="small"><pre><%=UserHelper.getInfoString(ReportForm.getActions(),rec)%></pre></td>
	<!--<td><c:out value="${rec.info.docNo}"/></td> 
	<td><c:out value="${rec.phone }"/> <c:out value="${rec.email }"/></td>-->
	<!-- td><c:out value="${rec.info.vehicleName }"/></td-->
	<!--<td><c:out value="${rec.info.vehicleNum }"/></td>-->
	<!-- td><c:out value="${rec.info.pts }"/></td-->
	<!--<td class="small"><%=rec.getRegion().getName()%>&nbsp;</td>-->
	<td><c:out value="${rec.phone }"/></td>
</tr>
</c:forEach>
</table>
	<h:textarea property="smsTemplate"></h:textarea>
	<h:submit property="action"><fmt:message key="label.sendNow"/></h:submit>
</l:notEmpty>
</h:form>
<l:empty name="ReportForm" property="records">
<fmt:message key="label.report.empty"/>
</l:empty>

<br/>
<h:form action="/welcome.do" method="post">
	<h:submit><fmt:message key="label.begin"/></h:submit>
</h:form>

</div>
