<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta"%>
<html lang="ru">
<head>
<meta http-equiv="content-type"
	content="text/html; charset=windows-1251" />
<title><c:out value="${AdvTerminalForm.nav.title}" /></title>
<script src="../include/jquery-1.10.2.min.js" type="text/javascript"></script>
<script type='text/javascript' src='../include/jquery.marquee-1.3.1.min.js'></script>
<script src="cfm.js" type="text/javascript"></script>
<script src="idletimer.js" type="text/javascript"></script>
<script src="util.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	initIdleTimer("?navId=${AdvTerminalForm.navId}",${AdvTerminalForm.nav.idleSecs > 0 ? AdvTerminalForm.nav.idleSecs : 30});
});
</script>

<link rel="stylesheet" type="text/css"href="../include/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="../custom/cfm.jsp" />
<link rel="stylesheet" type="text/css" href="record.css" />
<link rel="stylesheet" type="text/css"
	href="../include/font/glyphicons.css" />
</head>
<c:if test="${! empty AdvTerminalForm.nav.css}">
<style>
	<c:out value="${AdvTerminalForm.nav.css}"/>
</style>
</c:if>

<body>

	<div class="dashboard">
		<div class="panel-bg row">
			<div class="panel-header">
				<div class="panel-branding">
					<c:if test="${! empty  AdvTerminalForm.nav.logo }">
						<a href="?navId=${AdvTerminalForm.navId}&refresh=true"><img
							src="data:image/png;base64,<c:out value="${deta:encodeBase64(AdvTerminalForm.nav.logo)}" />"
							alt="" class="pull-left"></a>
					</c:if>
					<div class="pull-left">
						<c:out value="${AdvTerminalForm.nav.title}" escapeXml="false" />
					</div>
				</div>
				<div class="panel-current-time pull-right time"
					id="panel-current-time"></div>
			</div>
			<div class="col-xs-1">

				<div class="panel panel-button"	onclick="goTo('?navId=${AdvTerminalForm.navId}')">
					<p class=" glyphicon glyphicon-home"></p>
				</div>
				<c:forEach items="${AdvTerminalForm.nav.root.childs }" var="link">
					<c:if test="${! empty link.content || not empty link.childs }">
						<div class="panel panel-button"	onclick="goTo('?navId=${AdvTerminalForm.navId}&pageId=${link.id}')">
							<p class=" glyphicon ${link.icon}"></p>
						</div>
					</c:if>
				</c:forEach>
			</div>

			<div class="col-xs-23">
				<div class="panel">
					<c:if test="${AdvTerminalForm.pageId == 0 }">
						<div class="layout-container">
							<c:forEach items="${AdvTerminalForm.nav.root.childs }" var="link">
								<div>
									<c:if test="${! empty link.content || ! empty link.childs }">
										<button type="button" class="btn btn-large" onclick="goTo('?navId=${AdvTerminalForm.navId}&pageId=${link.id}')">
											<c:out value="${link.name }" />
										</button>
									</c:if>
									<c:if test="${empty link.content }">
										<h3 class="button">
											<c:out value="${link.name }" />
										</h3>
									</c:if>
								</div>
							</c:forEach>
						</div>
					</c:if>
					<c:if test="${AdvTerminalForm.pageId != 0 }">
						<c:if test="${not empty AdvTerminalForm.page.childs}">
						<div class="layout-container">
							<c:forEach items="${AdvTerminalForm.page.childs }" var="link">
								<div>
									<c:if test="${! empty link.content }">
										<button type="button" class="btn btn-large"	onclick="goTo('?navId=${AdvTerminalForm.navId}&pageId=${link.id}')">
											<c:out value="${link.name }" />
										</button>
									</c:if>
									<c:if test="${empty link.content }">
										<h3 class="button">
											<c:out value="${link.name }" />
										</h3>
									</c:if>
								</div>
							</c:forEach>
						</div>
						</c:if>
						
						<c:out value="${AdvTerminalForm.page.content.content }"
							escapeXml="false" />
					</c:if>
				</div>
			</div>
		</div>
		<script type="text/javascript">
	$(document).ready(function(){
		var f = function() {
			$("#scroll-panel").load("terminal.do?navId=${AdvTerminalForm.navId}&forward=scrolling");
		};
		f();
	});
	</script>
	<div class="scroll-panel" id="scroll-panel">

	</div>
	</div>
</body>
</html>