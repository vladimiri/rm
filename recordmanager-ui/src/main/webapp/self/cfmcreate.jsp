<%@page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="deta" uri="http://www.deta.pro/deta.tld"%>
<!doctype html>
<html>
<head>
<meta charset="utf8">
<style>
body {
	margin: 25px;
	padding: 0;
	overflow: hidden;
}

p {
	font: 18px "Helvetica Neue", Helvetica, Arial, sans-serif;
	text-align: center;
	margin: 0;
	padding: 0 0 8px 0;
}

.small {
	font-size: 12px;
}

.num {
	display: block;
	font-size: 60px;
	font-weight: bold;
}
</style>
<script type="text/javascript" src="../include/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="record.js"></script>
<c:if test="${! CFMCreateForm.debug }">
<script type="text/javascript">
	$(document).ready(function() {
		printPage(window);
	});
</script>
</c:if>
</head>
<body>
	<jsp:useBean id="CFMCreateForm"
		type="pro.deta.detatrak.user.form.CFMCreateForm" scope="request" />
	<c:if test="${! empty CFMCreateForm.record }">
		<c:out value="${deta:processTemplate(CFMCreateForm.template,CFMCreateForm,pageContext.out) }"/>
		
		<p class="small">
			<img
			src="data:image/gif;base64,R0lGODlhXAApANUAAP//////AP8A//8AAAD//wD/AAAA/wAAACMjIvj49+rq6ePj4uDg39TU08TEw8HBwL6+vbi4t6iop5ycm1NSUnp5efr5+d/e3r28vLq5ubW0tKWkpJiXl5CPj4uKiv7+/v39/fr6+vn5+ff39/T09PLy8u/v7+Xl5dra2s3NzcjIyK+vr6ysrJSUlISEhH9/f3Nzc2ZmZl9fX1hYWEhISDw8PDExMSwsLBsbGxcXFxUVFQ8PDwsLCwcHBwMDAwEBASwAAAAAXAApAAAG/0CAcEgsGo9I42HJZCaf0Kh0Oj0crdSsdpvFFr3csDgMHpbH6HTyDGCr32q2G04nX+vRpn7P7/ubV3+Cg35IFicPLjiEfDcdKgohbUtdjE06FRALI0iUUCAqNJYHMQ0fX55VozUPklCpUB8POYM2KJ2weYw8GiCqWSaifjCcuHOBtQqVWiExfRWna7mvgzQJWtNRHxR7FbrHSoI1vthsLh0PxUYii0w00UYmGR4zTlSCPCVIIiocMIHIdrRwVaQBEx8mjpCo4GMPslEHNBwBIYFWtotNbCgzwu2AiyMNLDoMB9EGvCEkhNlD9XBJDhJGDB5IWOREj0IkR0EwEuLGSP+WOZnUOAkARA4K69r1aUmIhwgjMvgADMokg5EXEou4EMR0UIyYS8N1PYCA3BAIF4p8gMCWrTM9Y/9IMKISrliqTBYUuXDtyYSfRCAeSFGEBE6gqPhgKBzlr93ElhoUPBxYTp8OWxwDwitILxEJlM1Y5jOXiNkkmldWHrVxiIPQQjDykUxkwekjqWUz6kFQyALYk8b26DskBU0iIZKHEMFhj+fVjL4WAaH0ceWxH4tMcEBX0M4i9SzdMgJa6l3ITHioG1LjhREGgv4VUdCQENKJ1VWL7qrCiIIDO/QmRG568EDcECwQ8lISDNS3GWLQLbHCER4s8cARW/kxoREdCIL6w3hJqHDTg9fldANthY1YlhEfYCCSHjoIKMQD+TEhw3FPMFADifsZkUMOMTggoxDhLcEBEiOsQIEOe3iApAYzMOnDDRWAKAUIKcCAgGVHDEnECnugKE0TYnZ5m1qLPTFkNlCkwEcPDFDTBJxigADDARsukwULDuqxA2FjztnfFglEtUQLZz7B5hEJ3PnHDxwkOgkfHHiJxAk7NjGDPr9EEUIEWxJSQwpETdrIA5ISUUIHI8LIwoHSPPHBAhyEOooNLLRmah83bHBCqfzA0GofOniAQqrBHaGCCzYItgcCM8BQ5CA4UADDtTHUMCwhO7wwKGfOhivuuEwEAQA7">
			<br> www.deta.pro
		</p>
	</c:if>
</body>
</html>