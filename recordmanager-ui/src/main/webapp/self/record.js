$(document).ready(function() {
	var frame = $('.popup iframe');
	frame.load(function () {
		if(frame.attr('src')) {
			$('.popup .popup-container').css('opacity', 1);
			$('.popup').fadeIn();
			setTimeout('kioskRefresh()', 1500);
			this.focus();
		}
	});
});

function kioskPrint(url) {
	var frame = $('.popup iframe');
	frame.attr('src',url);
}

function kioskRefresh() {
	$('.popup .popup-container').animate({
		top: 1024,
  		opacity: 0
  	}, 500, function() {
  		$('.popup').fadeOut();
  		$('.popup .popup-container').css('top','50%');
  	});
	var navId = getURLParameters(window.top,"navId");
	if(navId != null) {
		var arrParams = window.top.document.URL.toString().split("?");         
		window.top.location = arrParams[0]+"?navId="+navId;
	}
}

function getURLParameters(win,paramName) 
{
        var sURL = win.document.URL.toString();  
    if (sURL.indexOf("?") > 0)
    {
       var arrParams = sURL.split("?");         
       var arrURLParams = arrParams[1].split("&");      
       var arrParamNames = new Array(arrURLParams.length);
       var arrParamValues = new Array(arrURLParams.length);     
       var i = 0;
       for (i=0;i<arrURLParams.length;i++)
       {
        var sParam =  arrURLParams[i].split("=");
        arrParamNames[i] = sParam[0];
        if (sParam[1] != "")
            arrParamValues[i] = unescape(sParam[1]);
        else
            arrParamValues[i] = "No Value";
       }

       for (i=0;i<arrURLParams.length;i++)
       {
                if(arrParamNames[i] == paramName){
            //alert("Param:"+arrParamValues[i]);
                return arrParamValues[i];
             }
       }
       return null;
    }

}

function printPage(ifWin) {
	if (typeof jsPrintSetup != 'undefined' ) {
		// set portrait orientation
		jsPrintSetup.setOption('orientation',
		jsPrintSetup.kPortraitOrientation);
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 0);
		jsPrintSetup.setOption('marginBottom', 0);
		jsPrintSetup.setOption('marginLeft', 0);
		jsPrintSetup.setOption('marginRight', 0);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', '');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// clears user preferences always silent print value
		// to enable using 'printSilent' option
		jsPrintSetup.clearSilentPrint();
		// Suppress print dialog (for this context only)
		jsPrintSetup.setOption('printSilent', 1);
		// Do Print 
		// When print is submitted it is executed asynchronous and
		// script flow continues after print independently of completetion of print process!
		jsPrintSetup.printWindow(ifWin);
	} else {
		print();
	}
}
