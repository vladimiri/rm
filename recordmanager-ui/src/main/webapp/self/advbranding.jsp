<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>


<c:if test="${! empty AdvReportForm.report.css}">
<style>
<c:out value="${AdvReportForm.report.css}"/>
</style>
</c:if>
<c:if test="${! empty AdvReportForm.report.logo }">
<img src="data:image/png;base64,<c:out value="${deta:encodeBase64(AdvReportForm.report.logo)}" />" alt="" class="pull-left" id="logo">
</c:if>
<div class="pull-left"><c:out value="${AdvReportForm.report.name}" escapeXml="false" /></div>
