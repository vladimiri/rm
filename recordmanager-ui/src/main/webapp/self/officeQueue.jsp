<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script>
        var ws;
        $(document).ready(function(){
	      	initWS();
	      	$('.layout-head').SetScroller({
	        		velocity: <c:out value="${CFMStatusForm.speed}"/>,
	        		direction: 'horizontal',
	        		startfrom: 'right',
	        		loop: 'infinite',
	        		movetype: 'linear',
	        		onmouseover: 'pause',
	        		onmouseout: 'play',
	        		onstartup: 'play',
	        		cursor: 'default'
	        	});
	});

        var setInfoCommand="setInfo:";
        
	function initWS() {
		url = 'ws://<%=request.getServerName()%>:<%=request.getServerPort()%><%=request.getContextPath()%>/self/CFMDispatchServlet?officeId=<%=request.getParameter("officeId")%>';
		ws = new WebSocket(url);
			ws.onopen = function() {
			};
			ws.onmessage = function(message) {
				if(message.data.indexOf(setInfoCommand) == 0) {
					$(".scrollingtext").text(message.data.substr(setInfoCommand.length));
				} else {
					addQueueItem(message.data);
				}
			};
			ws.onclose = function() {
				if(lastQueueText != "Server connection lost!") {
					addQueueItem("Server connection lost!");
					restoreWS();
				}
			};
			
		return ws;
	}

	
	function restoreWS() {
		if(ws.readyState != 1) {
			initWS();
			setTimeout("restoreWS()",30000);
		}
	}
	function playSound(soundfile) {
		var myAudio = document.getElementById("myAudio");
		if (myAudio.paused) {
			myAudio.play();
		}
	}
	function aud_play_pause() {
		var myAudio = document.getElementById("myAudio");
		if (myAudio.paused) {
			myAudio.play();
		} else {
			myAudio.pause();
		}
	}
	var lastQueueText;
	function addQueueItem(queueItemText) {
		lastQueueText = queueItemText;
		newItem = $('<div>' + queueItemText + '</div>');
		newItem.addClass('queue-item');
		newItem.css('display', 'none')
		$('.queue').prepend(newItem);
		newItem.slideDown();
		playSound();
	}
</script>
<div class="layout-container">
	<div class="adv"><input type="button" onclick="addQueueItem()" value="�������� ������� �������">
	</div>
	<div class="queue"></div>
</div>

<audio id="myAudio">
<source src="gonghi.wav" type='audio/wav'>Your user agent does not support the HTML5 Audio element.</source>
</audio>
