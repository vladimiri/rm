<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="t" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="l" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="ru.yar.vi.rm.UserHelper"%>

<div class="popup-parameter" id="popup-parameter-${action.id }">
<div class="popup-parameter-bg"></div>
<div class="popup-parameter-container">
<form>
<table>
	<c:forEach items="${action.field }" var="item">
		<tr>
			<td class="name"><c:out value="${item.name }"/>
				<c:if test="${item.required > 0}"><fmt:message key="label.required"/>
				</c:if>
			</td>
			<td>
			<c:if test="${! empty item.criteria }">
				<select name="${item.field}" class="validator" required="${item.required}" requiredMsg="${item.requiredMsg}">
				<option value="0"><fmt:message key="label.choose.value"/></option>
				<c:forEach items="${item.criteria }" var="crit">
					<option value="${crit.id }"
						<c:if test="${UserForm[item.field] == crit.id }">selected</c:if>><c:out value="${crit.name }"/></option>
				</c:forEach>
				</select>
			</c:if>
			<c:if test="${empty item.criteria }">
				<input type="text" name="${item.field}" class="validator inputText keyboardInput" id="${item.field }" 
				maskMsg="${item.maskMsg }" mask="${item.mask }" required="${item.required}" requiredMsg="${item.requiredMsg}"/>
			</c:if>
			<c:if test="${ ! empty item.example }">
				<br/><fmt:message key="label.example"><fmt:param value="${item.example}"/></fmt:message>
			</c:if>
			</td>
		</tr>
	</c:forEach>
<!--
<h2>Request scope</h2>
<dl>
    <c:forEach items="${requestScope}" var="entry">
        <dt><c:out value="${entry.key}" /></dt>
        <dd><c:out value="${entry.value}" /></dd>
    </c:forEach>
</dl>
-->
	<tr>
		<td><button class="cancel"><fmt:message key="label.cancel"/></button></td>
		<td><button class="validate"><fmt:message key="label.enter"/></button></td>
	</tr>
</table>
</form>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
$("#popup-parameter-${action.id} button.validate").click(function (event) {
	var result = validate('#popup-parameter-${action.id}');
	if(result) { 
		$('#popup-parameter-${action.id}').fadeOut();
		$('#popup-parameter-${action.id} .popup-parameter-container').css('top','50%');
		kioskPrint('create.do?officeId=${CFMRecordForm.officeId}&actionId=${action.id}&'+result);
	}
	return false;
});
$("#popup-parameter-${action.id} button.cancel").click(function (event) {
	$('#popup-parameter-${action.id}').fadeOut();
	$('#popup-parameter-${action.id} .popup-parameter-container').css('top','50%');
});
});

</script>

