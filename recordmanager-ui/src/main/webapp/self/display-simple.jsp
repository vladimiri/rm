<%@page session="true" contentType="text/html; charset=windows-1251" %>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="b" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<jsp:useBean id="DisplayForm" class="ru.yar.vi.rm.user.form.DisplayForm" scope="request"/>

<c:if test="${ DisplayForm.objectId > 0 && ! empty DisplayForm.objectRecord}">


<c:forEach items="${DisplayForm.objectRecord }" var="objectRecord">

<div class="panel">
<c:if test="${! empty objectRecord.object}">
<h1><c:out value="${objectRecord.object.name }"/></h1>
	<c:if test="${! empty objectRecord.operName }">
		<div class="display-oper-name"><b:message key="label.display.operator" arg0="${objectRecord.operName }" arg1="${objectRecord.level}"/>
		</div>
	</c:if>
</c:if>


<div class="panel-reload">
<c:forEach items="${objectRecord.records }" var="rec" varStatus="status">

<div class="section">
 <span class="time"><fmt:formatDate  value="${rec.date}" pattern="HH:mm" /></span>
 <c:if test="${status.index == 0}">
  <span class="person"><span class="first-child"><c:out value="${rec.familyName}"/></span>
 	<span class="name"><span class="first-child"><c:out value="${rec.firstName}"/> <c:out value="${rec.middleName}"/></span></span>
 </span>
 
 </c:if>
 <c:if test="${status.index != 0}">
 <span class="person"><c:out value="${rec.familyName}"/>
 	<span class="name"><c:out value="${rec.shortName}"/></span>
 </span>
 
 </c:if>
 
</div>
</c:forEach>
</div>

</div>

</c:forEach>
 
</c:if>

<c:if test="${DisplayForm.isEmpty() }">
<!-- 
	<div class="display-date"><fmt:formatDate type="time" value="${DisplayForm.date}" /></div>
 -->
	<c:if test="${DisplayForm.random != -1 }">
		<img width="100%" height="100%" src="<%=DisplayForm.getPictures().get(DisplayForm.getRandom())%>"/>
	</c:if>
</c:if>

