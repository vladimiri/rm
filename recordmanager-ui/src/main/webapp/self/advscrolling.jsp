<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:if test="${! empty AdvReportForm.report.scrolling}">
		<script type="text/javascript">
		$(document).ready(function(){
	      	$('.scroll-panel').SetScroller({
	        		velocity: <c:out value="${AdvReportForm.report.scrollingSpeed}"/>,
	        		direction: 'horizontal',
	        		startfrom: 'right',
	        		loop: 'infinite',
	        		movetype: 'linear',
	        		onmouseover: 'pause',
	        		onmouseout: 'play',
	        		onstartup: 'play',
	        		cursor: 'default'
	        	});
		});</script>
		<div class="scroll"><c:out value="${AdvReportForm.report.scrolling}" /></div>
		</c:if>
