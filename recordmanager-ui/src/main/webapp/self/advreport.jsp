<%@page session="true" contentType="text/html; charset=windows-1251"%>
<%@page import="ru.yar.vi.rm.UserHelper"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://www.deta.pro/deta.tld" prefix="deta" %>
<!DOCTYPE html>
<html>
<head>
<title><c:out value="${AdvReportForm.report.title}" /></title>
<script src="../include/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="../include/jquery-scroller.js" type="text/javascript"></script>
<script src="cfm.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../include/bootstrap.min.css"/>
<link rel="shortcut icon" href="../favicon.png" type="image/png">

<link rel="stylesheet" type="text/css" href="../custom/cfm.jsp" id="cfmcss"/>

</head>
<body>
<div class="dashboard">
	<div class="panel-bg row">
		<div class="panel-header">
			<div class="panel-branding" id="panel-branding">
			</div>
			<div class="panel-current-time pull-right time" id="panel-current-time"></div>
		</div>
		<c:set var="count" value="${fn:substringBefore(12 /fn:length(AdvReportForm.report.objects),'.') }"/>
		

		<c:forEach items="${AdvReportForm.report.objects}" var="object" varStatus="status">
			<div class="col-sm-${object.width > 0 ? object.width : count }">
			<c:choose>
				<c:when test="${object.type == 'OFFICE_TYPE'}">
					<div class="panel">
						<h1><c:out value="${object.office.name }"/></h1>
						<div class="panel-reload" id="queue${status.index}"></div>
						<c:if test="${deta:hasPermission(pageContext.request,\"DEBUG\")}">
							<input type="button" onclick="ws${status.index}.addQueueItem('element')" value="�������� ������� �������">
						</c:if>
					</div>
					<audio id="audio${object.office.id}">
						<source src="filestorage.do?fileId=${object.office.sound.id}&filePath=/self/gonghi.wav&contentType=audio/wav" type="${not empty object.office.sound.contentType ? object.office.sound.contentType : 'audio/wav' }">Your user agent does not support the HTML5 Audio element.</source>
					</audio>
					
					<script type="text/javascript">
					var ws<c:out value="${status.index}"/>;
					$(document).ready(function(){ws${status.index} = initWS(${object.office.id},'#queue${status.index}','${AdvReportForm.report.blinkNum}','${AdvReportForm.report.blinkTime}');});
					</script>
				</c:when>
				<c:when test="${object.type == 'OBJECT_TYPE'}">
					<div id="object${status.index }" class="panel"></div>
					<script type="text/javascript">
					refreshDisplay('display-simple.do?objectId=${object.object.id}&${AdvReportForm.debug}','object${status.index }');
					window.setInterval('refreshDisplay(\'display-simple.do?objectId=${object.object.id}&${AdvReportForm.debug}\',\'object${status.index }\')',${! empty object.refresh ? object.refresh : 10}*1000);</script>
				</c:when>
				<c:when test="${object.type == 'ADV_TYPE'}">
					<div class="panel" style="overflow: hidden">
						<c:out value="${object.adv.html }" escapeXml="false"></c:out>
					</div>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			</div>
		</c:forEach>
	
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		var f = function() {
			$("#scroll-panel").load("report.do?reportId=${AdvReportForm.reportId}&forward=scrolling");
			$("#panel-branding").load("report.do?reportId=${AdvReportForm.reportId}&forward=branding");
			$("#cfmcss").attr("href",$("#cfmcss").attr("href"));
			if('${AdvReportForm.report.refreshTimeout}' > 0) {
				setTimeout(f,'${AdvReportForm.report.refreshTimeout}' );
			}
		};
		f();
	});
	</script>
	<div class="scroll-panel" id="scroll-panel">

	</div>


</div>

</body>
</html>