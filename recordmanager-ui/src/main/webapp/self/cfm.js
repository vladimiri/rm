var i = 0;

function refreshDisplay(url,containerId) {
	url = url +'&'+i;
	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}

	try {
		req.open("GET", url, false);
		req.send();
		if (req.status == 200 && req.readyState==4) {
			var f = document.getElementById(containerId);
			f.innerHTML = req.responseText;
		}
		i++;
	} catch(err) {
	}
};


$(document).ready(function(){
			var clock = document.getElementById('panel-current-time');
			
			var pad = function(x) {
				return x < 10 ? '0'+x : x;
			};
			var ticktock = function() {
				var d = new Date();
				
				var h = pad( d.getHours() );
				var m = pad( d.getMinutes() );
				var s = pad( d.getSeconds() );
				
				var current_time = [h,m,s].join(':');
				clock.innerHTML = current_time;
			};
			ticktock();
			
			// Calling ticktock() every 1 second
			setInterval(ticktock, 1000);
			});

	function initWS(officeId,containerId,blinkNum,blinkTime) {
		var arr = location.href.split("/");
		var context = arr[3];
		var url = 'ws://'+location.hostname+':'+location.port+'/'+context+'/self/CFMDispatchServlet?officeId='+officeId;
		var ws = getWS(url);
		
		if(ws == undefined)
			return;
		ws.onopen = function() {
			this.opened = true;
		};
		ws.onmessage = function(message) {
			var mes = JSON.parse(message.data);
			if(mes.mode == "INFO") {
				$(".scrollingtext").text(mes.message);
			} else if(mes.mode == "RECONNECT") {
				this.close();
			} else {
				this.addQueueItem(mes);
			}
		};
		
		ws.onclose = function() {
			if (this.opened)
				this.addQueueItem({objectId: 0,message: "Server connection lost!"});
			setTimeout(function() {
				initWS(officeId, containerId)
			}, 5000);
		};
		ws.addQueueItem = function(mes) {
			newItem = $('<div class="objectId'+mes.objectId+' blink">' + mes.message+'</div>');
			newItem.addClass('section');
			newItem.css('display', 'none')
			if(mes.distinctQueue) {
				$(containerId).find('.objectId'+mes.objectId).slideUp(1000);
			}
				
			$(containerId).prepend(newItem);
			newItem.slideDown();
			playSound('audio'+officeId);
		};
		return ws;
	}
	
	

		function getWS(url) {
		if(window.WebSocket){
			return new WebSocket(url);
		}
	}

	function playSound(soundfile) {
		var myAudio = document.getElementById(soundfile);
		if (myAudio.paused) {
			myAudio.play();
		}
	}
	function aud_play_pause() {
		var myAudio = document.getElementById("myAudio");
		if (myAudio.paused) {
			myAudio.play();
		} else {
			myAudio.pause();
		}
	}
