<%@ taglib uri="http://struts.apache.org/tags-html" prefix="h" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page errorPage="error.jsp" %>
<%@page import="ru.yar.vi.rm.UserHelper" contentType="text/html; charset=utf8"%>

<script type="text/javascript">
<!--
$( document ).ready( function() {
//	$('#ownerDocTextbox').hide();
	
	$('#ownerDocType').change(function() {
//		 if($(this).val() == 20) $('#ownerDocTextbox').show();
//		 else $('#ownerDocTextbox').hide();
		 if($(this).val() == 'REST') {
			 $('#ownerDoc').show();
		 } else {
			 $('#ownerDoc').hide();
		 }
		}).change();

	$('#personalDocType').change(function() {
//		 if($(this).val() == 20) $('#ownerDocTextbox').show();
//		 else $('#ownerDocTextbox').hide();
		 if($(this).val() == 'REST') {
			 $('#personalDoc').show();
		 } else {
			 $('#personalDoc').hide();
		 }
		}).change();

	$('#enableAgreement').change(function(){
		 if($(this).is(':checked')) {
			 $('#personalAgreement').show();
		 } else {
			 $('#personalAgreement').hide();
		 }
	}).change();
	
	$('#ownerCitizenship').change(function() {
		 if($(this).val() == 'OTHER') {
			 $('#ownerCitizenshipOther').show();
		 } else {
			 $('#ownerCitizenshipOther').hide();
		 }
		}).change();
	
	$('#personalCitizenship').change(function() {
		 if($(this).val() == 'OTHER') {
			 $('#personalCitizenshipOther').show();
		 } else {
			 $('#personalCitizenshipOther').hide();
		 }
		}).change();
	
	$('#vehicleRegDocName').change(function() {
		 if($(this).val() == 'OTHER') {
			 $('#vehicleRegDocNameOther').show();
		 } else {
			 $('#vehicleRegDocNameOther').hide();
		 }
	}).change();
	
	$('#action').change( function () {
		var action = $('#form1').attr('action');
		$("#form1").attr("action", action+ "?refresh=true");
		document.getElementById('form1').submit();
		$('#form1').attr('action',action);
	});
 
	
	
	$(".lastName").attr('placeholder', 'Фамилия');
	$(".firstName").attr('placeholder', 'Имя');
	$(".middleName").attr('placeholder', 'Отчество');
	$("#companyName").attr('placeholder', 'ООО Колокольчик');
	$('.birthDate').attr('placeholder', '07.08.1984');
	$('.date').attr('placeholder', '01.01.2013');
	$('.birthPlace').attr('placeholder', 'Ярославль');
	$('.docNum').attr('placeholder','1122333444');
	$('.docWhen').attr('placeholder','03.11.2011');
	$('.docWhere').attr('placeholder','Ленинским РОВД г. Ярославля');
	$('.address_apartment').attr('placeholder','15');
	$('.address_street').attr('placeholder','Белинского');
	$('.address_house').attr('placeholder','11');
	$('.address_town').attr('placeholder','Ярославль');
	$('.address_building').attr('placeholder','5');
	$('.address_district').attr('placeholder','РЫБИНСКИЙ');
	$('.address_post').attr('placeholder','150000');
	$('.phone').attr('placeholder','112233');
	$('.agreementIssuedBy').attr('placeholder','Нотариус Красноперекопского района');
	$('.agreementNumber').attr('placeholder','11313.12/12221');

	$('.inn').attr('placeholder','1234567890');
	$('.ogrn').attr('placeholder','1234567890');

	$('#vehicleDocNum').attr('placeholder','12ТС123456');
	$('#vehicleDocDate').attr('placeholder','17.07.2009');
	$('#vehicleModel').attr('placeholder','DAEWOO MATIZ');
	$('#vehicleSign').attr('placeholder','а001аа76');
	$('#vehicleVIN').attr('placeholder','XWB1A11СD1A123456');
	$('#vehicleManufacture').attr('placeholder','УЗ-ДЭУ');
	$('#vehicleEngineNum').attr('placeholder','123456ОН1');
	$('#vehicleEngineModel').attr('placeholder','F1CР');
	$('#vehicleYear').attr('placeholder','2007');
	$('#vehicleColor').attr('placeholder','Сине-зелёный');
	$('#vehiclePowerHp').attr('placeholder','51');
	$('#vehiclePowerVt').attr('placeholder','37.5');
	$('#vehicleEngineVol').attr('placeholder','796');
	$('#vehicleChassis').attr('placeholder','не заполнять в случае отсутствия');
	$('#vehicleBodyNum').attr('placeholder','XWB1A11СD1A123456');
	$('#vehicleWeight').attr('placeholder','810');
	$('#vehicleWeightMax').attr('placeholder','1210');
	$('#vehicleRegDocNum').attr('placeholder','12ТС123456');
	$('#vehicleRegDocDate').attr('placeholder','17.07.2009');
	$('#vehiclePrice').attr('placeholder','332000');

	
	
	$("#customerType").change(function() {
		if($(this).val() == '2') {
			$("#customerType1").show();
			$("#customerType2").hide();
		} else {
			$("#customerType2").show();
			$("#customerType1").hide();			
		}
	});
	if($("#customerType").val() == '2') {
		$("#customerType1").show();
		$("#customerType2").hide();
	} else {
		$("#customerType2").show();
		$("#customerType1").hide();			
	}
	//$('#ownerLName').on("")
});
//-->
</script>
<style>
<!--
input {
	width: default;
}
.field {
	font-size: 10pt;
	padding-top: 10px;
	width: 500px;
}
.inputText {
	width: 485px;
}
.star {
	font-size: 12px;
}
.ownerOptional {
	background-color: rgb(192,192,192);
	margin-top: 10px;
	padding-bottom: 10px;
}
-->
</style>
<div class="box">
<div>
<h2><fmt:message key="label.applicationService.header"/></h2>
<p>
<fmt:message key="label.applicationService.help"/>
</p>
</div>

<h:form action="/nextApplicationService.do" method="post" styleId="form1">
<c:if test="${ApplicationServiceForm.step == 0 }">
	<jsp:include page="app_service_step0.jsp"/>
</c:if>
<c:if test="${ApplicationServiceForm.step == 1 }">
	<jsp:include page="app_service_step1.jsp"/>
</c:if>

  <c:if test="${ApplicationServiceForm.step == 2}">
	<jsp:include page="app_service_step2.jsp"/>
 </c:if>
 
   <c:if test="${ApplicationServiceForm.step == 3}">
	<jsp:include page="app_service_step3.jsp"/>
  </c:if>
   <c:if test="${ApplicationServiceForm.step == 4}">
	<jsp:include page="app_service_step4.jsp"/>
  </c:if>
     <c:if test="${ApplicationServiceForm.step == 5}">
	<jsp:include page="app_service_step5.jsp"/>
  </c:if>
 
 </h:form>

<h:form action="/welcome.do" method="post">
<p>
	<h:submit><fmt:message key="label.begin"/></h:submit>
</p>
</h:form>

</div>