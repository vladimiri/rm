package pro.deta.detatrak.report;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

public class RecordAttachment {
	public static String junitReportAttach = System.getProperty("testJUnitReport");

	public static void record(String fileName, File path,byte[] value, CaptureType type){
		try {
//			Properties prop = System.getProperties();
//			for(Object p: System.getProperties().keySet()) {
//				System.out.println(""+p+" : " + System.getProperty((String)p));
//			}
//
//			System.out.println("ENVIRONMENTS:");
//			for(String p: System.getenv().keySet()) {
//				System.out.println(""+p+" : " + System.getenv(p));
//			}

//			String absPath = System.getenv("JENKINS_HOME")+File.separator+"jobs/"+System.getenv("JOB_NAME")+"/builds/"+System.getenv("BUILD_NUMBER")+"/archive/";
			FileUtils.writeByteArrayToFile(path, value);

			System.out.println();
			// according to https://wiki.jenkins-ci.org/display/JENKINS/JUnit+Attachments+Plugin
			System.out.println("[[ATTACHMENT|"+ path.getAbsolutePath() + "]]");
			System.out.println();
			System.out.println("<a href='"+junitReportAttach+File.separator+fileName+"'>");
			switch (type) {
			case PNG:
				System.out.println("<img src='"+junitReportAttach+File.separator+fileName+"' width=400 height=400/>");
			default:
				System.out.println(path.getName());
				break;
			}
			System.out.println("</a>");
			
		} catch (IOException e1) {
			System.out.println("unable to write'" + path + "':" + e1);
		}
	}
}
