package pro.deta.detatrak.test;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import pro.deta.detatrak.BaseITTest;
import pro.deta.detatrak.util.DataTest;

@RunWith(Parameterized.class)
public class CFMITCase extends BaseITTest {
	private Integer inputNumber;
	private Boolean expectedResult;


	public CFMITCase(Integer inputNumber, 
			Boolean expectedResult) {
		this.inputNumber = inputNumber;
		this.expectedResult = expectedResult;
	}

	@Parameterized.Parameters
	public static Collection primeNumbers() {
		return Arrays.asList(new Object[][] {
//				{ 2, true },
//				{ 6, false },
//				{ 19, true },
//				{ 22, false },
				{ 23, true }
		});
	}
	
	@Test
	public void testCFMTicket() {
		String ticketUrl="http://"+testServer+":"+testPort+"/"+testArtifactId+"/self/create.do?officeId="+DataTest.DEKABRISTOV_OFFICE+"&actionId="+DataTest.REG_WITH_CHECK_ACTION+"&info(name)=test&debug=true";
		System.out.println("!!!!!!! Testing CfmTicketURL: " + ticketUrl);
		getDriver().get(ticketUrl);
	}
}
