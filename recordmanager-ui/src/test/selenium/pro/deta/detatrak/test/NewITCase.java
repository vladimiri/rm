package pro.deta.detatrak.test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import pro.deta.detatrak.BaseITTest;
import pro.deta.detatrak.page.ChooseTimePageObject;
import pro.deta.detatrak.page.ConfirmPageObject;
import pro.deta.detatrak.page.MapPageObject;
import pro.deta.detatrak.page.Step1PageObject;
import pro.deta.detatrak.page.Step2PageObject;
import pro.deta.detatrak.page.Step3PageObject;
import pro.deta.detatrak.util.DataTest;

@RunWith(Parameterized.class)
public class NewITCase extends BaseITTest {
	private Integer inputNumber;
	private Boolean expectedResult;


	public NewITCase(Integer inputNumber, 
			Boolean expectedResult) {
		this.inputNumber = inputNumber;
		this.expectedResult = expectedResult;
	}

	@Parameterized.Parameters
	public static Collection primeNumbers() {
		return Arrays.asList(new Object[][] {
//				{ 2, true },
//				{ 6, false },
//				{ 19, true },
//				{ 22, false },
				{ 23, true }
		});
	}
	
//	@Rule
//    public DataSetRule rule = new DataSetRule(); // <-- this is used to access to the testVectors from inside the tests
//
//    public static class MyDataSet extends SimpleTestVectors {
//        @Override
//        protected Object[][] generateTestVectors() {
//            return new Object[][] {
//                    {true,  "alpha", new CustomProductionClass()}, // <-- this is a testVector
//                    {true,  "bravo", new CustomProductionClass()},
//                    {false, "alpha", new CustomProductionClass()},
//                    {false, "bravo", new CustomProductionClass() }
//            };
//        }
//    }

	@Test
//    @DataSet(testData = MyDataSet.class)
	public void testSearchReturnsResults() {
		Step1PageObject step1 = new MapPageObject(getDriver()).startRegistration();
		step1.selectAction(DataTest.REG_WITH_CHECK_ACTION);
		step1.selectCustomer(DataTest.PHIZ_CUSTOMER); 
		step1.selectRegion(DataTest.YAROSLAVL_REGION);
		step1.setAgreed(true);
		Step2PageObject page2 = step1.nextPage();
		page2.getNameInputElement().sendKeys("Иванов Иван Иванович");
		page2.getCriteria().selectByValue(DataTest.LIGHT_CAR);
		page2.getVehicleNumInputElement().sendKeys("А123АА76");
		page2.getVehicleNameInputElement().sendKeys("ВАЗ 2101");
		page2.getPtsInputElement().sendKeys("76АН123456");
		page2.getDocNoInputElement().sendKeys("7600000000");
		page2.getPhoneInputElement().sendKeys("112233");
		Step3PageObject page3 = page2.nextPage();
		page3.getCaptchaInputElement().sendKeys("07081981");
		ChooseTimePageObject chooseTime = page3.nextPage();
		chooseTime.getOffice().selectByValue(DataTest.DEKABRISTOV_OFFICE);
		chooseTime.getDay0().selectByValue("20141112");
		chooseTime.getDay1().selectByValue("20141111");
		chooseTime.getFree0().selectByValue("Nov 11, 2014-1 8:0 60 D:6 P:0");
		chooseTime.getFree1().selectByValue("Nov 11, 2014-10 9:0 60 D:12 P:0");
		ConfirmPageObject confirmation = chooseTime.nextPage();
		List<WebElement> confirmations = confirmation.getConfirmationElement();
		Assert.assertEquals(2, confirmations.size());
		for (WebElement webElement : confirmations) {
			System.out.println("" + webElement.getText());
		}
	}


}
