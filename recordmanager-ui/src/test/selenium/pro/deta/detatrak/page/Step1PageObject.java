package pro.deta.detatrak.page;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class Step1PageObject extends PageObject {
	@FindBy(xpath="//select[@name='customerId']")
	WebElement customerSelectElement;
	Select customer = new Select(customerSelectElement);
	@FindBy(xpath="//select[@name='regionId']")
	WebElement regionSelectElement;
	Select region = new Select(regionSelectElement);
	@FindBy(xpath="//select[@name='actionId']")
	WebElement actionSelectElement;
	Select action= new Select(actionSelectElement);
	
	@FindBy(xpath="//input[@name='agreed']")
	WebElement agreed;
	@FindBy(xpath="//button[@tabindex='-1']")
	WebElement prev;
	@FindBy(xpath="//input[@type='submit']")
	WebElement next;
	

	public Step1PageObject(PageObject d) {
		super(d);
	}

	public List<WebElement> getCustomerValues() {
		return customer.getOptions();
	}
	
	public List<WebElement> getRegionValues() {
		return region.getOptions();
	}	

	public List<WebElement> getActionValues() {
		return action.getOptions();
	}	

	public void selectCustomer(String value) {
		customer.selectByValue(value);
	}

	public void selectRegion(String value) {
		region.selectByValue(value);
	}

	public void selectAction(String value) {
		action.selectByValue(value);
	}
	
	public boolean isAgreed() {
		return agreed.isSelected();
	}
	
	public void setAgreed(boolean b) {
		agreed.click();
	}
	
	public Step2PageObject nextPage() {
		next.click();
		return new Step2PageObject(this);
	}
	
}
