package pro.deta.detatrak.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;



public class Step2PageObject extends PageObject {
	@FindBy(xpath="//input[@id='name']")
	WebElement nameInputElement;
	@FindBy(xpath="//select[@name='criteria']")
	WebElement criteriaSelectElement;
	@FindBy(xpath="//input[@id='info(vehicleNum)']")
	WebElement vehicleNumInputElement;
	@FindBy(xpath="//input[@id='info(vehicleName)']")
	WebElement vehicleNameInputElement;
	@FindBy(xpath="//input[@id='info(pts)']")
	WebElement ptsInputElement;
	@FindBy(xpath="//input[@id='info(documentNo)']")
	WebElement docNoInputElement;
	@FindBy(xpath="//input[@id='email']")
	WebElement emailInputElement;
	@FindBy(xpath="//input[@id='phone']")
	WebElement phoneInputElement;
	
	@FindBy(id="beginButton")
	WebElement beginButtonElement;
	@FindBy(id="backButton")
	WebElement backButtonElement;
	@FindBy(id="nextButton")
	WebElement nextElement;
	
	Select criteria = new Select(criteriaSelectElement);



	public WebElement getNameInputElement() {
		return nameInputElement;
	}



	public WebElement getCriteriaSelectElement() {
		return criteriaSelectElement;
	}



	public WebElement getVehicleNumInputElement() {
		return vehicleNumInputElement;
	}



	public WebElement getVehicleNameInputElement() {
		return vehicleNameInputElement;
	}



	public WebElement getPtsInputElement() {
		return ptsInputElement;
	}



	public WebElement getDocNoInputElement() {
		return docNoInputElement;
	}



	public WebElement getEmailInputElement() {
		return emailInputElement;
	}



	public WebElement getPhoneInputElement() {
		return phoneInputElement;
	}



	public WebElement getBeginButtonElement() {
		return beginButtonElement;
	}



	public WebElement getBackButtonElement() {
		return backButtonElement;
	}



	public WebElement getNextElement() {
		return nextElement;
	}



	public Select getCriteria() {
		return criteria;
	}

	public Step2PageObject(PageObject d) {
		super(d);
	
	}
	public Step3PageObject nextPage() {
		nextElement.click();
		return new Step3PageObject(this);
	}
	
}
