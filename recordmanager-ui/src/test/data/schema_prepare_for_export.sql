truncate table record cascade;

truncate table record_history;

truncate table gibdd_org;
truncate table gibdd_person;
truncate table gibdd_vehicle;
truncate table gibdd_quiz;

truncate table application;
truncate table application_service;
