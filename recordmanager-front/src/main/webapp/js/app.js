var View = function(title, templateName, data, selected, render) {
	this.title = title;
	this.templateName = templateName;
	this.data = data; 
	this.isSelected = ko.computed(function() {
		return this === selected();  
	}, this);
	this.render = render;
};

$('script[type="template/html"]').each(function(){
	$(this).hide();
	$(this).html($.ajax({
		type: "GET",
		url: $(this).attr("src"),
		cache: false,
		async: false,
		dataType: 'text'
	}).responseText);
});


var rootModel = function () {
	var self = this;
	self.selectedView= ko.observable();
	self.views= ko.observableArray([
	                                new View("Main", "mainTemplate", mainModel,self.selectedView),
	                                new View("Step1", "step1Template", step1Model,self.selectedView)
	                                ]);
	
	self.doLogIn = function(User) {
		self.views([
		            new View("Add meal", "addTemplate", addMealModel,rootModel.selectedView),
		            new View("List meals", "listTemplate", listMealModel,rootModel.selectedView),
		            new View("Meal calendar", "searchTemplate", calendarModel,rootModel.selectedView),
		            new View("User settings", "userSettingsTemplate", userSettingsModel,rootModel.selectedView)
		            ]);
		self.selectedView(self.views()[0]);
		self.registered(User);
		self.login(User.login);
	}
	
	
};
