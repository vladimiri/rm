package ru.yar.vi.rm.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;




public class ConfigDAO extends DAO {
	public static final Logger logger = Logger.getLogger(ConfigDAO.class);
	public static Object _lock = new Object();
	
	public String getConfig(String key) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			connect();
			pstmt= con.prepareStatement("select name,value from config where name = ?");
			pstmt.setString(1, key);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return rs.getString(2);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve action list",e);
		} finally {
			close(rs);
			close(pstmt);
			disconnect();
		}
		return null;
	}
	
	public String getConfig(String config,String defaultValue) {
		String result = getConfig(config);
		if(result == null)
			result = defaultValue;
		return result;
	}
	
}
