package ru.yar.vi.rm.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="generic_code")
public @Data @EqualsAndHashCode(callSuper=true) class GenCodeDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9105260998706010252L;
	@Column(name="gen_type")
	private String genType="";
	@Column(name="gen_desc")
	private String genDesc="";
	@Column(name="external_id")
	private String externalId="";
	private int sorter;
	private Integer visible;
	
}
