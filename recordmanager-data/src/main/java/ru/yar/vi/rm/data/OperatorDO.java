package ru.yar.vi.rm.data;

import lombok.Data;

public @Data class OperatorDO  {
	
	private String name;
	private String position;
	private String level;
	private int code;
}
