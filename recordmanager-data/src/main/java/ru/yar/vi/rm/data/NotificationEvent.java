package ru.yar.vi.rm.data;

public enum NotificationEvent {
	RECORD_FINISH,
	OPERATOR,
	CFM_TICKET;
	
	public String getCode() {
		return this.name();
	}
}
