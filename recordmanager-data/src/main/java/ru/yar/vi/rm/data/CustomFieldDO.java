package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="custom_field")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true,exclude= {"criteria"}) @ToString(exclude= {"criteria"}) class CustomFieldDO extends BaseDO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3544535052364002746L;
	@Column(name="customer_id")
	private Integer customerId;
	private int required;
	private String mask;
	@Column(name="mask_msg")
	private String maskMsg="";
	private String example="";
	private String field="";
	private String type="";
	private String reporting="";
	@Column(length=1024)
	private String requiredMsg="";
	/*
	 * <list name="criteria" table="custom_field_criteria" cascade="none">
			<key column="custom_field_id" />
			<list-index column="sorter"/>
			<many-to-many column="criteria_id"  class="CriteriaDO" />
		</list>
	 */
	@JoinTable(name = "custom_field_criteria", 
			joinColumns = @JoinColumn(name = "custom_field_id"), 
			inverseJoinColumns = @JoinColumn(name = "criteria_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany(cascade=CascadeType.MERGE)
	private List<CriteriaDO> criteria = new ArrayList<CriteriaDO>();
	private String onTerminal="";
}
