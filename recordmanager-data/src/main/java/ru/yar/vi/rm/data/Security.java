package ru.yar.vi.rm.data;


public enum Security {
	ALL(0),OPER(1);
	
	private int value;
//	public static final int ALL = 0;
//	public static final int OPER = 1;
	private Security(int value) {
		this.value= value;
	}
	public int getValue() {
		return value;
	}
	
}
