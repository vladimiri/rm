package ru.yar.vi.rm;

import java.lang.ref.SoftReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateKeyFormatConverter {
	private static final ThreadLocal<SoftReference<DateFormat>> threadLocal = new ThreadLocal<SoftReference<DateFormat>>();
	
	public static DateFormat getFormat() {
		SoftReference<DateFormat> ref = threadLocal.get();
		if (ref != null) {
			DateFormat result = ref.get();
			if (result != null) {
				return result;
			}
		}
		DateFormat result = initDateFormat();
		ref = new SoftReference<DateFormat>(result);
		threadLocal.set(ref);
		return result;
	}

	protected static DateFormat initDateFormat() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		return df;
	}

}
