package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="config")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class ConfigDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -805076825978370795L;
	@Column(length=2048)
	private String value="";
	@Column(length=1024)
	private String description="";
	
	
	public ConfigDO() {
	}
	
	public ConfigDO(Integer id,String name,String value,String description) {
		setId(id);
		setName(name);
		setValue(value);
		setDescription(description);
	}
}
