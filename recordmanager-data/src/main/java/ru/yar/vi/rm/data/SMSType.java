package ru.yar.vi.rm.data;

public enum SMSType {
	SECOND(2);
	
	private int value = 0;
	
	private SMSType(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getCode() {
		return this.name();
	}
}
