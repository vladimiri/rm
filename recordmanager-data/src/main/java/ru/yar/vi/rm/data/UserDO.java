package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;

@Entity
@Table(name="users")
@Cacheable(true)
@EntityListeners({DCNUpdateNotifier.class})
public @Data @ToString(exclude= {"office","role","site"}) class UserDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2079354678827162584L;
	private Integer id = 0;
    @Id
    @Column(name="login",unique=true)
	private String name="";
    
	private String pass="";
	private String description="";
	/*
	 * <list name="role" table="groups" cascade="save-update">
			<key column="login" />
			<list-index column="sorter"/>
			<many-to-many column="role" class="RoleDO"/>
		</list>
		<list name="site" table="user_site" cascade="save-update">
			<key column="login" />
			<list-index column="sorter"/>
			<many-to-many column="site_id" class="SiteDO"/>
		</list>
	 */
	@JoinTable(name = "groups", 
			joinColumns = @JoinColumn(name = "login"),
			inverseJoinColumns = @JoinColumn(name = "role")
	)
	@OrderColumn(name="sorter")
	@ManyToMany(fetch=FetchType.EAGER)
	private List<RoleDO> role = new ArrayList<RoleDO>();
	@JoinTable(name = "user_site", 
			joinColumns = @JoinColumn(name = "login"),
			inverseJoinColumns = @JoinColumn(name = "site_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<SiteDO> site = new ArrayList<SiteDO>();

	@JoinTable(name = "user_office", 
			joinColumns = @JoinColumn(name = "login"),
			inverseJoinColumns = @JoinColumn(name = "office_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany(fetch=FetchType.EAGER)
	private List<OfficeDO> office = new ArrayList<OfficeDO>();
	
}
