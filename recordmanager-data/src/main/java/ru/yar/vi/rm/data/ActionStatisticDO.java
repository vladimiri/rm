package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

public @Data @EqualsAndHashCode(callSuper=true) class ActionStatisticDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7840048334392554183L;
	private ActionDO action;
	private Date lastCreationDate;
	private List<StoredRecordDO> queue = new ArrayList<StoredRecordDO>();
	private int cfmQueueSize;
	private boolean officeClosed;
	
	
}
