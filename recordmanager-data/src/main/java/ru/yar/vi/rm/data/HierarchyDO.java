package ru.yar.vi.rm.data;

import java.util.List;

public interface HierarchyDO<E> {
	public List<E> getChilds();
	public E	getParent();
	public void	setParent(E parent);
}
