package ru.yar.vi.rm.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@MappedSuperclass
public @Data @EqualsAndHashCode(callSuper=true) @ToString(callSuper=true) class BaseDO extends BaseIdDO implements Serializable,ObjectIdentifiable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1620698643508612830L;
    @Column(length=300)
	private String name="";
	
	public BaseDO() {
	}
	
	public BaseDO(int id2, String name2) {
		setId(id2);
		setName(name2);
	}
	
	public BaseDO(String name2) {
		setName(name2);
	}
	
	public static String[] stringToArr(String str ) {
		if(str == null || "".equalsIgnoreCase(str))
			return new String[0];
		str = str.replaceFirst("^,", "");
		String vList[] = str.split(",");
		return vList;
	}
	
	public static String arrayToStr(String[] array) {
		if(array == null || array.length == 0)
			return null;
		String actions = ",";
		if(array != null && array.length > 0)
			for (String el : array) {
				if(el == null || "".equalsIgnoreCase(el))
					continue;
				actions += el+",";
			}
		return actions;
	}
	
	public static String join(String[] array) {
		StringBuffer ret = new StringBuffer();
		if(array != null && array.length > 0) {
			ret.append(array[0]);
			for (int i = 1; i < array.length; ++i) {
				ret.append(", "+array[i]);
			}
		}
		return ret.toString();
	}

}
