package ru.yar.vi.rm.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="send_sms")
public @Data class SMSDO {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="sql_id")
	private int sqlId;
	
	private String momt = null;
	private String sender = null;
	private String receiver = null;
	private String msgdata = "";
	private long time;
	@Column(name="sms_type")
	private int smsType = 0;
	@Column(name="smsc_id")
	private String smscId = null;
	private int coding;
	private String charset;
	
}
