package ru.yar.vi.rm.data;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "office")
@Cacheable(true)
public @Data @ToString(exclude= {"sound","serviceRegionList","serviceActionList"}) @EqualsAndHashCode(exclude= {"sound","serviceRegionList","serviceActionList"}) class OfficeDO extends BaseDO {
	private static final long serialVersionUID = -2403292827097976376L;
	private String schedule="";
	private int security;
	@Column(name="time_diff")
	private int timeDiff;
	private String okato="";
	@Column(name="default_office_id")
    private Integer defaultOfficeId;
	@Column(name="overlap_periods")
	private Boolean overlapPeriods = false;
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "sound_id")
	private FilestorageDO sound;
	private boolean distinctQueue;
	
	@JoinTable(name = "office_region", 
			joinColumns = @JoinColumn(name = "office_id"), 
			inverseJoinColumns = @JoinColumn(name = "region_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<RegionDO> serviceRegionList;
	@JoinTable(name = "office_action", 
			joinColumns = @JoinColumn(name = "office_id"), 
			inverseJoinColumns = @JoinColumn(name = "action_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<ActionDO> serviceActionList;
	
	@Column(length=4096)
	private String information;

}
