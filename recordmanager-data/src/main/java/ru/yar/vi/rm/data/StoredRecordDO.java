package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;
import lombok.ToString;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import pro.deta.detatrak.util.JsonMapType;

@Entity
@Table(name="record")
@TypeDefs({@TypeDef( name= "JsonObject", typeClass = JsonMapType.class)})
@Data @ToString(exclude= {"action","criteria","customer","object","office","recordHistory","region","extendedProperties","info"})
public class StoredRecordDO implements Serializable {
	public static final String RECORD_STATUS_SUCCESS="S"; // Используется для предварительной записи - запись создана
	public static final String RECORD_STATUS_DELETED="D"; // Используется для предварительной записи - запись удалена
	public static final String RECORD_STATUS_FINAL="F"; // Используется для электронной очереди - запись обработана
	public static final String RECORD_STATUS_WAITING="W"; // Используется для электронной очереди - запись ожидает приглашения
	public static final String RECORD_STATUS_REDIRECT = "R"; // Используется для электронной очереди - запись была переведена на другую услугу
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2512290443553900520L;
	@Id
	@GeneratedValue(generator="record_seq")
	@SequenceGenerator(name="record_seq",sequenceName="record_seq", allocationSize=1)
	private int id;
	
	@ManyToOne(fetch = FetchType.LAZY) // optional=false is inappropriate for CFM
	@JoinColumn(name = "object_id")
	private ObjectDO object;
	private Date day;
	private int hour;
	@Column(name="start_time")
	private int start;
	@Column(name="end_time")
	private int end;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "action_id")
	private ActionDO action;
	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "customer_id")
	private CustomerDO customer;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "criteria_id")
	private CriteriaDO criteria;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region_id")
	private RegionDO region;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "office_id")
	private OfficeDO office;
	private String name;
	private String email;
	private String phone;
	private String key;
	private String status;
	private String author;
	@ElementCollection(fetch=FetchType.EAGER)
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="record_map_info", joinColumns=@JoinColumn(name="id"))
	private Map<String, String> info;
	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	@Column(name="creation_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@Column(name="final_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date finalDate;
	@Transient
	private String tsv;
	@ElementCollection
	  @CollectionTable(
	        name="record_history",
	        joinColumns=@JoinColumn(name="ID")
	  )
	private List<RecordHistoryDO> recordHistory = new ArrayList<RecordHistoryDO>();
	@Type(type = "JsonObject")
	private Map<String,Object> extendedProperties = new HashMap<String, Object>();

	@Transient
	public Date getDate() {
		Date dt = new Date(day.getTime());
		dt.setHours(getHour());
		dt.setMinutes(getStart());
		return dt;
	}
}
