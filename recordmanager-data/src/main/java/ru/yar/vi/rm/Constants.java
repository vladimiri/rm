package ru.yar.vi.rm;

public class Constants {
	public static final String CURRENT_DAY_RECORD = "currentDayRecord";
	public static final String DAYS_TO_REGISTER_CHECK_FREE = "daysToRegisterCheckFree";
	public static final String REPORT_LIST_DAYS = "reportListDays";
	public static final String INTERVAL_TO_PREVENT_EARLY_RESTORE = "intervalToPreventEarlyRestore";
	public static final int CUSTOMER_FIZ = 1;
	public static final String CONFIG_REQUEST_ATTRIBUTE = "DETAConfig";
	public static final String HELPER_REQUEST_ATTRIBUTE = "DETAUserHelper";
	public static final String ENTITY_MANAGER_ATTRIBUTE = "em";
	public static final String ENTITY_MANAGER_FACTORY_ATTRIBUTE = "emf";
	public static final String SITE = "SITE";
	public static final String SITE_ID = "SITE_ID";
	public static final String DETATRAK_DEBUG = "DETATRAK_DEBUG";
	public static final String PERSISTENT_UNIT = "rm";
	
}
