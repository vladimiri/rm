package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name = "schedule")
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"actions","criterias","customer","object"}) class ScheduleDO extends BaseIdDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1504971430393027376L;
	@ManyToOne(fetch = FetchType.LAZY,optional=false)
    @JoinColumn(name = "object_id")
	private ObjectDO object;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	private CustomerDO customer;
	
	@Column(name = "security")
	private int security;
	
	@Column(name="start_date")
	@Temporal(TemporalType.DATE)
	private Date start;
	@Column(name="end_date")
	@Temporal(TemporalType.DATE)
	private Date end;

	@Column(name="schedule")
	private String schedule="";

	@JoinTable(name = "schedule_criteria", 
			joinColumns = @JoinColumn(name = "schedule_id"), 
			inverseJoinColumns = @JoinColumn(name = "criteria_id")
	)
	@ManyToMany(fetch=FetchType.LAZY)
	@OrderColumn(name="sorter")
	private List<CriteriaDO> criterias = new ArrayList<CriteriaDO>();

	@JoinTable(name = "schedule_action", 
			joinColumns = @JoinColumn(name = "schedule_id"), 
			inverseJoinColumns = @JoinColumn(name = "action_id")
	)
	@ManyToMany(fetch=FetchType.LAZY)
	@OrderColumn(name="sorter")
	private List<ActionDO> actions = new ArrayList<ActionDO>();
}
