package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * This object represent hierarchy in the menu list.
 * 
 * @author VIlmov
 *
 */
@Entity
@Table(name="terminal_link")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"childs","content","parent"}) class TerminalLinkDO extends BaseDO implements Serializable,HierarchyDO<TerminalLinkDO>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6179970673448639L;

	private String icon;

	@ManyToOne(fetch = FetchType.LAZY, optional = true)
	@JoinColumn(name = "content_id")
	private TerminalPageDO content;
	
	@ManyToOne(optional=true)
	@JoinColumn(name="parent_id")
	private TerminalLinkDO parent;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent",fetch=FetchType.EAGER)
	@OrderColumn(name="sorter")
	private List<TerminalLinkDO> childs;
	
	private Integer sorter;

}
