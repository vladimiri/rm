package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.ToString;

public @Data @ToString(exclude= {"officeDays"}) class RecordDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3533565335967824175L;
	private int id;
	private boolean selected = false;
	
	private ObjectTypeDO objectType;

	private int day;
	private int hourId;
	private String key;
	private boolean noObjects = false;
	private boolean required = false;
	private String freeSpaceId;
	private FreeSpaceDO freeSpace;
	// map key is Long because JSTL can't operate with integers.
	private Map<Long,List<DateBaseDO>> officeDays = new HashMap<Long, List<DateBaseDO>>(); 
	
	
	
	
	public String getObjectTypeName() {
		return objectType.getName();
	}
	
	public List<FreeSpaceDO> getSelectedFSList(long officeId) {
		List<DateBaseDO> days = officeDays.get(officeId);
		if(days != null && !days.isEmpty()) {
			for (DateBaseDO day1 : days) {
				if(day1.getId() == this.day || this.day == 0) {
					return day1.getFsList();
				}
			}
		}
		return new ArrayList<FreeSpaceDO>();
	}
	
}
