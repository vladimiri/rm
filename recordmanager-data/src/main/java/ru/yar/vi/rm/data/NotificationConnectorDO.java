package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="notification_connector")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class NotificationConnectorDO extends BaseDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1680469370876624693L;
	@Column(name="TYPE",insertable=false,updatable=false)
	private String type;
	
	
}
