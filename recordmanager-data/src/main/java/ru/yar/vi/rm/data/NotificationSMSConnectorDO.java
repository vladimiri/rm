package ru.yar.vi.rm.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("SMS")
public @Data @EqualsAndHashCode(callSuper=true) class NotificationSMSConnectorDO extends
		NotificationConnectorDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5100151012031538687L;
	private SMSType smsType;
	private SMSEncoding smsEncoding; 
	private String smsCharset;
	private String momt;
	private String smscId;
	private String sender;
	
}
