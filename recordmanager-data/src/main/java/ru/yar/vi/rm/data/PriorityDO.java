package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pro.deta.detatrak.dao.listener.DCNUpdateNotifier;

@Entity
@Table(name="priority")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"type","object"}) class PriorityDO extends BaseIdDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2403292827097976376L;
	
	@Column
	private int priority;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "action_id")
	private ActionDO type;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "object_id")
	private ObjectDO object;

	
}
