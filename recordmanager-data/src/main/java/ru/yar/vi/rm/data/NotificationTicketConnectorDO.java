package ru.yar.vi.rm.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("TICKET")
public class NotificationTicketConnectorDO extends
		NotificationConnectorDO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8450127532801972977L;
	
}
