package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

public @Data class DisplayValueDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3142746712190525935L;
	private Date date;
	private String value;
	private String familyName;
	private String firstName;
	private String middleName;
	private String shortName;
	
}
