package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="object_type")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class ObjectTypeDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2403292827097976376L;
	private String type="";
	private String description;
	
}
