package ru.yar.vi.rm.data;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name="criteria")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) class CriteriaDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6164440846441055752L;

	public CriteriaDO() {
	}

	public CriteriaDO(String string) {
		super(string);
	}

	
}
