package ru.yar.vi.rm.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Embeddable
public @Data class RecordHistoryDO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1778407045336269778L;
	@Column(name="name")
    private String name="";
	@Column(name="date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;
	@Column(name="status")
	private String status;
	@Column(name="source")
	private String source;
	
	public RecordHistoryDO() {
		
	}
	
	public RecordHistoryDO(String author, String remoteHost, String status2) {
		setName(author);
		this.source = remoteHost;
		this.status = status2;
		this.date = new Date();
	}
	
}
