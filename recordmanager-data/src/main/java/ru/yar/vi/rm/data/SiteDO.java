package ru.yar.vi.rm.data;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity
@Table(name="site")
@Cacheable(true)
public @Data @EqualsAndHashCode(callSuper=true) @ToString(exclude= {"actions","customers","offices","regions"}) class SiteDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2703420378494358167L;

	private String intro="";
	private String info="";
	private String description="";
	
	@Column(name="final_text")
	private String finalText="";
	
	@JoinTable(name = "site_action", 
			joinColumns = @JoinColumn(name = "site_id"), 
			inverseJoinColumns = @JoinColumn(name = "action_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<ActionDO> actions = new ArrayList<ActionDO>();
	
	@Column(name="http_keywords")
	private String httpKeywords="";
	@Column(name="http_description")
	String httpDescription="";
	@Column(name="http_title")
	String httpTitle="";
	@Column(name="http_template")
	String httpTemplate="";
	
	@JoinTable(name = "site_office", 
			joinColumns = @JoinColumn(name = "site_id"), 
			inverseJoinColumns = @JoinColumn(name = "office_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<OfficeDO> offices = new ArrayList<OfficeDO>();
	
	@JoinTable(name = "site_customer", 
			joinColumns = @JoinColumn(name = "site_id"), 
			inverseJoinColumns = @JoinColumn(name = "customer_id")
	)
	@OrderColumn(name="sorter")
	@OneToMany(cascade= {CascadeType.MERGE})
	private List<CustomerDO> customers = new ArrayList<CustomerDO>();
	
	@JoinTable(name = "site_region", 
			joinColumns = @JoinColumn(name = "site_id"), 
			inverseJoinColumns = @JoinColumn(name = "region_id")
	)
	@OrderColumn(name="sorter")
	@ManyToMany
	private List<RegionDO> regions = new ArrayList<RegionDO>();
	private String main="";
	private String selfCss="";
	private String styleCss="";
	private String cfmCss="";

	private String contact="";
	
	@ElementCollection(targetClass=String.class,fetch=FetchType.EAGER)
	@CollectionTable(name = "site_url", 
			joinColumns = @JoinColumn(name = "site_id") 
	)
	@Column(name="url")
	@OrderColumn(name="sorter")
	private List<String> urlMapping = new ArrayList<String>();
	
}
