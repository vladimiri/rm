package ru.yar.vi.template;

import java.io.IOException;
import java.util.HashMap;

/**
* A cache manager for MiniTemplator objects.
* This class is used to cache MiniTemplator objects in memory, so that
* each template file is only read and parsed once.
*
*/
public class MiniTemplatorCache {

private HashMap<String,MiniTemplator> cache;               // buffered MiniTemplator objects

/**
* Creates a new MiniTemplatorCache object.
*/
public MiniTemplatorCache() {
   cache = new HashMap<String,MiniTemplator>(); }

/**
* Returns a cloned MiniTemplator object from the cache.
* If there is not yet a MiniTemplator object with the specified <code>templateFileName</code>
* in the cache, a new MiniTemplator object is created and stored in the cache.
* Then the cached MiniTemplator object is cloned and the clone object is returned.
* @param  templateSpec      the template specification.
* @return                   a cloned and reset MiniTemplator object.
*/
public synchronized MiniTemplator get (MiniTemplator.TemplateSpecification templateSpec)
      throws IOException, MiniTemplator.TemplateSyntaxException {
   String key = generateCacheKey(templateSpec);
   MiniTemplator mt = cache.get(key);
   if (mt == null) {
      mt = new MiniTemplator(templateSpec);
      cache.put (key, mt); }
   return mt.cloneReset(); }

private static String generateCacheKey (MiniTemplator.TemplateSpecification templateSpec) {
   StringBuilder key = new StringBuilder(128);
   if (templateSpec.templateText != null)
      key.append (templateSpec.templateText);
    else if (templateSpec.templateFileName != null)
      key.append (templateSpec.templateFileName);
    else
      throw new IllegalArgumentException("No templateFileName or templateText specified.");
   if (templateSpec.conditionFlags != null) {
      for (String flag : templateSpec.conditionFlags) {
         key.append ('|');
         key.append (flag.toUpperCase()); }}
   return key.toString(); }

/**
* Clears the cache.
*/
public synchronized void clear() {
   cache.clear(); }

} // end class MiniTemplatorCache
