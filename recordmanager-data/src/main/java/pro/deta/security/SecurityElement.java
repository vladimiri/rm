package pro.deta.security;

import pro.deta.detatrak.util.Messages;
import ru.yar.vi.rm.data.ObjectIdentifiable;


public enum SecurityElement implements ObjectIdentifiable {
	ALL("ALL",Messages.getString("SecurityElement.ALL")),
	
	// Dynamic elements
	SELF_MODULE("SELF",Messages.getString("SecurityElement.SELF"),true),
	OPER_MODULE("OPER",Messages.getString("SecurityElement.OPER"),true),
	MAIN_MODULE("MAIN_MODULE",Messages.getString("SecurityElement.MAIN_MODULE"),true),
	OFFICE_UNRESTRICTED("OFFICE_UNRESTRICTED",Messages.getString("SecurityElement.OFFICE_UNRESTRICTED")),
	
	// Static elements;
	COMPARSION_REPORT("COMPARSION_REPORT",Messages.getString("SecurityElement.COMPARSION_REPORT")),
	CFM("CFM",Messages.getString("SecurityElement.CFM")),
	APPLICATION_SERVICE("APPLICATION_SERVICE",Messages.getString("SecurityElement.APPLICATION_SERVICE"),MAIN_MODULE),
	PAYMENT("PAYMENT",Messages.getString("SecurityElement.PAYMENT"),MAIN_MODULE),
	APPLICATION("APPLICATION",Messages.getString("SecurityElement.APPLICATION"),MAIN_MODULE),
	SELF_PRINT("SELF_PRINT",Messages.getString("SecurityElement.SELF_PRINT"),SELF_MODULE),
	
	// Access to operator
	OPER_ACCESS("OPER_ACCESS",Messages.getString("SecurityElement.OPER_ACCESS")),
	OPER_SMS("OPER_SMS",Messages.getString("SecurityElement.OPER_SMS")),
	
	// Tabs for vadmin
	ADMIN_ACCESS("ADMIN_ACCESS",Messages.getString("SecurityElement.ADMIN_ACCESS")),
	ADMINISTRATION_TAB("ADMINISTRATION_TAB",Messages.getString("SecurityElement.ADMINISTRATION_TAB"),ADMIN_ACCESS,OFFICE_UNRESTRICTED), 
	SERVICES_TAB("SERVICES_TAB",Messages.getString("SecurityElement.SERVICES_TAB"),ADMIN_ACCESS,OFFICE_UNRESTRICTED), 
	EXTRA_TAB("EXTRA_TAB",Messages.getString("SecurityElement.EXTRA_TAB"),ADMIN_ACCESS,OFFICE_UNRESTRICTED), 
	OBJECTS_TAB("OBJECTS_TAB",Messages.getString("SecurityElement.OBJECTS_TAB"),ADMIN_ACCESS), 
	SCHEDULE_TAB("SCHEDULE_TAB",Messages.getString("SecurityElement.SCHEDULE_TAB"),ADMIN_ACCESS), 
	
	// Others
	DEBUG("DEBUG",Messages.getString("SecurityElement.DEBUG")),
	;

	/**
	 * Требуемые элементы для совпадения.
	 */
	private SecurityElement[] required;
	/**
	 * Описание элемента, его назначение
	 * @TODO сделать в локализации, пока не сложно, но если часто менять - неудобно
	 */
	private String description;
	private boolean dynamic; 

	
	private SecurityElement(String code,SecurityElement... el ) {
		this.code = code;
		this.required = el;
	}

	private SecurityElement(String code,String description,SecurityElement... el ) {
		this.code = code;
		this.required = el;
		this.description = description;
	}
	
	private SecurityElement(String code) {
		this.code = code;
	}
	
	private SecurityElement(String code,String description) {
		this.code = code;
		this.description = description;
	}
	private SecurityElement(String code,String description,boolean dynamic) {
		this.code = code;
		this.description = description;
		this.dynamic = dynamic;
	}
	
	public SecurityElement[] getRequired() {
		return required;
	}

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public boolean match(SecurityElement security) {
		boolean base = code != null && code.equalsIgnoreCase(security.code);
		// для динамических элементов совпадения с ALL быть не должно
		if(!security.isDynamic())
			base = base || (this == ALL);
		return base;
	}
	
	public int getId() {
		   return ordinal();
    }
	public Object getObjectId() {
		   return ordinal();
	}
	public boolean isDynamic() {
		return dynamic;
	}

	public String getDescription() {
		return description;
	}

}
