package pro.deta.detatrak.dao.listener;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.PooledConnection;

import org.apache.log4j.Logger;
import org.hibernate.jdbc.Work;
import org.postgresql.PGConnection;

import com.google.gson.Gson;

public class DCNUpdateListener extends Thread implements Work {
	private static final Logger logger = Logger.getLogger(DCNUpdateListener.class);
	private PGConnection pgconn;
	private Connection conn;
	private String channel;
	private DCNCallback callback;


	public DCNUpdateListener(String channel,DCNCallback callback) {
		this.channel = channel;
		this.callback = callback;
		
	}

	@Override
	public void execute(Connection paramConnection) throws SQLException {
		pgconn = (org.postgresql.PGConnection)((PooledConnection) paramConnection).getConnection();
		conn = paramConnection;
		Statement stmt = paramConnection.createStatement();
		stmt.execute("LISTEN "+channel);
		stmt.close();
	}

	public void run() {
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Gson g = new Gson();
			while (!this.isInterrupted()) {
				try {
					// issue a dummy query to contact the backend
					// and receive any pending notifications.
					stmt = conn.createStatement();
					rs = stmt.executeQuery("SELECT 1");
					rs.close();
					rs = null;
					stmt.close();
					stmt = null;

					org.postgresql.PGNotification notifications[] = pgconn.getNotifications();
					if (notifications != null) {
						for (int i=0; i<notifications.length; i++) {
							DCNParameters param = g.fromJson(notifications[i].getParameter(), DCNParameters.class);
							param.setChannel(notifications[i].getName());
							callback.perform(param);
						}
					}
					// wait a while before checking again for new
					// notifications
					Thread.sleep(500);
				} catch (SQLException sqle) {
					logger.error("DCNUpdateListener exception ",sqle);
					return;
				} catch (InterruptedException ie) {
					return;
				}
			}
		} finally {
			if(rs != null)
				try {
					rs.close();
				} catch (SQLException e1) {
					logger.error("Error while finishing DCNUpdateListener", e1);
				}
			if(stmt != null)
				try {
					stmt.close();
				} catch (SQLException e1) {
					logger.error("Error while finishing DCNUpdateListener", e1);
				}
			
			conn = null;
			pgconn = null;
			callback = null;
		}
	}

}
