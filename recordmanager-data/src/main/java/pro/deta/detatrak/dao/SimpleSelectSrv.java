package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class SimpleSelectSrv<O> extends AbstractNewSQLSrv<O> {

    public SimpleSelectSrv(String sql) {
        super(sql);
    }

    @Override
    protected void fillStatementParam(PreparedStatement stmt) throws SQLException {
    }
}
