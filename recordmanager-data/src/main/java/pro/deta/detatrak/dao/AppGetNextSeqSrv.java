package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

/**
 * @author VIlmov Vladimir I.
 *         Aug 8, 2012 8:58:44 PM
 */
public class AppGetNextSeqSrv {
    private static final Logger logger = Logger.getLogger(AppGetNextSeqSrv.class);

    private String sequence;

    public AppGetNextSeqSrv (String sequence) {
        this.sequence = sequence;
    }

    public long execute (DAO dao) throws DAOException {
        ResultSet rs = null;
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = "SELECT " + sequence + ".NEXTVAL FROM DUAL";
            pstmt = dao.prepareStatement(sql);

            logger.debug("Filling statement with parameters: " + sequence);
            rs = pstmt.executeQuery();
            if (rs.next())
                return rs.getLong(1);
            else
                throw new DAOException("Can't get sequence <" + sequence + "> from ResultSet");
        } catch (Exception e) {
            logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + sequence, e);
            throw new DAOException(e);
        } finally {
            dao.close(rs);
            dao.close(pstmt);
        }
    }
}
