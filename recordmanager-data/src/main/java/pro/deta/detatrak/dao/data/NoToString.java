package pro.deta.detatrak.dao.data;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author VIlmov Vladimir I.
 *         Jul 30, 2012 1:23:13 PM
 */
@Retention(value = RetentionPolicy.RUNTIME)
public @interface NoToString {

}
