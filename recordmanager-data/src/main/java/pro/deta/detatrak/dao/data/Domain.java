package pro.deta.detatrak.dao.data;

import java.io.Serializable;

import org.apache.log4j.Logger;

public class Domain implements Serializable, Cloneable, Comparable {
    private static final long serialVersionUID = 1579677738143500629L;
    private static final Logger logger = Logger.getLogger(Domain.class);

    protected String code = "";
    protected String deCode = "";

    public Domain () {
    }

    public Domain (String code, String deCode) {
        this.code = code;
        this.deCode = deCode;
    }

    public Domain (String code) {
        this.code = code;
    }

    public String getCode () {
        return code;
    }

    public void setCode (String aCode) {
        code = aCode;
    }

    public String getDeCode () {
        return deCode;
    }

    public void setDeCode (String aDeCode) {
        deCode = aDeCode;
    }

    @Override
    public String toString () {
        //if (deCode != null)
        //  return deCode;
        //else
        //  return "";
        return code + "(" + deCode + ")";
    }

    public String stringValue () {
        return code;
    }

    @Override
    public int hashCode () {
        return code != null ? code.hashCode() : 0;
    }

    public boolean equals (Object other) {
        return other != null
                && other instanceof Domain
                && (code != null ? code.equalsIgnoreCase(((Domain) other).code) : ((Domain) other).code == null);

    }

    public int compareTo (Object other) {
        return code.compareTo(((Domain) other).getCode());
    }

    public Object clone () {
        Domain clonedDomain = null;
        try {
            clonedDomain = (Domain) getClass().newInstance();
            clonedDomain.setCode(code);
            clonedDomain.setDeCode(deCode);
        } catch (Exception ex) {
            logger.debug("Domain.clone() - Illegal access exception - " + ex.getMessage());
        }
        return clonedDomain;
    }

}
