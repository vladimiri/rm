package pro.deta.detatrak.dao.data;

import java.io.Serializable;

public class T2<A1, A2> implements Serializable {

    A1 t1 = null;
    A2 t2 = null;

    public T2 () {
    }

    public T2 (A1 t1, A2 t2) {
        this.t1 = t1;
        this.t2 = t2;
    }

    public A1 getFirst () {
        return t1;
    }

    public A2 getSecond () {
        return t2;
    }

    public void setFirst (A1 t1) {
        this.t1 = t1;
    }

    public void setSecond (A2 t2) {
        this.t2 = t2;
    }

    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((t1 == null) ? 0 : t1.hashCode());
        result = prime * result + ((t2 == null) ? 0 : t2.hashCode());
        return result;
    }

    @Override
    public boolean equals (Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        T2 other = (T2) obj;
        if (t1 == null) {
            if (other.t1 != null) return false;
        } else if (!t1.equals(other.t1)) return false;
        if (t2 == null) {
            if (other.t2 != null) return false;
        } else if (!t2.equals(other.t2)) return false;
        return true;
    }

    public String toString () {
        StringBuffer sb = new StringBuffer();
        sb.append("<T2>");
        if (t1 != null) sb.append("<first>").append(t1.toString()).append("</first>");
        if (t2 != null) sb.append("<second>").append(t2.toString()).append("</second>");
        sb.append("</T2>");
        return sb.toString();
    }
}
