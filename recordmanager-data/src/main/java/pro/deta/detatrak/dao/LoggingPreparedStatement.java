package pro.deta.detatrak.dao;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: VKornyshev Date: 04.12.12 Time: 15:59
 */
public class LoggingPreparedStatement implements PreparedStatement {

	private PreparedStatement stmt;
	private String sql;
	private Map<Integer, String> values;

	public LoggingPreparedStatement(String sql, PreparedStatement stmt) {
		this.sql = sql;
		this.stmt = stmt;
		this.values = new HashMap<Integer, String>();
	}

	public ResultSet executeQuery() throws SQLException {
		return stmt.executeQuery();
	}

	public int executeUpdate() throws SQLException {
		return stmt.executeUpdate();
	}

	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		stmt.setNull(parameterIndex, sqlType);
		values.put(parameterIndex, "null");
	}

	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		stmt.setBoolean(parameterIndex, x);
		values.put(parameterIndex, Boolean.toString(x));
	}

	public void setByte(int parameterIndex, byte x) throws SQLException {
		stmt.setByte(parameterIndex, x);
		values.put(parameterIndex, Byte.toString(x));
	}

	public void setShort(int parameterIndex, short x) throws SQLException {
		stmt.setShort(parameterIndex, x);
		values.put(parameterIndex, Short.toString(x));
	}

	public void setInt(int parameterIndex, int x) throws SQLException {
		stmt.setInt(parameterIndex, x);
		values.put(parameterIndex, Integer.toString(x));
	}

	public void setLong(int parameterIndex, long x) throws SQLException {
		stmt.setLong(parameterIndex, x);
		values.put(parameterIndex, Long.toString(x));
	}

	public void setFloat(int parameterIndex, float x) throws SQLException {
		stmt.setFloat(parameterIndex, x);
		values.put(parameterIndex, Float.toString(x));
	}

	public void setDouble(int parameterIndex, double x) throws SQLException {
		stmt.setDouble(parameterIndex, x);
		values.put(parameterIndex, Double.toString(x));
	}

	public void setBigDecimal(int parameterIndex, BigDecimal x)
			throws SQLException {
		stmt.setBigDecimal(parameterIndex, x);
		values.put(parameterIndex, x.toString());
	}

	public void setString(int parameterIndex, String x) throws SQLException {
		stmt.setString(parameterIndex, x);
		values.put(parameterIndex, x);
	}

	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		stmt.setBytes(parameterIndex, x);
		values.put(parameterIndex, "byte[]");
	}

	public void setDate(int parameterIndex, Date x) throws SQLException {
		stmt.setDate(parameterIndex, x);
		values.put(parameterIndex, x.toString());
	}

	public void setTime(int parameterIndex, Time x) throws SQLException {
		stmt.setTime(parameterIndex, x);
		values.put(parameterIndex, x.toString());
	}

	public void setTimestamp(int parameterIndex, Timestamp x)
			throws SQLException {
		stmt.setTimestamp(parameterIndex, x);
		values.put(parameterIndex, x != null ? x.toString() : null);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		stmt.setAsciiStream(parameterIndex, x, length);
		values.put(parameterIndex, "ascii stream");
	}

	public void setUnicodeStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		stmt.setUnicodeStream(parameterIndex, x, length);
		values.put(parameterIndex, "unicode stream");
	}

	public void setBinaryStream(int parameterIndex, InputStream x, int length)
			throws SQLException {
		stmt.setBinaryStream(parameterIndex, x, length);
		values.put(parameterIndex, "binary stream");
	}

	public void clearParameters() throws SQLException {
		stmt.clearParameters();
		values.clear();
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType)
			throws SQLException {
		stmt.setObject(parameterIndex, x, targetSqlType);
		values.put(parameterIndex, "Object");
	}

	public void setObject(int parameterIndex, Object x) throws SQLException {
		stmt.setObject(parameterIndex, x);
		values.put(parameterIndex, "Object");
	}

	public boolean execute() throws SQLException {
		return stmt.execute();
	}

	public void addBatch() throws SQLException {
		stmt.addBatch();
	}

	public void setCharacterStream(int parameterIndex, Reader reader, int length)
			throws SQLException {
		stmt.setCharacterStream(parameterIndex, reader, length);
		values.put(parameterIndex, "character stream");
	}

	public void setRef(int parameterIndex, Ref x) throws SQLException {
		stmt.setRef(parameterIndex, x);
		values.put(parameterIndex, "Ref");
	}

	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		stmt.setBlob(parameterIndex, x);
		values.put(parameterIndex, "Blob");
	}

	public void setClob(int parameterIndex, Clob x) throws SQLException {
		stmt.setClob(parameterIndex, x);
		values.put(parameterIndex, "Clob");
	}

	public void setArray(int parameterIndex, Array x) throws SQLException {
		stmt.setArray(parameterIndex, x);
		values.put(parameterIndex, "Array");
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		return stmt.getMetaData();
	}

	public void setDate(int parameterIndex, Date x, Calendar cal)
			throws SQLException {
		stmt.setDate(parameterIndex, x, cal);
		values.put(parameterIndex, x.toString());
	}

	public void setTime(int parameterIndex, Time x, Calendar cal)
			throws SQLException {
		stmt.setTime(parameterIndex, x, cal);
		values.put(parameterIndex, x.toString());
	}

	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
			throws SQLException {
		stmt.setTimestamp(parameterIndex, x, cal);
		values.put(parameterIndex, x.toString());
	}

	public void setNull(int parameterIndex, int sqlType, String typeName)
			throws SQLException {
		stmt.setNull(parameterIndex, sqlType, typeName);
		values.put(parameterIndex, "null");
	}

	public void setURL(int parameterIndex, URL x) throws SQLException {
		stmt.setURL(parameterIndex, x);
		values.put(parameterIndex, "URL(" + x + ")");
	}

	public ParameterMetaData getParameterMetaData() throws SQLException {
		return stmt.getParameterMetaData();
	}

	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		stmt.setRowId(parameterIndex, x);
		values.put(parameterIndex, "RowId(" + x + ")");
	}

	public void setNString(int parameterIndex, String value)
			throws SQLException {
		stmt.setNString(parameterIndex, value);
		values.put(parameterIndex, value);
	}

	public void setNCharacterStream(int parameterIndex, Reader value,
			long length) throws SQLException {
		stmt.setNCharacterStream(parameterIndex, value, length);
		values.put(parameterIndex, "NCharacterStream");
	}

	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		stmt.setNClob(parameterIndex, value);
		values.put(parameterIndex, "NClob");
	}

	public void setClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		stmt.setClob(parameterIndex, reader, length);
		values.put(parameterIndex, "CLob");
	}

	public void setBlob(int parameterIndex, InputStream inputStream, long length)
			throws SQLException {
		stmt.setBlob(parameterIndex, inputStream, length);
		values.put(parameterIndex, "Blob");
	}

	public void setNClob(int parameterIndex, Reader reader, long length)
			throws SQLException {
		stmt.setNClob(parameterIndex, reader, length);
		values.put(parameterIndex, "NClob");
	}

	public void setSQLXML(int parameterIndex, SQLXML xmlObject)
			throws SQLException {
		stmt.setSQLXML(parameterIndex, xmlObject);
		values.put(parameterIndex, "SQLXML");
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType,
			int scaleOrLength) throws SQLException {
		stmt.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
		values.put(parameterIndex, "Object");
	}

	public void setAsciiStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		stmt.setAsciiStream(parameterIndex, x, length);
		values.put(parameterIndex, "ascii stream");
	}

	public void setBinaryStream(int parameterIndex, InputStream x, long length)
			throws SQLException {
		stmt.setBinaryStream(parameterIndex, x, length);
		values.put(parameterIndex, "binary stream");
	}

	public void setCharacterStream(int parameterIndex, Reader reader,
			long length) throws SQLException {
		stmt.setCharacterStream(parameterIndex, reader, length);
		values.put(parameterIndex, "character stream");
	}

	public void setAsciiStream(int parameterIndex, InputStream x)
			throws SQLException {
		stmt.setAsciiStream(parameterIndex, x);
		values.put(parameterIndex, "ascii stream");
	}

	public void setBinaryStream(int parameterIndex, InputStream x)
			throws SQLException {
		stmt.setBinaryStream(parameterIndex, x);
		values.put(parameterIndex, "binary stream");
	}

	public void setCharacterStream(int parameterIndex, Reader reader)
			throws SQLException {
		stmt.setCharacterStream(parameterIndex, reader);
		values.put(parameterIndex, "character stream");
	}

	public void setNCharacterStream(int parameterIndex, Reader value)
			throws SQLException {
		stmt.setNCharacterStream(parameterIndex, value);
		values.put(parameterIndex, "NCharacterStream");
	}

	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		stmt.setClob(parameterIndex, reader);
		values.put(parameterIndex, "Clob");
	}

	public void setBlob(int parameterIndex, InputStream inputStream)
			throws SQLException {
		stmt.setBlob(parameterIndex, inputStream);
		values.put(parameterIndex, "Blob");
	}

	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		stmt.setNClob(parameterIndex, reader);
		values.put(parameterIndex, "NClob");
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		return stmt.executeQuery(sql);
	}

	public int executeUpdate(String sql) throws SQLException {
		return stmt.executeUpdate(sql);
	}

	public void close() throws SQLException {
		stmt.close();
	}

	public int getMaxFieldSize() throws SQLException {
		return stmt.getMaxFieldSize();
	}

	public void setMaxFieldSize(int max) throws SQLException {
		stmt.setMaxFieldSize(max);
	}

	public int getMaxRows() throws SQLException {
		return stmt.getMaxRows();
	}

	public void setMaxRows(int max) throws SQLException {
		stmt.setMaxRows(max);
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		stmt.setEscapeProcessing(enable);
	}

	public int getQueryTimeout() throws SQLException {
		return stmt.getQueryTimeout();
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		stmt.setQueryTimeout(seconds);
	}

	public void cancel() throws SQLException {
		stmt.cancel();
	}

	public SQLWarning getWarnings() throws SQLException {
		return stmt.getWarnings();
	}

	public void clearWarnings() throws SQLException {
		stmt.clearWarnings();
	}

	public void setCursorName(String name) throws SQLException {
		stmt.setCursorName(name);
	}

	public boolean execute(String sql) throws SQLException {
		return stmt.execute(sql);
	}

	public ResultSet getResultSet() throws SQLException {
		return stmt.getResultSet();
	}

	public int getUpdateCount() throws SQLException {
		return stmt.getUpdateCount();
	}

	public boolean getMoreResults() throws SQLException {
		return stmt.getMoreResults();
	}

	public void setFetchDirection(int direction) throws SQLException {
		stmt.setFetchDirection(direction);
	}

	public int getFetchDirection() throws SQLException {
		return stmt.getFetchDirection();
	}

	public void setFetchSize(int rows) throws SQLException {
		stmt.setFetchSize(rows);
	}

	public int getFetchSize() throws SQLException {
		return stmt.getFetchSize();
	}

	public int getResultSetConcurrency() throws SQLException {
		return stmt.getResultSetConcurrency();
	}

	public int getResultSetType() throws SQLException {
		return stmt.getResultSetType();
	}

	public void addBatch(String sql) throws SQLException {
		stmt.addBatch(sql);
	}

	public void clearBatch() throws SQLException {
		stmt.clearBatch();
	}

	public int[] executeBatch() throws SQLException {
		return stmt.executeBatch();
	}

	public Connection getConnection() throws SQLException {
		return stmt.getConnection();
	}

	public boolean getMoreResults(int current) throws SQLException {
		return stmt.getMoreResults(current);
	}

	public ResultSet getGeneratedKeys() throws SQLException {
		return stmt.getGeneratedKeys();
	}

	public int executeUpdate(String sql, int autoGeneratedKeys)
			throws SQLException {
		return stmt.executeUpdate(sql, autoGeneratedKeys);
	}

	public int executeUpdate(String sql, int[] columnIndexes)
			throws SQLException {
		return stmt.executeUpdate(sql, columnIndexes);
	}

	public int executeUpdate(String sql, String[] columnNames)
			throws SQLException {
		return stmt.executeUpdate(sql, columnNames);
	}

	public boolean execute(String sql, int autoGeneratedKeys)
			throws SQLException {
		return stmt.execute(sql, autoGeneratedKeys);
	}

	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return stmt.execute(sql, columnIndexes);
	}

	public boolean execute(String sql, String[] columnNames)
			throws SQLException {
		return stmt.execute(sql, columnNames);
	}

	public int getResultSetHoldability() throws SQLException {
		return stmt.getResultSetHoldability();
	}

	public boolean isClosed() throws SQLException {
		return stmt.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		stmt.setPoolable(poolable);
	}

	public boolean isPoolable() throws SQLException {
		return stmt.isPoolable();
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return stmt.unwrap(iface);
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return stmt.isWrapperFor(iface);
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		// s.append("SQL: ").append(sql).append("\n");
		List<Integer> keys = new ArrayList<Integer>(values.keySet());
		Collections.sort(keys);
		s.append("{");
		for (Integer key : keys) {
			if (key != 1) {
				s.append(", ");
			}
			s.append(key).append(": ").append(values.get(key));
		}
		s.append("}");
		return s.toString();
	}

	public void closeOnCompletion() throws SQLException {
		throw new AbstractMethodError("not implement yet");
	}

	public boolean isCloseOnCompletion() throws SQLException {
		throw new AbstractMethodError("not implement yet");
	}

}
