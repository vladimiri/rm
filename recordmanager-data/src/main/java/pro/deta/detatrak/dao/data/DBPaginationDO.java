package pro.deta.detatrak.dao.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author VIlmov Vladimir I.
 *         Jul 30, 2012 7:49:50 PM
 */
public class DBPaginationDO extends DataObject {
    /**
     *
     */
    private static final long serialVersionUID = -1248484697736578092L;
    private int start;
    private int pageSize = 100;
    private boolean needCount = false;
    private List<SortDesc> sort = new ArrayList<SortDesc>();

    public DBPaginationDO () {
    }

    /**
     * @param start2
     * @param cacheSize
     */
    public DBPaginationDO (int start2, int cacheSize) {
        this.start = start2;
        this.pageSize = cacheSize;
    }

    public int getStart () {
        return start;
    }

    public void setStart (int start) {
        this.start = start;
    }

    public int getPageSize () {
        return pageSize;
    }

    public void setPageSize (int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isNeedCount () {
        return needCount;
    }

    public void setNeedCount (boolean needCount) {
        this.needCount = needCount;
    }

    public List<SortDesc> getSort () {
        return sort;
    }

    public void setSort (List<SortDesc> sort) {
        this.sort = sort;
    }

    /**
     * @return
     */
    public int getPageNo () {
        return start / pageSize;
    }

    public DBPaginationDO clone () {
        DBPaginationDO page = new DBPaginationDO();
        page.setNeedCount(this.needCount);
        page.setStart(getStart());
        page.setPageSize(getPageSize());
        page.setSort(new ArrayList<SortDesc>());
        page.getSort().addAll(getSort());
        return page;
    }

    @Override
    public int hashCode () {
        final int prime = 31;
        int result = 1;
        result = prime * result + (needCount ? 1231 : 1237);
        result = prime * result + pageSize;
        result = prime * result + ((sort == null) ? 0 : sort.hashCode());
        result = prime * result + start;
        return result;
    }

    @Override
    public boolean equals (Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DBPaginationDO other = (DBPaginationDO) obj;
        if (needCount != other.needCount) return false;
        if (pageSize != other.pageSize) return false;
        if (sort == null) {
            if (other.sort != null) return false;
        } else if (!sort.equals(other.sort)) return false;
        if (start != other.start) return false;
        return true;
    }
    
    
}
