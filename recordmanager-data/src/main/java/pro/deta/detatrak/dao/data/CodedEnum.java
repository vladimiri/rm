package pro.deta.detatrak.dao.data;

public interface CodedEnum {

    public String code();

}
