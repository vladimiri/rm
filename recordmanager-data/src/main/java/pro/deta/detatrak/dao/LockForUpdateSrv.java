package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * @author VIlmov Vladimir I.
 *         Jul 30, 2012 7:43:37 PM
 */
public class LockForUpdateSrv<O> extends AbstractNewSrv<AbstractNewSrv<? extends Object, O>, O> {
    private Integer limit = 500;
    private String append = "";

    public LockForUpdateSrv (AbstractNewSrv<? extends Object, O> filter, Integer limit) {
        super(filter);
    }

    public LockForUpdateSrv (AbstractNewSrv<? extends Object, O> filter) {
        super(filter);
        limit = 0;
    }
    
    @Override
    protected String buildWhereClause () {
        return filter.buildWhereClause();
    }

    @Override
    protected int fillStatementParameters (PreparedStatement stmt) throws SQLException, AppException {
        currentParameterNum = filter.fillStatementParameters(stmt);
        if(limit > 0)
        	stmt.setInt(nextParam(), limit);
        return currentParameterNum;
    }

    protected String generateRetrieveSqlString () throws AppException {
        String buildWhereClause = filter.buildWhereClause();
        String sql = filter.buildSqlString(filter.buildFields(), filter.buildTableNames(), StringUtils.isBlank(buildWhereClause) ? null :new String[]{buildWhereClause}, null, null);
        String limitStr = "";
        if(limit > 0) 
        	limitStr = " where  rownum <= ? ";
        sql = "select * from ( " + sql + " ) "+limitStr+ " for update skip locked " + append;
        return sql;
    }

    
    
    @Override
    protected String[] buildFields () {
        return filter.buildFields();
    }

    @Override
    protected String[] buildTableNames () {
        return filter.buildTableNames();
    }

    @Override
    public O readOneRow (ResultSet rs) throws SQLException {
        return filter.readOneRow(rs);
    }

    protected String[] buildGroupByClause () {
        return filter.buildGroupByClause();
    }

    protected List<String> buildOrderByClause () {
        return filter.buildOrderByClause();
    }

	public String getAppend() {
		return append;
	}

	public void setAppend(String append) {
		this.append = append;
	}

}
