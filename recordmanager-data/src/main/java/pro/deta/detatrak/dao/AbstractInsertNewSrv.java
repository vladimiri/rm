package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public abstract class AbstractInsertNewSrv<T> implements InsertUpdateSrv<T> {
    private static final Logger logger = Logger.getLogger(AbstractInsertNewSrv.class);
    protected T filter;

    public abstract String getInsertTableName();

    public AbstractInsertNewSrv(T filter) {
        this.filter = filter;
    }

    public abstract int fillStatementParameters(PreparedStatement stmt, T filter)
            throws SQLException, AppException;

    public abstract String[] buildFields();


    protected String buildValues() {
        String[] keyFields = buildFields();
        StringBuilder sql = new StringBuilder();
        for (int i = 0; i < keyFields.length; i++) {
            sql.append(" ? ");
            if (i + 1 < keyFields.length)
                sql.append(" , ");
        }
        return sql.toString();
    }


    public Long executeUnique(DAO dao) throws DAOException {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            if(logger.isDebugEnabled()) logger.debug("SQL: " +sql);
            pstmt = prepareStatement(dao, sql);
            fillStatementParameters(pstmt, filter);
            if(logger.isDebugEnabled()) {
                logger.debug(pstmt);
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                if (!suppressLoggingXmlDetails) {logger.debug("Filling statement with parameters: "+ filter);}
            }
            return executeInsert(pstmt);
        } catch (SQLException e) {
            throw new DAOException(e);
        } catch (Exception e) {
            logger.error("Error while executing service "
                    + this.getClass().getCanonicalName() + " SQL: " + sql
                    + " With parameters: " + filter, e);
            throw new DAOException(e);
        } finally {
            dao.close(pstmt);
        }
    }

    public PreparedStatement prepareStatement(DAO dao, String sql)throws SQLException {
        return dao.prepareStatement(sql);
    }

    public PreparedStatement prepareStatement(DAO dao, String sql, String... keyColumns)throws SQLException {
        return dao.prepareStatement(sql, keyColumns);
    }

    protected Long executeInsert(PreparedStatement pstmt) throws SQLException, AppException {
        return (long) pstmt.executeUpdate();
    }

    public String generateSqlString() throws AppException {
        String tableName = getInsertTableName();
        String[] keyFields = buildFields();
        String keyValues = buildValues();

        return buildInsertSqlString(tableName, keyFields, keyValues);
    }


    public String buildInsertSqlString(String tableName, String[] keyFields, String keyValues) throws AppException {
        try {
            StringBuilder sql = new StringBuilder("insert into ");
            sql.append(tableName).append(" ");

            if (keyFields != null && keyFields.length > 0) {
                sql.append(" ( ");
                for (int i = 0; i < keyFields.length; i++) {
                    if (i > 0) {sql.append(", ");}
                    sql.append(keyFields[i]);
                }
                sql.append(" ) ");
            }

            if (keyValues != null) {
                sql.append(" values ( ");
                sql.append(keyValues);
                sql.append(" ) ");
            }
            return sql.toString();
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }
    }

    public void setFilter(T filter) {
        this.filter = filter;
    }
}
