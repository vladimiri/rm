package pro.deta.detatrak.dao.listener;

import java.io.Serializable;

public class DCNParameters implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5042137202635310844L;
	private String channel;
	private String command;
	private String className;
	private Object id;
	
	public DCNParameters(String command,String className,Object id) {
		this.command = command;
		this.className = className;
		this.id = id;
	}
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}

}