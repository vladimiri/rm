package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pro.deta.detatrak.dao.data.BooleanFieldFilter;
import pro.deta.detatrak.dao.data.CharacterFieldFilter;
import pro.deta.detatrak.dao.data.DateFieldFilter;
import pro.deta.detatrak.dao.data.Domain;
import pro.deta.detatrak.dao.data.NumberFieldFilter;
import pro.deta.detatrak.dao.data.StringFieldFilter;
import pro.deta.detatrak.dao.data.YesNo;

public abstract class AbstractNewSrv<T, O> {
    private static final Logger logger = Logger.getLogger(AbstractNewSrv.class);
    private static int SQL_WARN_TIME = 1000;

    protected T filter = null;
    protected int currentParameterNum = 0;

    protected AbstractNewSrv () {
    }

    public AbstractNewSrv (T filter) {
        this.filter = filter;
    }


    protected abstract String buildWhereClause ();

    protected abstract int fillStatementParameters (PreparedStatement stmt) throws SQLException, AppException;

    protected abstract String[] buildFields ();

    protected abstract String[] buildTableNames ();

    public abstract O readOneRow (ResultSet rs) throws SQLException;

    protected String[] buildGroupByClause () {
        return null;
    }

    protected List<String> buildOrderByClause () {
        return null;
    }

    protected List<O> readAllRows (ResultSet resultSet) throws SQLException {
        List<O> ret = new ArrayList<O>();
        while (resultSet.next()) {
            O t = readOneRow(resultSet);
            if (t != null)
                ret.add(t);
        }
        return ret;
    }

    public List<O> execute (DAO dao) {
        ResultSet rs = null;
        String sql = null;
        PreparedStatement pstmt = null;
        long t1 = 0, t2 = 0, t3 = 0;
        try {
            t1 = System.currentTimeMillis();
            sql = generateRetrieveSqlString();
            if (logger.isDebugEnabled()) {
                logger.debug("SQL: " + sql);
            }
            pstmt = dao.prepareStatement(sql);

            fillStatementParameters(pstmt);
            if (logger.isDebugEnabled()) {
                logger.debug(pstmt);
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                if (!suppressLoggingXmlDetails) {
                    logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                }
            }
            t2 = System.currentTimeMillis();
            rs = pstmt.executeQuery();
            t3 = System.currentTimeMillis();

            return readAllRows(rs);
        } catch (Exception e) {
            logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + filter, e);
            //throw new DAOException("Error while executing service " + this.getClass().getCanonicalName(), e);
        } finally {
            long t4 = System.currentTimeMillis();
            if (t4 - t1 > SQL_WARN_TIME) {
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                logger.error("!!Warning!! service is too slow\n" + this.getClass().getName() + "\nSQL:" + sql
                        + (suppressLoggingXmlDetails ? "" : "\nWith parameters: " + filter));
                logger.error("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            } else {
                logger.debug("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            }
            dao.close(rs);
            dao.close(pstmt);
        }
        return Collections.emptyList();
    }

    public List<O> executeWithException (DAO dao) throws DAOException {
        ResultSet rs = null;
        String sql = null;
        PreparedStatement pstmt = null;
        long t1 = 0, t2 = 0, t3 = 0;
        try {
            t1 = System.currentTimeMillis();
            sql = generateRetrieveSqlString();
            if (logger.isDebugEnabled()) {
                logger.debug("SQL: " + sql);
            }
            pstmt = dao.prepareStatement(sql);

            fillStatementParameters(pstmt);
            if (logger.isDebugEnabled()) {
                logger.debug(pstmt);
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                if (!suppressLoggingXmlDetails) {
                    logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                }
            }
            t2 = System.currentTimeMillis();
            rs = pstmt.executeQuery();
            t3 = System.currentTimeMillis();

            return readAllRows(rs);
        } catch (Exception e) {
            logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + filter, e);
            throw new DAOException("Error while executing service " + this.getClass().getCanonicalName(), e);
        } finally {
            long t4 = System.currentTimeMillis();
            if (t4 - t1 > SQL_WARN_TIME) {
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                logger.error("!!Warning!! service is too slow\n" + this.getClass().getName() + "\nSQL:" + sql
                        + (suppressLoggingXmlDetails ? "" : "\nWith parameters: " + filter));
                logger.error("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            } else {
                logger.debug("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            }
            dao.close(rs);
            dao.close(pstmt);
        }
    }

    public O executeUnique (DAO dao) {
        ResultSet rs = null;
        String sql = null;
        PreparedStatement pstmt = null;
        long t1 = 0, t2 = 0, t3 = 0;
        if (dao.isConnected()) {
            try {
                t1 = System.currentTimeMillis();
                sql = generateRetrieveSqlString();
                if (logger.isDebugEnabled()) {
                    logger.debug("SQL: " + sql);
                }
                pstmt = dao.prepareStatement(sql);

                fillStatementParameters(pstmt);
                if (logger.isDebugEnabled()) {
                    logger.debug(pstmt);
                    boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                    if (!suppressLoggingXmlDetails) {
                        logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                    }
                }

                t2 = System.currentTimeMillis();
                rs = pstmt.executeQuery();
                t3 = System.currentTimeMillis();
                if (rs.next()) {
                    return readOneRow(rs);
                } else {
                    return null;
                }
            } catch (Exception e) {
                logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + filter, e);
                //throw new DAOException("Error while executing service " + this.getClass().getCanonicalName(), e);
            } finally {
                long t4 = System.currentTimeMillis();
                if (t4 - t1 > SQL_WARN_TIME) {
                    boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                    logger.error("!!Warning!! service is too slow\n" + this.getClass().getName() + "\nSQL:" + sql
                            + (suppressLoggingXmlDetails ? "" : "\nWith parameters: " + filter));
                    logger.error("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
                } else {
                    logger.debug("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
                }
                dao.close(rs);
                dao.close(pstmt);
            }
        } else {
            logger.error("DAO " + dao + " is not connected while executing service " + this.getClass() + " With parameters: " + filter);
        }
        return null;
    }

    public O executeUniqueWithException (DAO dao) throws DAOException {
        ResultSet rs = null;
        String sql = null;
        PreparedStatement pstmt = null;
        long t1 = 0, t2 = 0, t3 = 0;
        //if (dao.isConnected()) {
            try {
                t1 = System.currentTimeMillis();
                sql = generateRetrieveSqlString();
                if (logger.isDebugEnabled()) {
                    logger.debug("SQL: " + sql);
                }
                pstmt = dao.prepareStatement(sql);

                fillStatementParameters(pstmt);
                if (logger.isDebugEnabled()) {
                    logger.debug(pstmt);
                    boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                    if (!suppressLoggingXmlDetails) {
                        logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                    }
                }

                t2 = System.currentTimeMillis();
                rs = pstmt.executeQuery();
                t3 = System.currentTimeMillis();
                if (rs.next()) {
                    return readOneRow(rs);
                } else {
                    return null;
                }
            } catch (Exception e) {
                logger.error("Error while executing service " + this.getClass().getCanonicalName() + " SQL: " + sql + " With parameters: " + filter, e);
                throw new DAOException("Error while executing service " + this.getClass().getCanonicalName(), e);
            } finally {
                long t4 = System.currentTimeMillis();
                if (t4 - t1 > SQL_WARN_TIME) {
                    boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                    logger.error("!!Warning!! service is too slow\n" + this.getClass().getName() + "\nSQL:" + sql
                            + (suppressLoggingXmlDetails ? "" : "\nWith parameters: " + filter));
                    logger.error("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
                } else {
                    logger.debug("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
                }
                dao.close(rs);
                dao.close(pstmt);
            }
        //} else {
        //    logger.error("DAO " + dao + " is not connected while executing service " + this.getClass() + " With parameters: " + filter);
        //}
        //return null;
    }
    
    protected String generateRetrieveSqlString () throws AppException {
        String[] selectFields = buildFields();
        String[] tableNames = buildTableNames();
        String coreWhere = buildWhereClause();
        String[] coreGroupBy = buildGroupByClause();
        List<String> coreOrderBy = buildOrderByClause();
        String[] conditions = null;
        if (coreWhere != null && !coreWhere.equals("")) {
            conditions = new String[]{coreWhere};
        }
        String sql = buildSqlString(selectFields, tableNames, conditions, coreGroupBy, coreOrderBy);
        logger.debug(this.getClass().getCanonicalName() + " generateRetrieveSqlString sql is: " + sql);
        return sql;
    }

    protected static void fillStatementParams (PreparedStatement stmt,
                                               List<Object> params) throws AppException {
        int arg = 1;
        try {
            for (Iterator<Object> e = params.iterator(); e.hasNext(); ) {
                Object param = e.next();
                if (param == null)
                    stmt.setString(arg++, null);
                else if (param instanceof String)
                    stmt.setString(arg++, (String) param);
                else if (param instanceof Integer)
                    stmt.setInt(arg++, ((Integer) param).intValue());
                else if (param instanceof Short)
                    stmt.setShort(arg++, ((Short) param).shortValue());
                else if (param instanceof Long)
                    stmt.setLong(arg++, ((Long) param).longValue());
                else if (param instanceof Double)
                    stmt.setDouble(arg++, ((Double) param).doubleValue());
                else if (param instanceof Byte)
                    stmt.setByte(arg++, ((Byte) param).byteValue());
                else if (param instanceof Character)
                    stmt.setString(arg++, ((Character) param).toString());
                else if (param instanceof Boolean)
                    stmt.setBoolean(arg++, ((Boolean) param).booleanValue());
                else if (param instanceof Date)
                    stmt.setTimestamp(arg++, new Timestamp(((Date) param).getTime()));
                else if (param instanceof java.sql.Date)
                    stmt.setTimestamp(arg++, new Timestamp(((java.sql.Date) param).getTime()));
                else if (param instanceof Timestamp)
                    stmt.setTimestamp(arg++, (Timestamp) param);
                else if (param instanceof YesNo)
                    stmt.setString(arg++, yesNoToString((YesNo) param));
                else if (param instanceof Domain)
                    stmt.setString(arg++, domainToString((Domain) param));
                else
                    stmt.setObject(arg++, param);
            }
        } catch (Exception e) {
            logger.debug("DBUtils - fillStatementParams: Failed " + e);
            throw new AppException("DBUtils - fillStatementParams: Failed " + e, e);
        }
    }

    /**
     * Построение списка значений для подстановки в SQL оператор IN
     *
     * @param elements Список значений
     * @return Строка. Например "1,2,3,4,77". В случае если список пустой, то "NULL"
     */
    public static String makeListOfValues (List<?> elements) {
        return (elements == null || elements.isEmpty()) ? "NULL" : StringUtils.join(elements, ",");
    }

    /**
     * Поиск и замена всех вхождений подстроки в строке
     *
     * @param source      Строка в которой ищется подстрока, заданная placeHolder, например "SELECT 1 FROM DUAL WHERE {WHERE_CLAUSE}"
     * @param placeHolder Подстрока для замены, например, "{WHERE_CLAUSE}"
     * @param replacement Замена (на что зменить), например, "1 = 1"
     * @return Строка, например, "SELECT 1 FROM DUAL WHERE 1 = 1"
     */
    public static String replacePlaceholder (String source, String placeHolder, String replacement) {
        return source.replace(placeHolder, replacement);
    }

    public static String domainToString (Domain domain) {
        return domain != null ? domain.getCode() : null;
    }

    public static String yesNoToString (YesNo yesNo) {
        return yesNo != null ? yesNo.stringValue() : null;
    }

    protected static final StringBuffer generateSelectSQLString (
            String[] outputFields) {
        StringBuffer sql = new StringBuffer("SELECT ");
        if (outputFields != null) {
            for (int i = 0; i < outputFields.length; i++) {
                sql.append(outputFields[i]);
                if (i + 1 < outputFields.length)
                    sql.append(", ");
            }

        } else {
            sql.append("*");
        }
        return sql;
    }

    protected String buildSqlString (String[] outputFields, String[] tables,
                                     String[] conditions, String[] groupBy, List<String> orderBy)
            throws AppException {
        try {
            StringBuffer sql = generateSelectSQLString(outputFields);
            sql.append(" FROM ");
            for (int i = 0; i < tables.length; i++) {
                sql.append(tables[i]);
                if (i + 1 < tables.length)
                    sql.append(", ");
            }

            if (conditions != null && conditions.length > 0) {
                sql.append(" ");
                if (!conditions[0].startsWith("START"))
                    sql.append("WHERE ");
                sql.append(StringUtils.join(conditions, " AND "));
            }

            if (groupBy != null && groupBy.length > 0) {
                sql.append(" GROUP BY ");
                sql.append(StringUtils.join(groupBy, ", "));
            }

            if (orderBy != null && orderBy.size() > 0) {
                sql.append(" ");
                if (!orderBy.get(0).startsWith("ORDER"))
                    sql.append(" ORDER BY ");
                sql.append(StringUtils.join(orderBy, ", "));
            }

            return sql.toString();
        } catch (Exception e) {
            logger.error("Error while callong service" + this.getClass() + " with filter: " + filter, e);
            throw new AppException(e.getMessage(), e);
        }
    }

    /**
     * @param strings
     * @return
     */
    final protected List<String> orderBy (String... strings) {
        List<String> list = new ArrayList<String>();
        for (String string2 : strings) {
            list.add(string2);
        }
        return list;
    }

    @Override
    public String toString () {
        return "Executing service " + this.getClass().getName() + " with parameters: " + filter;
    }

    public int nextParam () {
        return ++currentParameterNum;
    }

    public String TO_DATE (String dateYYYYMMDD) {
        return "TO_DATE('" + dateYYYYMMDD + "01','YYYYMMDDHH')";
    }

    public String NVL (String field) {
        return "NVL(" + field + ", TO_DATE('47001231','YYYYMMDD'))";
    }

    public void andFilter (StringBuilder where, String field, BooleanFieldFilter filter) {
        if (filter.isValue() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append(field).append(" = ?");
        }
    }

    public void fillParams (PreparedStatement stmt, BooleanFieldFilter filter) throws SQLException {
        if (filter.isValue() != null) {
            stmt.setBoolean(nextParam(), filter.isValue());
        }
    }

    public void andFilter (StringBuilder where, String field, NumberFieldFilter<?> filter) {
        if (filter.getValue() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append(field).append(" = ?");
        } else {
            if (filter.getFrom() != null) {
                if (where.length() > 0) {
                    where.append(" and ");
                }
                where.append("? ").append(filter.isIncludeFrom() ? "<=" : "<").append(" ").append(field);
            }
            if (filter.getTo() != null) {
                if (where.length() > 0) {
                    where.append(" and ");
                }
                where.append(field).append(" ").append(filter.isIncludeTo() ? "<=" : "<").append(" ?");
            }
        }
    }

    public void fillParams (PreparedStatement stmt, NumberFieldFilter<?> filter) throws SQLException {
        if (filter.getValue() != null) {
            setValue(stmt, filter.getValue());
        } else {
            if (filter.getFrom() != null) {
                setValue(stmt, filter.getFrom());
            }
            if (filter.getTo() != null) {
                setValue(stmt, filter.getTo());
            }
        }
    }

    public void setValue (PreparedStatement stmt, Number value) throws SQLException {
        if (value instanceof Byte) {
            stmt.setByte(nextParam(), value.byteValue());
        } else if (value instanceof Short) {
            stmt.setShort(nextParam(), value.shortValue());
        } else if (value instanceof Integer) {
            stmt.setInt(nextParam(), value.intValue());
        } else if (value instanceof Long) {
            stmt.setLong(nextParam(), value.longValue());
        } else if (value instanceof Float) {
            stmt.setFloat(nextParam(), value.floatValue());
        } else if (value instanceof Double) {
            stmt.setDouble(nextParam(), value.doubleValue());
        }
    }

    public void andFilter (StringBuilder where, String field, CharacterFieldFilter filter) {
        if (filter.getValue() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append(field).append(" = ?");
        } else if (filter.getValues() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append("(");
            for (int i = 0; i < filter.getValues().size(); i++) {
                if (i > 0) {
                    where.append(", ");
                }
                where.append("?");
            }
            where.append(")");
        }
    }

    public void fillParams (PreparedStatement stmt, CharacterFieldFilter filter) throws SQLException {
        if (filter.getValue() != null) {
            stmt.setString(nextParam(), Character.toString(filter.getValue()));
        } else if (filter.getValues() != null) {
            for (Character c : filter.getValues()) {
                stmt.setString(nextParam(), Character.toString(c));
            }
        }
    }

    public void andFilter (StringBuilder where, String field, StringFieldFilter filter) {
        if (filter.getValue() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append(field).append(" = ?");
        } else if (filter.getValues() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append("(");
            for (int i = 0; i < filter.getValues().size(); i++) {
                if (i > 0) {
                    where.append(", ");
                }
                where.append("?");
            }
            where.append(")");
        }
    }

    public void fillParams (PreparedStatement stmt, StringFieldFilter filter) throws SQLException {
        if (filter.getValue() != null) {
            stmt.setString(nextParam(), filter.getValue());
        } else if (filter.getValues() != null) {
            for (String s : filter.getValues()) {
                stmt.setString(nextParam(), s);
            }
        }
    }

    public void andFilter (StringBuilder where, String field, DateFieldFilter filter) {
        if (filter.getValue() != null) {
            if (where.length() > 0) {
                where.append(" and ");
            }
            where.append(field).append(" = ?");
        } else {
            if (filter.getFrom() != null) {
                if (where.length() > 0) {
                    where.append(" and ");
                }
                where.append("? ").append(filter.isIncludeFrom() ? "<=" : "<").append(" ").append(field);
            }
            if (filter.getTo() != null) {
                if (where.length() > 0) {
                    where.append(" and ");
                }
                where.append(field).append(" ").append(filter.isIncludeTo() ? "<=" : "<").append(" ?");
            }
        }
    }

    public void fillParams (PreparedStatement stmt, DateFieldFilter filter) throws SQLException {
        if (filter.getValue() != null) {
            stmt.setTimestamp(nextParam(), new Timestamp(filter.getValue().getTime()));
        } else {
            if (filter.getFrom() != null) {
                stmt.setTimestamp(nextParam(), new Timestamp(filter.getFrom().getTime()));
            }
            if (filter.getTo() != null) {
                stmt.setTimestamp(nextParam(), new Timestamp(filter.getTo().getTime()));
            }
        }
    }

}
