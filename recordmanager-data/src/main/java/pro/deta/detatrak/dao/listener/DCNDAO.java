package pro.deta.detatrak.dao.listener;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.slf4j.LoggerFactory;

import pro.deta.detatrak.dao.EMDAO;


public class DCNDAO {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DCNDAO.class);
	private static DCNDAO instance = null;
	
	private DCNUpdateListener listener = null;
	private EMDAO emd;
	private EntityManager em;
	private Session con;

	private DCNDAO(DCNCallback callback,EMDAO emd) {
		listener = new DCNUpdateListener(DCNUpdateNotifier.DEFAULT_CHANNEL,callback);
		this.emd = emd;
		try {
			em = emd.createEntityManager();
			con = em.unwrap(Session.class);
			con.doWork(listener);
			listener.start();
		} catch (Exception e) {
			logger.error("DataChangeNotification not started.",e);
		} finally {
			
		}
	}
	
	public static DCNDAO getInstance(DCNCallback callback,EMDAO emd) {
		if(instance == null)
			synchronized (DCNDAO.class) {
				if(instance == null)
					instance = new DCNDAO(callback, emd);
			}
		return instance;
	}
	
	public void close() {
		try {
			if(listener != null) {
				listener.interrupt();
				listener.join();
			}
		} catch (Throwable e) {
			logger.error("Error while interrupting listener",e);
		}
		listener = null;
		
		con = null;
		
		try {
			if(em != null)
				em.close();
		} catch (Exception e) {
			logger.error("Error while closing emd",e);
		}
		em = null;
		try {
			if(emd != null)
				emd.close();
		} catch (Exception e) {
			logger.error("Error while closing emd",e);
		}
		emd = null;
		instance = null;
	}
}
