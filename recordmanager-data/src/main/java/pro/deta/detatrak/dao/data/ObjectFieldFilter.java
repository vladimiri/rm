package pro.deta.detatrak.dao.data;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ObjectFieldFilter<T extends Serializable> implements Serializable {

    private T value;
    private List<T> values;

    public ObjectFieldFilter() {
    }

    public ObjectFieldFilter(T value) {
        this.value = value;
    }

    public ObjectFieldFilter(List<T> values) {
        this.values = values;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public List<T> getValues() {
        return values;
    }

    public void setValues(List<T> values) {
        this.values = values;
    }

    public String generateSql(String fieldName) {
        StringBuilder where = new StringBuilder();
        generateSql(where, fieldName);
        return where.toString();
    }

    public void generateSql(StringBuilder where, String fieldName) {
        if (value != null) {
            where.append(fieldName).append(" = ?");
        } else if (values != null && values.size() > 0) {
            where.append(fieldName).append(" in (");
            int i = 0;
            for (T t: values) {
                if (i++ > 0) {where.append(", ");}
                where.append("?");
            }
            where.append(")");
        }
    }

    public int fillStatementParameters(PreparedStatement st, int index, Mapper<T> mapper) throws SQLException {
        if (value != null) {
            mapper.fillStatementParameter(st, ++index, value);
        } else if (values != null && values.size() > 0) {
            for (T t: values) {
                mapper.fillStatementParameter(st, ++index, t);
            }
        }
        return index;
    }

    public abstract static class Mapper<T extends Serializable> {
        public abstract void fillStatementParameter(PreparedStatement st, int index, T value) throws SQLException;
    }

}
