package pro.deta.detatrak.dao.data;

import java.util.Date;

import ru.yar.vi.rm.data.BaseDO;

public class CFMReportDO extends BaseDO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7584330856696864515L;
	private Date date;
	private String user;
	private int actionId;
	
	private Date startTime;
	private Date endTime;
	private int count;
	private String avgWaitingTime;
	private String avgProcessingTime;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public String getAvgWaitingTime() {
		return avgWaitingTime;
	}
	public void setAvgWaitingTime(String avgWaitingTime) {
		this.avgWaitingTime = avgWaitingTime;
	}
	public String getAvgProcessingTime() {
		return avgProcessingTime;
	}
	public void setAvgProcessingTime(String avgProcessingTime) {
		this.avgProcessingTime = avgProcessingTime;
	}
	
}
