package pro.deta.detatrak.dao;

public class AppException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3581012584336865411L;

	public AppException(String message, Exception e) {
		super(message,e);
	}

	public AppException(String message) {
		super(message);
	}

}
