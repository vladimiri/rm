package pro.deta.detatrak.dao;

public class DAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7052145275606324787L;

	public DAOException(Exception e) {
		super(e);
	}

	public DAOException(String string, Exception e) {
		super(string,e);
	}

	public DAOException(String string) {
		super(string);
	}

}
