package pro.deta.detatrak.dao;

import java.sql.CallableStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public abstract class AbstractSQLScriptSrv<T, O> {
    protected T filter;
    private static final Logger logger = Logger.getLogger(AbstractSQLScriptSrv.class);
    private static int SQL_WARN_TIME = 1000;

    protected AbstractSQLScriptSrv () {
    }

    public AbstractSQLScriptSrv (T filter) {
        this.filter = filter;
    }

    public O executeUnique (DAO dao) throws DAOException {
        O data;
        CallableStatement cs = null;
        long t1 = 0, t2 = 0, t3 = 0;
        String sql = "";
        try {
            sql = generateSQLCode();
            t1 = System.currentTimeMillis();
            if (logger.isDebugEnabled()) {
                logger.debug("SQL: " + sql);
            }

            cs = dao.prepareCall(sql);
            fillStatementParameters(cs);
            if (logger.isDebugEnabled()) {
                logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
            }
            t2 = System.currentTimeMillis();

            logger.debug("Filling statement with parameters: " + filter);
            cs.execute();
            t3 = System.currentTimeMillis();
            data = readOneRow(cs);
        } catch (Exception ex) {
            /*logging*/
            logger.error(new StringBuilder("Error while executing service\n")
                    .append(this.getClass().getCanonicalName()).append("\nSQL:").append(sql)
                    .append("\nWith parameters: ").append(filter).append(ex.getMessage()), ex);
            throw new DAOException(ex);
        } finally {
            long t4 = System.currentTimeMillis();
            if (t4 - t1 > SQL_WARN_TIME) {
                logger.error("!!Warning!! procedure is too slow\n" + this.getClass().getName() + "\nSQL:" + sql
                        + "\nWith parameters: " + filter);
                logger.error("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            } else {
                logger.debug("Timings for " + this.getClass().getName() + "\ninit/exec/read:" + (t2 - t1) + "/" + (t3 - t2) + "/" + (t4 - t3) + " Total: " + (t4 - t1));
            }
            dao.close(cs);
        }
        return data;
    }

    abstract protected String generateSQLCode () throws AppException;

    abstract protected int fillStatementParameters (CallableStatement cs) throws SQLException, AppException;

    abstract protected O readOneRow (CallableStatement cs) throws SQLException;
}
