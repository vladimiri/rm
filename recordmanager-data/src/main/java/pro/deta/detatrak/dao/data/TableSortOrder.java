package pro.deta.detatrak.dao.data;

public enum TableSortOrder {

    ASCENDING("ASC", 'A'), DESCENDING("DESC", 'D'), NONE("NONE", ' ');

    private String label;
    private char charLabel;

    private TableSortOrder (String label, char charLabel) {
        this.label = label;
        this.charLabel = charLabel;
    }

    public String getLabel () {
        return label;
    }

    public char getCharLabel () {
        return charLabel;
    }

}
