package pro.deta.detatrak.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public abstract class AbstractUpdateNewSrv<T> implements InsertUpdateSrv<T> {
    public static final Logger logger = Logger.getLogger(AbstractUpdateNewSrv.class);
    protected T filter;

    public AbstractUpdateNewSrv (T filter) {
        this.filter = filter;
    }

    public abstract String[] buildUpdateFields ();

    protected String buildUpdateSqlString (String tableName, String[] updateFields, String[] updateValues,
                                           String whereString) throws AppException {
        try {
            StringBuilder sql = new StringBuilder("update " + tableName + " SET ");
            String[] updates = new String[updateFields.length];
            for (int i = 0; i < updateFields.length; i++) {
                String value = (updateValues[i] == null || updateValues[i].isEmpty()) ? "?" : updateValues[i];
                updates[i] = updateFields[i] + "=" + value;
            }
            sql.append(StringUtils.join(updates, ", "));
            if (!StringUtils.isEmpty(whereString)) {
                sql.append(" WHERE ");
                sql.append(whereString);
            }
            return sql.toString();
        } catch (Exception e) {
            throw new AppException(e.getMessage(), e);
        }
    }

    protected String buildWhereClause () {
        return null;
    }

    protected String[] buildUpdateValues () {
        String[] updateFields = buildUpdateFields();
        String[] updateValues = new String[updateFields.length];
        Arrays.fill(updateValues, "?");
        return updateValues;
    }


    protected int executeBatch (PreparedStatement pstmt) throws SQLException, AppException {
        int[] res = pstmt.executeBatch();
        return res.length;
    }

    public int executeBatch (T filter, DAO dao) {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            pstmt = dao.prepareStatement(sql);
            fillStatementParameters(pstmt, filter);
            return executeBatch(pstmt);
        } catch (Exception e) {
            getLogger().error("SQL: " + sql + " with parameters: " + filter, e);
        } finally {
            dao.close(pstmt);
        }
        return 0;
    }

    public Long executeUnique (DAO dao) {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            if (logger.isDebugEnabled()) {
                logger.debug("SQL: " + sql);
            }            
            pstmt = dao.prepareStatement(sql);
            fillStatementParameters(pstmt, filter);
            if (logger.isDebugEnabled()) {
                logger.debug(pstmt);
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                if (!suppressLoggingXmlDetails) {
                    logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                }
            }            
            return executeUpdate(pstmt);
        } catch (Exception e) {
            getLogger().error("SQL: " + sql + " with parameters: " + filter, e);
        } finally {
            dao.close(pstmt);
        }
        return null;
    }

    public Long executeUniqueWithException (DAO dao) throws DAOException {
        String sql = null;
        PreparedStatement pstmt = null;
        try {
            sql = generateSqlString();
            if (logger.isDebugEnabled()) {
                logger.debug("SQL: " + sql);
            }            
            pstmt = dao.prepareStatement(sql);
            fillStatementParameters(pstmt, filter);
            if (logger.isDebugEnabled()) {
                logger.debug(pstmt);
                boolean suppressLoggingXmlDetails = Boolean.parseBoolean(System.getProperty("suppress.logging.xml.details", "false"));
                if (!suppressLoggingXmlDetails) {
                    logger.debug(this.getClass().getCanonicalName() + " Filling statement with parameters: " + filter);
                }
            }            
            return executeUpdate(pstmt);
        } catch (Exception e) {
            getLogger().error("SQL: " + sql + " with parameters: " + filter, e);
            throw new DAOException(e);
        } finally {
            dao.close(pstmt);
        }
    }

    protected Long executeUpdate (PreparedStatement pstmt) throws SQLException, AppException {
    	long result = (long) pstmt.executeUpdate();
        return result;
    }

    public abstract int fillStatementParameters (PreparedStatement stmt, T filter) throws SQLException, AppException;

    public String generateSqlString () throws AppException {
        String tableName = getUpdateTableName();
        String[] updateFields = buildUpdateFields();
        String[] updateValues = buildUpdateValues();
        String whereString = buildWhereString();
        String sql = buildUpdateSqlString(tableName, updateFields, updateValues, whereString);
        return sql;
    }

    abstract protected String buildWhereString ();

    public Logger getLogger () {
        return Logger.getLogger(getClass());
    }

    public abstract String getUpdateTableName ();

    public void setFilter (T filter) {
        this.filter = filter;
    }

}
