package pro.deta.detatrak;

import java.net.URL;

import javax.xml.parsers.FactoryConfigurationError;

import org.apache.log4j.xml.DOMConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogInitializer {
	
	public static void initialize(URL file) {
		Logger logger = null;
		try {
			DOMConfigurator.configure(file);
		} catch (FactoryConfigurationError e1) {
			e1.printStackTrace();
		}
		logger = LoggerFactory.getLogger(LogInitializer.class);
		logger.error("Loaded: " + file);
	}
}
