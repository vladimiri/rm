package pro.deta.detatrak.serviceapp;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
public @Data class Company implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4604922461824765721L;
	private String name;
	private Address address = new Address();
	private String ogrn;
	private String inn;
	
	
}
