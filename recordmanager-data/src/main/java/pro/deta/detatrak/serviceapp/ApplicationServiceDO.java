package pro.deta.detatrak.serviceapp;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="APPLICATION_SERVICE")
public @Data class ApplicationServiceDO implements Serializable {
	/**
	 * 
	 */
	@Id
	@GeneratedValue(generator="seq-gen")
	@SequenceGenerator(name="seq-gen", sequenceName="record_seq",allocationSize=1)
	private long id;
	 
	private static final long serialVersionUID = -3871942898961267306L;


	private String status;
	private String externalId;
	private String applicationAuthority;
	private int action;
	private int subAction;
	private int ownership;
	private String docs;
	private String docsOther;
	private String customerType;
	
	@Embedded
	private Company company = new Company();
	@Embedded
	private Person owner = new Person();
	@Embedded
	private Person personal = new Person();
	
	// vehicle data
	@Embedded
	private Vehicle vehicle = new Vehicle();
	
	
	// personal data
	private String enableAgreement;
	private String authAgreementDate;
	private String authAgreementIssuedBy;
	private String authAgreementNumber;
	private Date sysCreationDate;
	
	
	
	public Map<String, String> getMap() {
		Map<String, String> ret = new HashMap<String, String>();
		ret.put("WHERE", getApplicationAuthority());
		ret.put("ACTION", ""+getAction());
		ret.put("DOCS", getDocs());
		ret.put("AUTH_AGREEMENT", getAuthAgreementDate() +" "+ getAuthAgreementIssuedBy()+" "+getAuthAgreementNumber());
		return ret ;
	}

	

	
	public String[] getDocsArr() {
		if(docs != null)
			return docs.split(",");
		else 
			return new String[0];
	}
	public void setDocsArr(String[] docs) {
		if(docs != null)
			this.docs = StringUtils.join(docs,",");
		else
			this.docs = "";
	}
	
}
