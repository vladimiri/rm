package pro.deta.detatrak.serviceapp;

import java.io.Serializable;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
public @Data class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7852492451441292535L;
	private String apartment;
	private String street;
	private String phone;
	private String house;
	private String town;
	private String building;
	private String district;
	private String post;
	private String region;
	private String regionName;
	private String state;
}
