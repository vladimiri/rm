package pro.deta.detatrak.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.template.MiniTemplator;
import ru.yar.vi.template.MiniTemplator.TemplateSpecification;
import ru.yar.vi.template.MiniTemplator.TemplateSyntaxException;
import ru.yar.vi.template.MiniTemplatorCache;

public class DETAHelper {
	private static final Logger logger = Logger.getLogger(DETAHelper.class);
	private static MiniTemplatorCache cache = new MiniTemplatorCache();
	
	public static String getTemplate(StoredRecordDO rec, TemplateSpecification spec)  {
		MiniTemplator t;
		try {
			t = cache.get(spec);
		} catch (Exception e) {
			logger.error("Exception: ",e);
			return "";
		}
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("name", rec.getName());
		hm.put("email", rec.getEmail());
		hm.put("phone", rec.getPhone());
		Map<String, String> map = rec.getInfo();
		if(map != null)
			hm.putAll(map);
		
		for (String key : hm.keySet()) {
			Object val = hm.get(key);
			if(val != null) {
				t.setVariable(key,val.toString(),true);
			}
		}
		return t.generateOutput();
	}

}
