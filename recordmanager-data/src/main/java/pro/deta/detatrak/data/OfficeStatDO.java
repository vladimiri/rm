package pro.deta.detatrak.data;

import java.io.Serializable;
import java.util.List;

import ru.yar.vi.rm.data.OfficeDO;

public class OfficeStatDO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7564921945549978661L;
	private List<ActionQueueDO> actionQueue;
	private OfficeDO office;
	
	public OfficeDO getOffice() {
		return office;
	}

	public void setOffice(OfficeDO office) {
		this.office = office;
	}

	public List<ActionQueueDO> getActionQueue() {
		return actionQueue;
	}

	public void setActionQueue(List<ActionQueueDO> actionQueue) {
		this.actionQueue = actionQueue;
	}

	
}
