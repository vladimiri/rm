package pro.deta.detatrak.data;

import java.util.List;

import lombok.Data;
import ru.yar.vi.rm.data.ActionDO;

public @Data class ActionQueueDO {
	private ActionDO action;
	/**
	 * количество посетителей в ожидании электронной очереди
	 */
	private int cfmQueueSize;
	private int pluralQueueSize;
	/**
	 * ближайшее доступное время для записи по предварительной записи
	 */
	private List<ObjectTypeDateDO> typeDateList;
	private boolean hasTime;
}
