package pro.deta.detatrak.data;

import lombok.Data;
import ru.yar.vi.rm.data.StoredRecordDO;

public @Data class RankInfo{
	private Integer id;
	private Float rank;
	private StoredRecordDO record;
	
}