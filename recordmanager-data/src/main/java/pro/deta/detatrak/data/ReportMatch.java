package pro.deta.detatrak.data;

import java.util.List;

import lombok.Data;

public @Data class ReportMatch {
	private FileRecord fileRecord;
	private List<RankInfo> rank;
	
	public ReportMatch(FileRecord fileRecord2, List<RankInfo> found) {
		this.fileRecord = fileRecord2;
		this.rank = found;
	}
}
