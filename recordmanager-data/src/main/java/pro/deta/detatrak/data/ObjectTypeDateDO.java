package pro.deta.detatrak.data;

import java.util.Date;

import ru.yar.vi.rm.data.ObjectTypeDO;

public class ObjectTypeDateDO {
	ObjectTypeDO type;
	Date available;
	
	public ObjectTypeDO getType() {
		return type;
	}
	public void setType(ObjectTypeDO type) {
		this.type = type;
	}
	public Date getAvailable() {
		return available;
	}
	public void setAvailable(Date available) {
		this.available = available;
	}
}
