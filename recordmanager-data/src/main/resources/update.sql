create table action_object_type_item (
	action_id integer,
	object_type_item_id integer,
	sorter integer
);

create table object_type_item (
	id integer,
	type_id integer,
	required character varying(10)
);


alter table office add column default_office_id integer;
update office set default_office_id = 0;

alter table object add column priority integer;


create table weekend_object (
	weekend_id integer,
	object_id integer,
	sorter integer
);

create table schedule_criteria (
	schedule_id integer,
	criteria_id integer,
	sorter integer
);

create table schedule_action (
	schedule_id integer,
	action_id integer,
	sorter integer
);


alter table duration add column type_id integer;
alter table criteria add column sorter integer;
insert into criteria (id,name,security,sorter) values(0,'���',0,0);
ALTER TABLE duration
   ALTER COLUMN action_id TYPE numeric;
update duration set action_id = null where action_id = 0
insert into customers (id,name) values(0,'���');

create table record_map_info (
	id integer,
	name varchar(100),
	value varchar(200),
	tsv tsvector
);

CREATE INDEX record_map_info_ix
   ON record_map_info (id ASC NULLS LAST);


CREATE OR REPLACE FUNCTION record_map_info_trigger()
  RETURNS trigger AS
$BODY$
begin
	IF new.name = 'pts' THEN
  		new.tsv := 	
  					setweight(to_tsvector('pg_catalog.russian', coalesce(new.value,'')),'A' ) ||
  					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.value,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.value,E'\\s*'),' '),'')),'D' );
	ELSIF  new.name = 'vehicle_no' THEN
		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'C' );
	ELSIF  new.name = 'vehicle_name' THEN
		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'B' );
	ELSIF  new.name = 'doc_no' THEN
  		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'A' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(regexp_replace(new.value,E'(\\S{2})\\s*',E'\\1 ','g'),'')),'C' ) ||
					setweight(to_tsvector('pg_catalog.russian', coalesce(array_to_string(regexp_split_to_array(new.value,E'\\s*'),' '),'')),'D' );
	ELSE
  		new.tsv :=
					setweight(to_tsvector('pg_catalog.russian', coalesce(split_border(new.value),'')),'B' );
	
  	END IF;
	return new;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
CREATE TRIGGER tsvectorupdate_record_map_info
  BEFORE INSERT OR UPDATE
  ON record_map_info
  FOR EACH ROW
  EXECUTE PROCEDURE record_map_info_trigger();

CREATE FUNCTION tsextcat(acc tsvector, instr tsvector) RETURNS tsvector AS $$
  BEGIN
      RETURN acc || instr;
  END;
$$ LANGUAGE plpgsql;

CREATE AGGREGATE textcat_all(
  basetype    = tsvector,
  sfunc       = tsextcat,
  stype       = tsvector,
  initcond    = to_tsvector('russian','')
);

create table role_security (
	name varchar(100),
	code varchar(100)
);

alter table role drop column id;


update validator set parameter = 
'{"query":"SIMPLE_UNIQUE","maxOccurences":2,"parameterName":"pts"}'
where clazz = 'pro.deta.detatrak.validator.MaxNumberRecordsValidator' and parameter like '%pts%';

update validator set parameter = 
'{"query":"SIMPLE_UNIQUE","maxOccurences":3,"parameterName":"pts"}'
where clazz = 'pro.deta.detatrak.validator.MaxNumberRecordsValidator' and parameter like '%pts%' and parameter like '%maxoccurences":3%';

update validator set parameter = 
'{"query":"SIMPLE_UNIQUE","maxOccurences":1,"parameterName":"documentNo"}'
where clazz = 'pro.deta.detatrak.validator.MaxNumberRecordsValidator' and parameter like '%documentNo%';


alter table object add column type_id integer;

update object o set type_id = (select id from object_type t where t.type = o.type);

update duration set object_id = null where object_id = 0;
update duration set action_id = null where action_id = 0;
update duration set criteria_id = null where criteria_id = 0;
update duration set customer_id = null where customer_id = 0;

delete from duration where action_id not in (select id from actions);
ALTER TABLE duration
   ALTER COLUMN action_id TYPE integer;
ALTER TABLE duration
   ALTER COLUMN action_id SET DEFAULT null;
   
ALTER TABLE duration
  ADD CONSTRAINT duration_action_fk FOREIGN KEY (action_id) REFERENCES actions (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE role
  ADD COLUMN id integer;

update custom_field set field = 'criteria' where field = 'criteriaId';

ALTER TABLE role
  ADD CONSTRAINT role_pk PRIMARY KEY (name);
  
CREATE TABLE user_office
(
  login character varying(255) NOT NULL,
  office_id integer NOT NULL,
  sorter integer NOT NULL,
  CONSTRAINT user_office_pkey PRIMARY KEY (login, sorter),
  CONSTRAINT user_office_fk1 FOREIGN KEY (office_id)
      REFERENCES office (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT user_office_fk2 FOREIGN KEY (login)
      REFERENCES users (login) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

insert into role_security (name,code) values('rm-admin','ADMIN_ACCESS');
insert into role_security (name,code) values('rm-admin','ADMINISTRATION_TAB');
insert into role_security (name,code) values('rm-oper','OPER_ACCESS');
insert into role_security (name,code) values('full','ALL');
update object set priority = 0 where priority is null;

update duration d set type_id = (select id from object_type where type = d.object_type)

-- alter table weekend drop column object_list - ������ ����� ������