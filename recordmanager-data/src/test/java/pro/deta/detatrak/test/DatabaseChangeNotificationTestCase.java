package pro.deta.detatrak.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.persistence.EntityManager;
import javax.sql.PooledConnection;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Test;
import org.postgresql.PGConnection;

public class DatabaseChangeNotificationTestCase extends BaseTestAssert {

	public void listen() throws InterruptedException {
		EntityManager ownEM = emf.createEntityManager();
		Session con = ownEM.unwrap(Session.class);
		DCNWork work = new DCNWork();
		con.doWork(work);
		assertTrue(true);
		work.start();
		Thread.sleep(10000);
		work.interrupt();
	}
}


class DCNWork extends Thread implements Work {
	PGConnection pgconn;
	Connection conn;
	@Override
	public void execute(Connection paramConnection) throws SQLException {
		pgconn = (org.postgresql.PGConnection)((PooledConnection) paramConnection).getConnection();
		conn = paramConnection;
		Statement stmt = paramConnection.createStatement();
		stmt.execute("LISTEN mymessage");
		stmt.close();
	}
	
	public void run() {
		while (!this.isInterrupted()) {
			try {
				// issue a dummy query to contact the backend
				// and receive any pending notifications.
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT 1");
				rs.close();
				stmt.close();

				org.postgresql.PGNotification notifications[] = pgconn.getNotifications();
				if (notifications != null) {
					for (int i=0; i<notifications.length; i++) {
						System.out.println("Got notification: " + notifications[i].getName() +" "+ notifications[i].getParameter());
					}
				}

				// wait a while before checking again for new
				// notifications
				Thread.sleep(500);
			} catch (SQLException sqle) {
				sqle.printStackTrace();
			} catch (InterruptedException ie) {
				return;
			}
		}
	}
	
}