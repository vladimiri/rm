package pro.deta.detatrak.test;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ru.yar.vi.rm.data.TerminalLinkDO;

public class TerminalLinkDOTestCase extends Assert {
	private EntityManager em;

    public void beforeEach(){
        // Exception occurs here
        em = getEntityManagerFactory().createEntityManager();
    }
    
    public void testClass() {
    	TerminalLinkDO root =  em.find(TerminalLinkDO.class, 1);
    	if(root != null)
    	for (TerminalLinkDO terminalLinkDO : root.getChilds()) {
    		if(terminalLinkDO != null)
			System.out.println("Terminal link:" + terminalLinkDO.getId());
		}
    }
    public static EntityManagerFactory getEntityManagerFactory(){
        return Persistence.createEntityManagerFactory("rm");
    }
    
    public<T> List<T> getAll(Class<T> class1) {
    	CriteriaQuery<T> criteria = em.getCriteriaBuilder().createQuery(class1);
        criteria.select(criteria.from(class1));
        List<T> ListOfEmailDomains = em.createQuery(criteria).getResultList();
        return ListOfEmailDomains;
    }
}
