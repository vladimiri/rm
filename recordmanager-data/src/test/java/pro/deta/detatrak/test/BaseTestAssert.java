package pro.deta.detatrak.test;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSourceFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class BaseTestAssert extends Assert {
	private static final String ENTITY_MANAGER_INSTANCE = "rm";
	protected EntityManagerFactory emf;
	protected EntityManager em;
	
//	@Before
    public void beforeEach(){
    	// rcarver - setup the jndi context and the datasource
        try {
            // Create initial context
        	System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
                    "org.apache.naming.java.javaURLContextFactory");
            System.setProperty(Context.URL_PKG_PREFIXES,"org.apache.naming");
            InitialContext ic = new InitialContext();

            ic.createSubcontext("java:");
            ic.createSubcontext("java:comp");
            ic.createSubcontext("java:comp/env");
            ic.createSubcontext("java:comp/env/jdbc");
           
            Properties properties = new Properties();
            properties.put("username", "gbt");
            properties.put("password", "gbt");
            properties.put("driverClassName", "org.postgresql.Driver");
            properties.put("url", "jdbc:postgresql://127.0.0.1:15432/gbt?autoReconnect=true");
            properties.put("logAbandoned", "true");
			// Construct DataSource
            DataSource ds = new DataSourceFactory().createDataSource(properties );
            ic.bind("java:comp/env/jdbc/RecordManagerDS", ds);
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).error(null, ex);
        }
        
      //Get the System Classloader
        ClassLoader sysClassLoader = ClassLoader.getSystemClassLoader();

        //Get the URLs
        URL[] urls = ((URLClassLoader)sysClassLoader).getURLs();

        for(int i=0; i< urls.length; i++)
        {
            System.out.println(urls[i].getFile());
        }       
        Map<String, String> map = new HashMap<>();
        map.put("hibernate.show_sql", "true");
        map.put("hibernate.format_sql", "true");
        map.put("hibernate.use_sql_comments", "true");
        
		emf = Persistence.createEntityManagerFactory(ENTITY_MANAGER_INSTANCE, map );
        em = emf.createEntityManager();
    }

//	@After
    public void afterEach(){
    	em.close();
    	emf.close();
    }
        
}
