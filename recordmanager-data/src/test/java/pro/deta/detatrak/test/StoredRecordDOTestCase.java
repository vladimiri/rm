package pro.deta.detatrak.test;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;

import org.junit.Test;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.StoredRecordDO;


public class StoredRecordDOTestCase extends BaseTestAssert {
    public void testInstantinate() {
    	StoredRecordDO sr = new StoredRecordDO();
    	sr.setExtendedProperties(new HashMap<String, Object>());
    	sr.getExtendedProperties().put("habahaba", new Integer(12));
    	ActionDO action = em.find(ActionDO.class, 1);
		sr.setAction(action );
		em.getTransaction().begin();
    	em.persist(sr);
    	em.getTransaction().commit();
    	int id = sr.getId();
    	StoredRecordDO sr1 = em.find(StoredRecordDO.class, id);
    	System.out.println(sr1.getExtendedProperties());
    }
    
    public void testEviction() {
    	ActionDO act = new ActionDO();
    	act.setName("Test action");
    	EntityManager em1 = emf.createEntityManager();
    	em1.getTransaction().begin();
    	em1.persist(act);
    	em1.getTransaction().commit();
    	em1.close();
    	
    	int id = act.getId();
    	
    	EntityManager em2 = emf.createEntityManager();
    	ActionDO action = em2.find(ActionDO.class, id);
    	assertEquals(action.getName(), act.getName());

    	String newName = "Test action 2";
    	em2.getTransaction().begin();
    	int updated = em2.createNativeQuery("update actions set name = '"+newName+"' where id = "+ id).executeUpdate();
    	em2.getTransaction().commit();
    	assertFalse(updated == 0);
    	em2.close();
    	
    	EntityManager em3 = emf.createEntityManager();
    	ActionDO action1 = em3.find(ActionDO.class, id);
    	assertEquals(action1.getName(), newName);
    	em3.getTransaction().begin();
    	em3.remove(action1);
    	em3.getTransaction().commit();
    	em3.close();
    	
    }
    
//    @Test
    public void testListReport() {
    	EntityManager em3 = emf.createEntityManager();
    	Calendar cal = Calendar.getInstance();
    	cal.set(2014, 4, 8,0,0,0);
//    	cal.set(Calendar.MILLISECOND,0);
    	Date day = cal.getTime();
    	ObjectDO object = em3.find(ObjectDO.class, 684);
		Query q = null;
		q = em3.createNativeQuery("( select r.* from record r, object_relation orel where orel.parent_id = :objectId and r.object_id = orel.child_id and r.status = 'S' "
				+ "and r.day = :day order by r.day,r.hour,r.start_time, orel.sorter )"
				+ " union all "
				+ " (select r.* from record r where r.object_id = :objectId and r.status = 'S' and r.day = :day order by r.day,r.hour,r.start_time )" ,StoredRecordDO.class);
		q.setParameter("day", day);
		q.setParameter("objectId", object.getObjectId());
		List<StoredRecordDO> list = q.getResultList();
		System.out.println(list);
    }
}
