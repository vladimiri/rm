package pro.deta.detatrak.test;

import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

import org.junit.Test;

import ru.yar.vi.rm.data.SiteDO;

public class SiteDOTestCase extends BaseTestAssert {
	public void testCaching() {
		SiteDO s = new SiteDO();
		s.setName("New SiteDO");
		try {
			
			em.getTransaction().begin();
			em.persist(s);
			int id = s.getId();
			em.getTransaction().commit();

			EntityManager em1 = emf.createEntityManager();
			SiteDO s1 = em1.find(SiteDO.class, id);
			assertEquals(s.getName(), s1.getName());
			em1.close();

			String newName = "new SiteDO1";
			DataSource ds = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/RecordManagerDS");
			Connection con = ds.getConnection();
			PreparedStatement st = con.prepareStatement("update site set name = '"+newName+"' where id = "+id);
			st.executeUpdate();
			st.close();con.close();

			EntityManager em3 = emf.createEntityManager();
			SiteDO s2 = em3.find(SiteDO.class, id);
//			assertNotEquals(s.getName(), s2.getName()); // caching disabled
			assertEquals(s.getName(), s2.getName()); // caching enabled
			em3.close();
			
			emf.getCache().evict(SiteDO.class, id);
			EntityManager em4 = emf.createEntityManager();
			SiteDO s3 = em4.find(SiteDO.class, id);
			assertNotEquals(s.getName(), s3.getName()); // caching disabled
			em4.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.getTransaction().begin();
			em.remove(s);
			em.getTransaction().commit();

		}

	}
}
