import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


public class SplitToMain {

	public static void main(String[] args) {
		String str = "673746asdasd27dаа вова 874 23";
		int num = 2;
		
		splitString(str, num);
	}

	private static void splitString(String str, int num) {
		Pattern pat = Pattern.compile("(\\S{"+num+"})\\s*");
		String array = pat.matcher(str).replaceAll("$1 ");
		String realArr[] = array.split(" ");
		System.out.println(StringUtils.join(realArr," | "));
	}

}
