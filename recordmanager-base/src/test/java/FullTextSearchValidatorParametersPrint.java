import pro.deta.detatrak.validator.FullTextMatchParameter;
import pro.deta.detatrak.validator.FullTextMatchValidatorParameters;

import com.google.gson.Gson;


public class FullTextSearchValidatorParametersPrint {

	public static void main(String[] args) {
		FullTextMatchValidatorParameters avp = new FullTextMatchValidatorParameters();
		avp.setLimitRank(4.0);
		avp.getMatchParameters().add(new FullTextMatchParameter("name"));
		avp.getMatchParameters().add(new FullTextMatchParameter("info(document_no)",FullTextMatchParameter.splitToOneTwo));
		System.out.println(new Gson().toJson(avp));
	}

}
