package pro.deta.detatrak.user.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;



public class ApplicationServiceForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2276640797058847264L;
	private String action;
	private List<ru.yar.vi.rm.user.form.ApplicationServiceForm> forms = new ArrayList<ru.yar.vi.rm.user.form.ApplicationServiceForm>();
	private List<String> result = new ArrayList<String>();
	private String user ="_system";
	private String pass = "";
	private String driver = "com.intersys.jdbc.CacheDriver";
	private String url ="jdbc:Cache://127.0.0.1:1972/region";
	private String regionUser = "";
	private FormFile personFile;
	private FormFile vehicleFile;
	
	
	public String getRegionUser() {
		return regionUser;
	}
	public void setRegionUser(String regionUser) {
		this.regionUser = regionUser;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<ru.yar.vi.rm.user.form.ApplicationServiceForm> getForms() {
		return forms;
	}
	public void setForms(List<ru.yar.vi.rm.user.form.ApplicationServiceForm> forms) {
		this.forms = forms;
	}
	public List<String> getResult() {
		return result;
	}
	public void setResult(List<String> result) {
		this.result = result;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public FormFile getPersonFile() {
		return personFile;
	}
	public void setPersonFile(FormFile personFile) {
		this.personFile = personFile;
	}
	public FormFile getVehicleFile() {
		return vehicleFile;
	}
	public void setVehicleFile(FormFile vehicleFile) {
		this.vehicleFile = vehicleFile;
	}
	
	
}
