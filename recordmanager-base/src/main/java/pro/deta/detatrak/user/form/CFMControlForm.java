package pro.deta.detatrak.user.form;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.ActionStatisticDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class CFMControlForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6478381524200752129L;
	private List<ObjectDO> objects = new ArrayList<ObjectDO>();
	private int officeId;
	private int objectId;
	private String login;
	private String module;
	private int actionId;
	private List<ActionStatisticDO> stat = new ArrayList<ActionStatisticDO>();
	private String action;
	private int siteId;
	private List<SiteDO> sites = null;
	private SiteDO site;
	private ObjectDO selectedObject;
	private StoredRecordDO currentRecord;
	private int redirectTo;
	private int selectedRec;

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		actionId = 0;
		this.action = "";
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public List<ObjectDO> getObjects() {
		return objects;
	}

	public void setObjects(List<ObjectDO> objects) {
		this.objects = objects;
	}

	public int getObjectId() {
		return objectId;
	}

	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}

	public List<ActionStatisticDO> getStat() {
		return stat;
	}

	public void setStat(List<ActionStatisticDO> stat) {
		this.stat = stat;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	public SiteDO getSite() {
		return site;
	}

	public void setSite(SiteDO site) {
		this.site = site;
	}

	public ObjectDO getSelectedObject() {
		return selectedObject;
	}

	public void setSelectedObject(ObjectDO selectedObject) {
		this.selectedObject = selectedObject;
	}

	public StoredRecordDO getCurrentRecord() {
		return currentRecord;
	}

	public void setCurrentRecord(StoredRecordDO currentRecord) {
		this.currentRecord = currentRecord;
	}

	public List<SiteDO> getSites() {
		return sites;
	}

	public void setSites(List<SiteDO> sites) {
		this.sites = sites;
	}

	public int getRedirectTo() {
		return redirectTo;
	}

	public void setRedirectTo(int redirectTo) {
		this.redirectTo = redirectTo;
	}

	public int getSelectedRec() {
		return selectedRec;
	}

	public void setSelectedRec(int selectedRec) {
		this.selectedRec = selectedRec;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
