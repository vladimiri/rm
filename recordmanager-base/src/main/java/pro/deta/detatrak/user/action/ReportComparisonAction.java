package pro.deta.detatrak.user.action;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.upload.FormFile;

import pro.deta.detatrak.DETAConfig;
import pro.deta.detatrak.data.FileRecord;
import pro.deta.detatrak.data.RankInfo;
import pro.deta.detatrak.data.ReportMatch;
import pro.deta.detatrak.user.form.ReportComparisonForm;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.CriteriaDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.model.DateWrapper;
import ru.yar.vi.rm.model.NumberWrapper;
import au.com.bytecode.opencsv.CSVReader;

public class ReportComparisonAction extends Action {
	public static final Logger logger = Logger.getLogger(ReportComparisonAction.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ReportComparisonForm uf = (ReportComparisonForm) form;
		
		ActionMessages ams = new ActionMessages();

		
		boolean blockNext = matchReport(uf, ams,request);
		
		
		if(!ams.isEmpty()) {
			saveErrors(request, ams);
			if(blockNext && ((request.getUserPrincipal()==null || request.getUserPrincipal().getName() == null) ))
				return mapping.getInputForward();
		}
		return mapping.findForward("success");
	}

	private boolean matchReport(ReportComparisonForm uf, ActionMessages ams, HttpServletRequest request) {
		if(uf.getFile() != null) {
			RecordDAO recDao = new RecordDAO(request);
			List<ReportMatch> matchList = new ArrayList<ReportMatch>();
			FormFile inFile = uf.getFile();
			if(inFile.getFileSize() > 0) {
				DictDAO dao = new DictDAO();
				HDAO hdao = new HDAO(request);
				try {
					uf.setCustomers(dao.getCustomers(UserHelper.getCurrentSiteId(request)));
					uf.setRegions(dao.getRegions(UserHelper.getCurrentSiteId(request)));
					uf.setActions(dao.getActions(UserHelper.getCurrentSiteId(request),Security.OPER));
					uf.setCriterias(hdao.getAll(CriteriaDO.class));
				} finally {
					dao.disconnect();
				}
				
				try {
					DETAConfig config = new DETAConfig();
					InputStream is = inFile.getInputStream();
					BufferedReader isr = new BufferedReader(new InputStreamReader(is, config.getComparisonReportEncoding()));
					Double rankVal = new NumberWrapper(uf.getRank()).getDouble();
					Integer limit  = new NumberWrapper(uf.getLimit()).getInteger();
					
					char separator = ';';
					if(uf.getSeparator() != null && uf.getSeparator().length() > 0)
						separator = uf.getSeparator().charAt(0);
					char quotechar = '\'';
					if(uf.getQuotechar() != null && uf.getQuotechar().length() > 0)
						quotechar = uf.getQuotechar().charAt(0);
					CSVReader reader = new CSVReader(isr,separator,quotechar);
					String[] line = null;
					while((line = reader.readNext()) != null) {
						try {
							FileRecord record = parseLine(line);
							List<RankInfo> found = recDao.searchRecord(record, rankVal, limit);
							if(found != null)
								matchList.add(new ReportMatch(record,found));
						} catch(IllegalArgumentException e) {
							logger.error("Error while parsing file", e);
							ams.add("file",new ActionMessage("error.reportComparison.fileParsing",e.getMessage(),line));
						} catch(Exception e) {
							logger.error("Error while parsing file", e);
							ams.add("file",new ActionMessage("error.reportComparison.general",e.getMessage(),StringUtils.join(line,",")));
							return false;
						}
					}
				} catch (Exception e) {
					logger.error("Error while parsing file", e);
					ams.add("file",new ActionMessage("error.reportComparison",e.getMessage()));
				}

				uf.setMatches(matchList);
			}
		}
		if("refresh".equalsIgnoreCase(uf.getAction())) {
			List<ReportMatch> matchList = uf.getMatches();
			List<ReportMatch> resultMatchList = new ArrayList<ReportMatch>();
			RecordDAO recDao = new RecordDAO(request);
			Double rankVal = new NumberWrapper(uf.getRank()).getDouble();
			Integer limit  = new NumberWrapper(uf.getLimit()).getInteger();
			for (ReportMatch reportMatch : matchList) {

				List<RankInfo> found = recDao.searchRecord(reportMatch.getFileRecord(),rankVal,limit);
				if(found != null)
					resultMatchList.add(new ReportMatch(reportMatch.getFileRecord(),found));
			}
			uf.setMatches(resultMatchList);
		}
		
		return false;
	}

	private FileRecord parseLine(String[] array) throws Exception {
		if(array.length < 7) {
			throw new Exception("String length less than required");
		}
		FileRecord fr = new FileRecord();
		fr.setDate(new DateWrapper(array[0]).getDate());
		fr.setOperation(array[1]);
		fr.setVehicleType(array[2]);
		fr.setPtsSeries(array[3]);
		fr.setPtsNumber(array[4]);
		fr.setFamilyName(array[5]);
		fr.setName(array[6]);
		if(array.length > 7)
			fr.setFatherName(array[7]);
		return fr;
	}
}
