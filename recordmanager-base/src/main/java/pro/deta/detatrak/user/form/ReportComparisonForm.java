package pro.deta.detatrak.user.form;

import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import pro.deta.detatrak.data.ReportMatch;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.CriteriaDO;

public class ReportComparisonForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String action = null;
	private FormFile file = null;
	private List<ReportMatch> matches;
	private List<CriteriaDO> criterias = null;
	private List<BaseDO> actions = null;
	private List<BaseDO> regions = null;
	private List<BaseDO> customers = null;
	private String rank = "0.11";
	private String limit = "10";
	private String separator = ";";
	private String quotechar = "\"";
	
	public FormFile getFile() {
		return file;
	}
	public void setFile(FormFile file) {
		this.file = file;
	}
	public List<ReportMatch> getMatches() {
		return matches;
	}
	public void setMatches(List<ReportMatch> matches) {
		this.matches = matches;
	}
	public List<CriteriaDO> getCriterias() {
		return criterias;
	}
	public void setCriterias(List<CriteriaDO> criterias) {
		this.criterias = criterias;
	}
	public List<BaseDO> getActions() {
		return actions;
	}
	public void setActions(List<BaseDO> actions) {
		this.actions = actions;
	}
	public List<BaseDO> getRegions() {
		return regions;
	}
	public void setRegions(List<BaseDO> regions) {
		this.regions = regions;
	}
	public List<BaseDO> getCustomers() {
		return customers;
	}
	public void setCustomers(List<BaseDO> customers) {
		this.customers = customers;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getSeparator() {
		return separator;
	}
	public void setSeparator(String separator) {
		this.separator = separator;
	}
	public String getQuotechar() {
		return quotechar;
	}
	public void setQuotechar(String quotechar) {
		this.quotechar = quotechar;
	}
	
	
}
