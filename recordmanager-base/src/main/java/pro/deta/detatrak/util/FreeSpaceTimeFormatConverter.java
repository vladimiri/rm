package pro.deta.detatrak.util;

import java.lang.ref.SoftReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import pro.deta.detatrak.CacheContainer;

public class FreeSpaceTimeFormatConverter {
	private static final ThreadLocal<SoftReference<DateFormat>> threadLocal = new ThreadLocal<SoftReference<DateFormat>>();
	
	public static DateFormat getFormat() {
		SoftReference<DateFormat> ref = threadLocal.get();
		if (ref != null) {
			DateFormat result = ref.get();
			if (result != null) {
				return result;
			}
		}
		DateFormat result = initDateFormat();
		ref = new SoftReference<DateFormat>(result);
		threadLocal.set(ref);
		return result;
	}

	protected static DateFormat initDateFormat() {
		String pattern = CacheContainer.getInstance().getConfig("freeSpaceFormatTime");
		String locale = CacheContainer.getInstance().getConfig("locale");
		SimpleDateFormat df = null;
		if(locale != null)
			df = new SimpleDateFormat(pattern,new Locale(locale));
		else
			df = new SimpleDateFormat(pattern);
		return df;
	}

}
