package pro.deta.detatrak.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import pro.deta.security.SecurityElement;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.RoleDO;
import ru.yar.vi.rm.data.UserDO;

public class SecurityUtil {
	private static final Logger logger = Logger.getLogger(SecurityUtil.class);

	public static boolean hasPermission(ServletRequest request,
			SecurityElement element, String module) {
		HttpServletRequest req;
		EntityManager sess = UserHelper.getSession(request);
		// Dynamic Security Elements
		List<SecurityElement> additionalList = new ArrayList<>();
		if(UserHelper.isSelf(module))
			additionalList.add(SecurityElement.SELF_MODULE);
		else if(UserHelper.isOper(module))
			additionalList.add(SecurityElement.OPER_MODULE);
		else
			additionalList.add(SecurityElement.MAIN_MODULE);

		// end of Dynamic Security Elements
		if(request instanceof HttpServletRequest && sess != null) {
			req = (HttpServletRequest) request;
			if(req.getUserPrincipal() != null) {
				String login = req.getUserPrincipal().getName();
				UserDO user = (UserDO) sess.find(UserDO.class,login);
				additionalList.addAll(DataUtil.getRestrictions(user));
			} else {
				RoleDO guestRole = (RoleDO) sess.find(RoleDO.class,"guest");
				if(guestRole != null && guestRole.getSecurityElements() != null) {
					additionalList.addAll(guestRole.getSecurityElements());
				} else {
					logger.error("Role 'guest' not found.");
				}
			}
			boolean result = DataUtil.matchElement(additionalList,element);
			return result;
		}

		return false;
	}

}
