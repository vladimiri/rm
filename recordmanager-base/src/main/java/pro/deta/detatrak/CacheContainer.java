package pro.deta.detatrak;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import pro.deta.detatrak.dao.data.T2;
import pro.deta.detatrak.data.ActionQueueDO;
import pro.deta.detatrak.data.ObjectTypeDateDO;
import pro.deta.detatrak.data.OfficeStatDO;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.ConfigDAO;
import ru.yar.vi.rm.dao.FreeSpaceDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.OnlineDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.ObjectTypeItemDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.exception.NoLinesAvailableException;
import ru.yar.vi.rm.model.BaseFreeSpaceModel;
import ru.yar.vi.rm.model.NumberWrapper;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheLoader.InvalidCacheLoadException;
import com.google.common.cache.LoadingCache;
/**
 *  
 * @author vladimiri
 *
 */
public class CacheContainer {
	private static final Logger logger = Logger.getLogger(CacheContainer.class);
	private static CacheContainer container = null;
	private static final Object _lock = new Object();

	public static CacheContainer getInstance() {
		if(container == null)
			synchronized (_lock) {
				if(container == null) {
					container = new CacheContainer();
				}
			}
		return container;
	}

	LoadingCache<T2<Integer, Integer>,Integer> priorityCache;
	LoadingCache<String,String> configurationCache;
	LoadingCache<Integer,OfficeStatDO> statisticsCache;

	public CacheContainer() {
		Integer timeout = new NumberWrapper(new ConfigDAO().getConfig("cache.timeout","3600")).getInteger();
		if(timeout == null || timeout == 0)
			timeout = 600;
		
		configurationCache = CacheBuilder.newBuilder()
				.expireAfterWrite(timeout, TimeUnit.SECONDS)
				.build(new CacheLoader<String,String>() {
					@Override
					public String load(String key) throws Exception {
						return new ConfigDAO().getConfig(key);
					}
				});
		priorityCache = CacheBuilder.newBuilder()
				.expireAfterWrite(timeout, TimeUnit.SECONDS)
				.build(new CacheLoader<T2<Integer, Integer>,Integer>() {
					@Override
					public Integer load(T2<Integer, Integer> key) throws Exception {
						FreeSpaceDAO dao = null;
						try {
							dao = new FreeSpaceDAO();
							return dao.getPriority(key.getFirst(), key.getSecond());
						} finally {
							dao.disconnect();
						}
					}
				});
		statisticsCache = CacheBuilder.newBuilder()
				.expireAfterWrite(30, TimeUnit.SECONDS)
				.build(new CacheLoader<Integer,OfficeStatDO>() {
					@Override
					public OfficeStatDO load(Integer officeId) throws Exception {
						HDAO dao = null;
						OnlineDAO onlineDAO = null;
						OfficeStatDO stat = new OfficeStatDO();
						try {
							onlineDAO = new OnlineDAO();
							dao = new HDAO(JPAFilter.createEntityManager());
							stat.setOffice(dao.getById(OfficeDO.class, officeId));
							List<ActionDO> actions = dao.getAll(ActionDO.class);
							stat.setActionQueue(new ArrayList<ActionQueueDO>());
							for (ActionDO actionDO : actions) {
								ActionQueueDO aq = new ActionQueueDO();
								aq.setAction(actionDO);
								stat.getActionQueue().add(aq);
								if(!StringUtils.isEmpty(actionDO.getCode())) {
									int queueSize = onlineDAO.getCountRecords(actionDO.getId(),officeId,new Date(),StoredRecordDO.RECORD_STATUS_WAITING);
									aq.setCfmQueueSize(queueSize);
									aq.setPluralQueueSize(UserHelper.plurals(new Long(queueSize)));
								}
								aq.setHasTime(false);
								aq.setTypeDateList(new ArrayList<ObjectTypeDateDO>());
								for (ObjectTypeItemDO item : actionDO.getType()) {
									BaseFreeSpaceModel bfsm = new BaseFreeSpaceModel(Security.ALL, item.getType(), UserHelper.getCurrentCalendar().getTime());
									try {
										List<FreeSpaceDO> list = bfsm.getFreeSpaceList(actionDO,new Date(), stat.getOffice());
										if(!list.isEmpty()) {
											ObjectTypeDateDO otd = new ObjectTypeDateDO();
											FreeSpaceDO freeSpace = list.get(0);
											otd.setType(item.getType());
											otd.setAvailable(UserHelper.freeSpaceToDate(freeSpace));
											aq.getTypeDateList().add(otd);
											aq.setHasTime(true);
										}
									} catch (NoLinesAvailableException e) {
										continue;
									}
								}
								
							}
						} finally {
							dao.disconnect();
							onlineDAO.disconnect();
						}
						return stat;
					}
				});
	}

	public OfficeStatDO getOfficeStat(Integer officeId) {
		try {
			return statisticsCache.get(officeId);
		} catch(InvalidCacheLoadException e) {
			return null;
		} catch (Exception e) {
			logger.error("Error while retrieve statistics cache value by "+officeId,e);
		}
		return null;
	}
	
	public SiteDO getSite(ServletRequest req,Integer siteId) {
		EntityManager session = UserHelper.getSession(req);
		SiteDO find = session.find(SiteDO.class, siteId);
		session.detach(find);
		return find;
	}

	public String getConfig(String key) {
		try {
			return configurationCache.get(key);
		} catch(InvalidCacheLoadException e) {
			return null;
		} catch (Exception e) {
			logger.error("Error while retrieve configuration cache value by "+key,e);
		}
		return null;
	}

	public Integer getPriority(int actionId,int objectId) {
		try {
			return priorityCache.get(new T2<Integer, Integer>(actionId, objectId));
		} catch(InvalidCacheLoadException e) {
			return null;
		} catch (Exception e) {
			logger.error("Error while retrieve configuration cache value by "+new T2<Integer, Integer>(actionId, objectId),e);
		}
		return 0;
	}
	
	public String getConfig(String key,String def) {
		String ret = null;
		try {
			ret = configurationCache.get(key);
		} catch(InvalidCacheLoadException e) {
			//			return null;
		} catch (ExecutionException e) {
			logger.error("Error while retrieve configuration cache value by "+key,e);
		}
		if(ret == null)
			ret = def;
		return ret;
	}

}
