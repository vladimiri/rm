package pro.deta.detatrak;

import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Log4jConfigureContextListener implements ServletContextListener {
	public void contextInitialized(ServletContextEvent e) {
		try {
			String log4jFile = e.getServletContext().getInitParameter("log4jFileName");
			URL logConfig = e.getServletContext().getResource(log4jFile);
			System.out.println("Logger initializing with" + logConfig);
			LogInitializer.initialize(logConfig);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	// Release the EntityManagerFactory:
	public void contextDestroyed(ServletContextEvent e) {
	}
}
