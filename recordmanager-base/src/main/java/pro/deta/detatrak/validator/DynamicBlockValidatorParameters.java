package pro.deta.detatrak.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * {"blockingList":[{"subject":"IP","limit":5,"interval":10000,"duration":100000},{"subject":"IP","limit":5,"interval":10000,"duration":100000}]}
 * 
 * 
 * @author vladimiri
 *
 */
public class DynamicBlockValidatorParameters implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8532545469177495121L;
	private List<BlockingRule> blockingList = new ArrayList<>();

	public List<BlockingRule> getBlockingList() {
		return blockingList;
	}

	public void setBlockingList(List<BlockingRule> blockingList) {
		this.blockingList = blockingList;
	}
	
}

