package pro.deta.detatrak.validator;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

public class MaxNumberRecordsValidator extends CustomValidator {
	public static final Logger logger = Logger.getLogger(MaxNumberRecordsValidator.class);
	private static Map<String,String> queryMap = new HashMap<String, String>();
	
	
	static {
		queryMap.put("SIMPLE_UNIQUE", "from StoredRecordDO rec where lower(translate(rec.info[:param], ' ', '')) like '%'||lower(translate(:value, ' ', ''))||'%' "
				+ "and rec.status = 'S' and (day > current_date or (day = current_date and hour > date_part('hour',current_time)))");
	}
	
	public ValidationResult validate(UserForm uf, HttpServletRequest request, ValidationResult result,ValidatorDO validator) {
		MaxNumberRecordsValidatorParameters parameters = getParameters(MaxNumberRecordsValidatorParameters.class, validator);
		
		HDAO recDao = new HDAO(request);
		List<StoredRecordDO> list = null;
		try {
			String value = uf.getInfoByKey(parameters.getParameterName());
			if(StringUtils.isEmpty(value)) {
				logger.error("Error while executing MaxNumberRecordsValidator field to check is empty for " + uf.getSelectedAction() +" for parameter " + parameters.getParameterName());
				result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(validator.getError(),false));
				result.setBlock();
				return result;
			}
// GIBDD query
// from StoredRecordDO rec where lower(translate(rec.info.pts, ' ', '')) like '%'||lower(translate(:pts, ' ', ''))||'%' 
// and rec.status = 'S' and day >= current_date
		
// FMS Query
// from StoredRecordDO where info.documentNo = :value and status = 'S' and day > current_date() order by day,hour,start
			String query = parameters.getQuery();
			String parameterKey = null;
			if(queryMap.containsKey(query)) {
				query = queryMap.get(query);
				parameterKey = parameters.getParameterName();
			}
			list = recDao.getRecords(query,	value, parameterKey);
		} catch(Exception e) {
			logger.error("Error while searching by " +parameters.getParameterName() +" with query " + parameters.getQuery(), e);
			result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(validator.getError(),false));
			result.setBlock();
			return result;
		}

		try {
			if(!list.isEmpty()) {
				for (StoredRecordDO rec : list) {
					String office = rec.getObject().getOffice().getName();
					String object = rec.getObject().getName();
					String error = MessageFormat.format(validator.getError(), UserHelper.format(rec.getDay(), rec.getHour(), rec.getStart()),
							office,object);
					result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(error,false));
				}
				if(list.size() >= parameters.getMaxOccurences())
					result.setBlock();
			}
		} catch (Exception e) {
			logger.error("Error while searching by " +parameters.getParameterName() +" with query " + parameters.getQuery(), e);
			result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(validator.getError(),false));
			result.setBlock();
		}
		return result;
	}

}
