package pro.deta.detatrak.validator;

public class ValidatorParameters {
	private String parameterName;
	
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
}
