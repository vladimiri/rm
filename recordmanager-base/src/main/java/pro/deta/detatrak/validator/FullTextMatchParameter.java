package pro.deta.detatrak.validator;

import java.util.ArrayList;
import java.util.List;

public class FullTextMatchParameter {
	public static final List<Integer> splitToOne = new ArrayList<Integer>();
	public static final List<Integer> splitToOneTwo = new ArrayList<Integer>();
	
	static{
		splitToOne.add(1);
		splitToOneTwo.add(1);
		splitToOneTwo.add(2);
	}
	
	private String name;
	private List<Integer> splitToChars = new ArrayList<Integer>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getSplitToChars() {
		return splitToChars;
	}

	public void setSplitToChars(List<Integer> splitToChars) {
		this.splitToChars = splitToChars;
	}
	public FullTextMatchParameter() {
	}
	public FullTextMatchParameter(String name) {
		this.name = name;
	}
	
	public FullTextMatchParameter(String name,List<Integer> splitToChars) {
		this.name = name;
		this.splitToChars = splitToChars;
	}
}