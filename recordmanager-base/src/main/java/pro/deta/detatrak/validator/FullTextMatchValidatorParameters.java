package pro.deta.detatrak.validator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FullTextMatchValidatorParameters implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7389768136720241942L;
	private List<Double> weights = new ArrayList<Double>();
	private List<FullTextMatchParameter> matchParameters = new ArrayList<FullTextMatchParameter>();
	private Double limitRank = 4.0;
	private boolean disjunction = true; // true = OR, false = AND
	
	public List<Double> getWeights() {
		return weights;
	}

	public void setWeights(List<Double> weights) {
		this.weights = weights;
	}

	public List<FullTextMatchParameter> getMatchParameters() {
		return matchParameters;
	}

	public void setMatchParameters(List<FullTextMatchParameter> matchParameters) {
		this.matchParameters = matchParameters;
	}


	public Double getLimitRank() {
		return limitRank;
	}

	public void setLimitRank(Double limitRank) {
		this.limitRank = limitRank;
	}

	public boolean isDisjunction() {
		return disjunction;
	}

	public void setDisjunction(boolean disjunction) {
		this.disjunction = disjunction;
	}

	{
		weights.add(new Double(0.1)); // D-group
		weights.add(new Double(0.2)); // C-group
		weights.add(new Double(0.4)); // B-group
		weights.add(new Double(1));   // A-group
	}
	

}

