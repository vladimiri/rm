package pro.deta.detatrak.validator;

public class TimeDifferenceValidatorParameters {
	int difference;

	public int getDifference() {
		return difference;
	}

	public void setDifference(int difference) {
		this.difference = difference;
	}
	
}
