package pro.deta.detatrak.validator;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class BlockingRule  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3797579235489689292L;
	private String subject="";
	private int limit = 0;
	private long interval;
	private long duration;


	public long getDuration() {
		return duration;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public long getInterval() {
		return interval;
	}
	public void setInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BlockingRule){
			final BlockingRule other = (BlockingRule) obj;
			return new EqualsBuilder()
			.append(duration, other.duration)
			.append(interval, other.interval)
			.append(limit, other.limit)
			.append(subject, other.subject)
			.isEquals();
		} else{
			return false;
		}
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.append(subject);
		hashCodeBuilder.append(limit);
		hashCodeBuilder.append(interval);
		hashCodeBuilder.append(duration);
		return hashCodeBuilder.toHashCode();
	}
	
	public String toString() {
		return "BlockingRule [subject=" + subject + ", limit=" + limit
				+ ", interval=" + interval + ", duration=" + duration + "]";
	}

}