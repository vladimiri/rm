package pro.deta.detatrak.validator;

import java.text.DateFormat;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

public abstract class CustomValidator {
	/**
	 * Вызывается после введения пользователем персональных данных на шаге 2
	 * @param uf
	 * @param request
	 * @param result
	 * @param validator
	 * @return
	 * @throws Exception
	 */
	
	public ValidationResult validate(UserForm uf,HttpServletRequest request, ValidationResult result,ValidatorDO validator) throws Exception {
		return result;
	}
	/**
	 * Вызывается после выбора пользователем времени записи
	 * 
	 * @param uf
	 * @param request
	 * @param result
	 * @param validator
	 * @return
	 * @throws Exception
	 */
	public ValidationResult validatePreEnd(UserForm uf,HttpServletRequest request, ValidationResult result,ValidatorDO validator) throws Exception {
		return result;
	}
	
	/**
	 * Вызывается перед отображением списка доступного времени для записи
	 * @param uf
	 * @param request
	 * @param validator
	 * @param rec
	 * @param validationResult
	 * @throws Exception
	 */
	public void filterSchedule(UserForm uf, HttpServletRequest request, ValidatorDO validator, RecordDO rec, ValidationResult validationResult) throws Exception {
	}
	
	
	
	public <T> T getParameters(Class<T> cls,ValidatorDO action) {
		return new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.FULL).create().fromJson(action.getParameter(), cls);
	}
}
