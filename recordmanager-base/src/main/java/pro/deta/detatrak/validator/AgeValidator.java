package pro.deta.detatrak.validator;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

public class AgeValidator  extends CustomValidator {
	public static final Logger logger = Logger.getLogger(AgeValidator.class);
	
	public ValidationResult validate(UserForm uf, HttpServletRequest request, ValidationResult result,ValidatorDO validator) {
		AgeValidatorParameters parameters = getParameters(AgeValidatorParameters.class, validator);
		
		try {
			String value = uf.getInfoByKey(parameters.getParameterName());
			if(StringUtils.isEmpty(value)) {
				logger.error("Error while executing AgeValidator field to check is empty for " + uf.getSelectedAction() +" for parameter " + parameters.getParameterName());
				result.setBlock();
				return result;
			}
			Date dt = UserHelper.parseDate(value,parameters.getPattern());
			long dateToCompate = dt.getTime();
			Calendar cal = UserHelper.getCurrentCalendar();
			long currentDate = cal.getTimeInMillis();
			
			if(parameters.getMinAge() >= 0) {
				int diff = (int) ((currentDate - dateToCompate)/1000/60/60/24);
				if(diff < parameters.getMinAge()) {
					String error = MessageFormat.format(validator.getError(), dt);
					result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(error,false));
					result.setBlock();
				}	
			}
			if(parameters.getMaxAge() >= 0) {
				int diff = (int) ((currentDate - dateToCompate)/1000/60/60/24);
				if(diff > parameters.getMaxAge()) {
					String error = MessageFormat.format(validator.getError(), dt);
					result.getMessages().add("info("+parameters.getParameterName()+")",new ActionMessage(error,false));
					result.setBlock();
				}	
			}
			
			
// GIBDD query
// from StoredRecordDO rec where lower(translate(rec.info.pts, ' ', '')) like '%'||lower(translate(:pts, ' ', ''))||'%' 
// and rec.status = 'S' and day >= current_date
			
// FMS Query
// from StoredRecordDO where info.documentNo = :value and status = 'S' and day > current_date() order by day,hour,start

		} catch(Exception e) {
			logger.error("Error while validating " +parameters.getParameterName() , e);
			result.setBlock();
			return result;
		}

		
		return result;
	}

}
