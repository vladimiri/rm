package pro.deta.detatrak.validator;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;

import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

import com.google.gson.Gson;

public class FullTextMatchValidator  extends CustomValidator {
	public static final Logger logger = Logger.getLogger(FullTextMatchValidator.class);
	
	public ValidationResult validate(UserForm uf, HttpServletRequest request, ValidationResult result,ValidatorDO validator) {
		FullTextMatchValidatorParameters parameters = getParameters(FullTextMatchValidatorParameters.class, validator);
		
		try {
			List<String> parameterVariants = new ArrayList<String>();
			for (FullTextMatchParameter param : parameters.getMatchParameters()) {
				String value = (String) PropertyUtils.getProperty(uf, param.getName());
				parameterVariants.add(splitStringToSpaces((value)));
				if(param.getSplitToChars() != null && !param.getSplitToChars().isEmpty()) {
					for (Integer splitTo : param.getSplitToChars()) {
						String variant = splitString(value, splitTo);
						parameterVariants.add(variant);
					}
				}
			}
			String tsQuery = StringUtils.join(parameterVariants, " | ");
			logger.debug("Validation query is :" + tsQuery);
			RecordDAO dao = new RecordDAO(request);
			String weights = joinWeights(parameters.getWeights());
			List<StoredRecordDO> records = dao.matchFullText(tsQuery,weights,parameters.getLimitRank(),uf.getActionId());
			if(records != null && !records.isEmpty()) {
				String error = MessageFormat.format(validator.getError(),records.get(0).getName());
				result.getMessages().add("name",new ActionMessage(error,false));
				result.setBlock();
				return result;
			}
			
		} catch(Exception e) {
			logger.error("Error while validating " +validator.getParameter() +" on object: " +uf , e);
			result.setBlock();
			return result;
		}

		
		return result;
	}
	
	private String splitString(String str, int num) {
		Pattern pat = Pattern.compile("(\\S{"+num+"})\\s*");
		String array = pat.matcher(str).replaceAll("$1 ");
		return splitStringToSpaces(array);
	}
	private String splitStringToSpaces(String str) {
		String realArr[] = str.split(" ");
		return "(" +StringUtils.join(realArr," | ") +")";
	}
	private String joinWeights(List<Double> str) {
		String str1 = StringUtils.join(str,",");
		return "{" +str1 +"}";
	}
}
