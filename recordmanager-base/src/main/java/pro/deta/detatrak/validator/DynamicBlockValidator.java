package pro.deta.detatrak.validator;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import com.google.gson.Gson;

import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

public class DynamicBlockValidator  extends CustomValidator {
	private static final Logger logger = Logger.getLogger(DynamicBlockValidator.class);

	private static final Map<StatisticKey,StatisticValue> stat = new ConcurrentHashMap<>();
	private static final Map<StatisticKey,Block> blockList = new ConcurrentHashMap<>();
	private static final Object _lock = new Object();

	public ValidationResult validate(UserForm uf,HttpServletRequest request, ValidationResult result,ValidatorDO validator) throws Exception {
		DynamicBlockValidatorParameters parameters = getParameters(DynamicBlockValidatorParameters.class, validator);
		int actionId = uf.getActionId();
		for (BlockingRule rule : parameters.getBlockingList()) {
			String subject = "";
			try {
				switch (rule.getSubject()) {
				case "IP":
					subject = request.getRemoteHost();
					break;
				case "JSESSIONID":
					subject = request.getSession().getId();
					break;
				default:
					subject = uf.getInfoByKey(rule.getSubject());
				}
			} catch (Exception e) {
				logger.error("Can't calculate subject for rule");
			}
			StatisticKey key = new StatisticKey(actionId,subject,rule);
			
			if(blockList.containsKey(key)) {
				Block block = blockList.get(key);
				if(isBlockValid(key,block))
					return failed(result,validator);
				else
					blockList.remove(key);
			}
			
			StatisticValue val = null;
			synchronized (_lock) {
				if(stat.containsKey(key)) {
					val = stat.get(key);
				} else {
					val = new StatisticValue();
					stat.put(key, val);
				}
				if(!applyRule(key,val)) {
					stat.remove(key);
				} else {
					Block b = checkRuleRestriction(key,val);
					if(b != null) {
						blockList.put(key, b);
						stat.remove(key);
						return failed(result, validator);
					}
				}
			}
			
		}
		return result;
	}

	private Block checkRuleRestriction(StatisticKey key, StatisticValue val) {
		int limit = key.getRule().getLimit();
		if (val.getCurrentCounter() >= limit) {
			Block b = new Block();
			b.setStartTime(System.currentTimeMillis());
			return b;
		} else
			return null;
	}

	private boolean applyRule(StatisticKey key, StatisticValue val) {
		long interval = key.getRule().getInterval();
		if(System.currentTimeMillis() <= val.getLastRecordedValue() + interval) {
			val.increment();
			return true;
		}
		return false;
	}

	private ValidationResult failed(ValidationResult result, ValidatorDO validator) {
		result.setBlock();
		result.getMessages().add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage(validator.getError(),false));
		return result;
	}

	private boolean isBlockValid(StatisticKey key, Block val) {
		long duration = key.getRule().getDuration();
		if(System.currentTimeMillis() <= val.getStartTime() + duration)
			return true;
		else 
			return false;
	}

	public static void cleanup() {
		for (Entry<StatisticKey, Block> val : blockList.entrySet()) {
			long endBlockAt = val.getValue().getStartTime() + val.getKey().getRule().getDuration();
			if(endBlockAt < System.currentTimeMillis())
				blockList.remove(val.getKey());
		}
		for(Entry<StatisticKey, StatisticValue> val: stat.entrySet()) {
			long endIntervalAt = val.getValue().getLastRecordedValue() + val.getKey().getRule().getInterval();
			if(endIntervalAt < System.currentTimeMillis()) {
				stat.remove(val.getKey());
			}
		}
	}

	public static String status() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statistics: ").append(new Gson().toJson(stat));
		sb.append("\n\n");
		sb.append("BlockList: ").append(new Gson().toJson(blockList));
		return sb.toString();
	}
}

class StatisticKey {
	private int actionId;
	private String subject;
	private BlockingRule rule;

	public StatisticKey(int actionId2, String subject2, BlockingRule rule2) {
		this.actionId = actionId2;
		this.subject = subject2;
		this.rule = rule2;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public BlockingRule getRule() {
		return rule;
	}

	public void setRule(BlockingRule rule) {
		this.rule = rule;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof StatisticKey){
			final StatisticKey other = (StatisticKey) obj;
			return new EqualsBuilder()
			.append(actionId, other.actionId)
			.append(subject, other.subject)
			.append(rule, other.rule)
			.isEquals();
		} else{
			return false;
		}
	}
	
	@Override
	public String toString() {
		return "StatisticKey [actionId=" + actionId + ", subject=" + subject
				+ ", rule=" + rule + "]";
	}

	@Override
	public int hashCode() {
		HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.append(actionId);
		hashCodeBuilder.append(subject);
		hashCodeBuilder.append(rule);
		return hashCodeBuilder.toHashCode();
	}
}

class StatisticValue {
	private long lastRecordedValue = System.currentTimeMillis();
	private int currentCounter = 0;
	
	public long getLastRecordedValue() {
		return lastRecordedValue;
	}
	public void setLastRecordedValue(long lastRecordedValue) {
		this.lastRecordedValue = lastRecordedValue;
	}
	public int getCurrentCounter() {
		return currentCounter;
	}
	public void setCurrentCounter(int currentCounter) {
		this.currentCounter = currentCounter;
	}

	public void increment() {
		lastRecordedValue = System.currentTimeMillis();
		currentCounter++;
	}

	public void clear() {
		lastRecordedValue = System.currentTimeMillis();
		currentCounter = 1;
	}
}


class Block {
	private long startTime;

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
}

