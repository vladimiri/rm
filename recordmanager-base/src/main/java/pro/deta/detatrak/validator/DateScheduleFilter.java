package pro.deta.detatrak.validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.data.DateBaseDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.user.form.UserForm;

import com.google.gson.Gson;

public class DateScheduleFilter extends CustomValidator {
	public static final Logger logger = Logger.getLogger(DateScheduleFilter.class);
	
	@Override
	public void filterSchedule(UserForm uf, HttpServletRequest request,
			ValidatorDO validator, RecordDO rec,ValidationResult result) {

		AgeValidatorParameters parameters = getParameters(AgeValidatorParameters.class, validator);
		
		try {
			String value = uf.getInfoByKey(parameters.getParameterName());
			if(StringUtils.isEmpty(value)) {
				logger.error("Error while executing DateScheduleFilter field to check is empty for " + uf.getSelectedAction() +" for parameter " + parameters.getParameterName());
			}
			Date dt = UserHelper.parseDate(value,parameters.getPattern());
			long dateToCompate = dt.getTime();
			
			List<DateBaseDO> list = new ArrayList<DateBaseDO>();
			long officeId = uf.getOfficeId();
			for(DateBaseDO db: rec.getOfficeDays().get(officeId)) {
				long currentDate = db.getDate().getTime();
				if(parameters.getMinAge() > 0) {
					int diff = (int) ((currentDate - dateToCompate)/1000/60/60/24);
					if(diff < parameters.getMinAge()) {
						continue;
					}	
				}
				if(parameters.getMaxAge() > 0) {
					int diff = (int) ((currentDate - dateToCompate)/1000/60/60/24);
					if(diff > parameters.getMaxAge()) {
						continue;
					}	
				}
				list.add(db);
			}
			rec.getOfficeDays().put(officeId,list);
		} catch(Exception e) {
			logger.error("Error while validating " +parameters.getParameterName() , e);
		}
	}
	
}
