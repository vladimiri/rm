package ru.yar.vi.rm.user.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.PaymentFormHandler;
import ru.yar.vi.rm.Translit;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PaymentParamDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.user.form.PaymentForm;



public class PaymentAction extends Action {
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		PaymentForm pf = (PaymentForm) form;
		
		if(pf.getPaymentId() > 0 ) {
			PaymentParamDO pp = (PaymentParamDO) UserHelper.getBaseDO(pf.getPaymentId(),pf.getPaymentParam());
			OfficeDO office = (OfficeDO) UserHelper.getBaseDO(pf.getOfficeId(),pf.getOffices());
			
			if(pp != null && pf.getPayer() != null) {
				
				pf.setBik(pp.getBik());
				pf.setAccount(pp.getAccount());
				pf.setBank(pp.getBank());
				pf.setReceiverAccount(pp.getReceiverAccount());
				pf.setReceiverName(pp.getReceiverName().toUpperCase());
				pf.setInn(pp.getInn());
				pf.setKpp(pp.getKpp());
				pf.setKbk(pp.getKbk());

				String payer = Translit.translit(pf.getPayer());
				payer = payer.replace(' ', '_');
				pf.setAddress(pf.getAddress().toUpperCase());
				pf.setReason(pp.getPurpose().toUpperCase());
				pf.setAmount(pp.getAmount());
				pf.setOkato(office.getOkato());
				PaymentFormHandler pfh = new PaymentFormHandler("/zfl.xml");
				response.addHeader("Content-Disposition", "attachment; filename=\"zfl-"+payer+".xml\"");
				response.setContentType("application/vnd.ms-excel");
				pfh.handle(pf,response.getOutputStream());
				response.flushBuffer();
				pf.setPaymentId(0);
				return null;
			} else {
				pf.setPaymentId(0);
				initPaymentForm(request, pf);
			}
		} else {
			initPaymentForm(request, pf);
		}
		return mapping.getInputForward();
	}

	private void initPaymentForm(HttpServletRequest request, PaymentForm pf) {		
		DictDAO dict = new DictDAO();
		try {
			dict.connect();
			pf.setOffices( (List<OfficeDO>) dict.getOffices(Security.ALL.getValue()));
			pf.setActions( dict.getActions(UserHelper.getCurrentSiteId(request),Security.ALL));
			if(pf.getActionId() == 0) {
				pf.setActionId(pf.getActions().get(0).getId());
			}
			pf.setPaymentParam(dict.getPaymentParam(pf.getActionId()));
		} finally {
			dict.disconnect();
		}
		
//		List<PaymentDO> arr = pf.getPayments();
//		arr.clear();
//		arr.add(new PaymentDO(1,"За выдачу загранпаспорта заграничного образца.",2500));
//		arr.add(new PaymentDO(2,"За выдачу загранпаспорта заграничного образца детям до 14 лет.",1200));
////		arr.add(new PaymentDO(3,"За внесение изменений в загранпаспорт.",200));
	}

}
