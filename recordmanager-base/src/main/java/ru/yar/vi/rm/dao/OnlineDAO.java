package ru.yar.vi.rm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import pro.deta.detatrak.dao.data.CFMReportDO;
import ru.yar.vi.rm.data.ActionStatisticDO;
import ru.yar.vi.rm.data.StoredRecordDO;


public class OnlineDAO extends DAO {
	private static final Logger logger = Logger.getLogger(OnlineDAO.class);
	private boolean isTransactional = false;


	public ActionStatisticDO getStatistic(int actionId,int officeId,String status) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select * from record where " +
					"day = current_date and action_id = ? and office_id = ? and status = ? order by creation_date");
			pstmt.setInt(1, actionId);
			pstmt.setInt(2, officeId);
			pstmt.setString(3, status);
			rs = pstmt.executeQuery();
			ActionStatisticDO stat = new ActionStatisticDO();
			while(rs.next()) {
				StoredRecordDO sr = new StoredRecordDO();
				sr.setName(rs.getString("name"));
				sr.setId(rs.getInt("id"));
				sr.setCreationDate(rs.getDate("creation_date"));
				stat.getQueue().add(sr);
			}
			try {
				if(stat.getQueue() != null && stat.getQueue().size() > 0) {
					stat.setCfmQueueSize(stat.getQueue().size());
					stat.setLastCreationDate(stat.getQueue().get(stat.getCfmQueueSize()-1).getCreationDate());
				}
			} catch (Exception e) {
				logger.error("Error while set last creation Date", e);
			}
			return stat;
		} catch (SQLException e) {
			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return new ActionStatisticDO();
	}

	public ActionStatisticDO getCfmQueueLength(int actionId,int officeId,String status) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select count(*) from record where " +
					"day = current_date and action_id = ? and office_id = ? and status = ? ");
			pstmt.setInt(1, actionId);
			pstmt.setInt(2, officeId);
			pstmt.setString(3, status);
			rs = pstmt.executeQuery();
			ActionStatisticDO stat = new ActionStatisticDO();
			while(rs.next()) {
				stat.setCfmQueueSize(rs.getInt(1));
			}
			return stat;
		} catch (SQLException e) {
			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return new ActionStatisticDO();
	}

	public int updateStatus(int recId,int objectId, String status,String author) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("update record set status = ?,update_date = ?,object_id = ?,author = ? where id = ? ");
			int i = 1;
			pstmt.setString(i++, status);
			pstmt.setTimestamp(i++, new java.sql.Timestamp(new Date().getTime()));
			pstmt.setInt(i++, objectId);
			pstmt.setString(i++, author);
			pstmt.setInt(i++, recId);
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
//			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(pstmt);
		}
		return 0;
	}
	
	public StoredRecordDO lockForUpdate(ArrayList<Integer> tmpActionList,
			String status,String status2,int officeId) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			String s = getQuestionmarksQuery(tmpActionList.size(), true);
			pstmt= con.prepareStatement("select * from record where day = current_Date and action_id "+s+" and status in (?,?) and office_id = ? order by creation_date for update limit 1 ");
			int i=1;
			for (Integer integer : tmpActionList) {
				pstmt.setInt(i++, integer);
			}
			pstmt.setString(i++, status);
			pstmt.setString(i++, status2);
			pstmt.setInt(i++, officeId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				StoredRecordDO data = new StoredRecordDO();
				data.setId(rs.getInt("id"));
				data.setName(rs.getString("name"));
				return data;
			}
		} catch (SQLException e) {
			logger.error("Can't get statistic for " + tmpActionList +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}
	
	public StoredRecordDO lockForUpdate(Integer recordId,String status,String status2) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt= con.prepareStatement("select * from record where day = current_Date and id = ? and status in (?,?) order by creation_date for update limit 1 ");
			int i=1;
			pstmt.setInt(i++, recordId);
			pstmt.setString(i++, status);
			pstmt.setString(i++, status2);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				StoredRecordDO data = new StoredRecordDO();
				data.setId(rs.getInt("id"));
				data.setName(rs.getString("name"));
				return data;
			}
		} catch (SQLException e) {
			logger.error("Can't get statistic for " + recordId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}

	public StoredRecordDO getMaxRecord(int actionId, int officeId,
			Date currentDate) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select * from record where day = ? and action_id = ? and office_id = ? order by id desc for update limit 1 ");
			pstmt.setDate(1, new java.sql.Date(currentDate.getTime()));
			pstmt.setInt(2, actionId);
			pstmt.setInt(3, officeId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				StoredRecordDO data = new StoredRecordDO();
				data.setId(rs.getInt("id"));
				data.setName(rs.getString("name"));
				data.setStart(rs.getInt("start_time"));
				return data;
			}
		} catch (SQLException e) {
			e.printStackTrace();
//			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}
	
	public int getCountRecords(int actionId, int officeId,
			Date currentDate,String status) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select count(*) from record where day = ? and action_id = ? and office_id = ? and status = ?");
			pstmt.setDate(1, new java.sql.Date(currentDate.getTime()));
			pstmt.setInt(2, actionId);
			pstmt.setInt(3, officeId);
			pstmt.setString(4, status);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
//			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return 0;
	}
	
    
    public boolean isTransactional() {
    	return isTransactional ;
    }

	public void setTransactional(boolean isTransactional) {
		this.isTransactional = isTransactional;
	}



	public void updateFinalDate(int id, Date date) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("update record set final_date = ? where id = ? ");
			pstmt.setTimestamp(1, new java.sql.Timestamp(date.getTime()));
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
//			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(pstmt);
		}
	}



	public List<CFMReportDO> getReportDO(Date dt) {
		String query = "select day,author,action_id,min(coalesce(creation_date,'1980-01-01')),max(coalesce(final_date,'1980-01-01')),count(*),avg(coalesce(update_date,creation_date)-creation_date) as waitingTime,avg(coalesce(final_date,update_date)-update_date) as processingTime  from record where day = ? group by day,author,action_id";
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		List<CFMReportDO> ret = new ArrayList<CFMReportDO>();
		try {
			con = connect();
			pstmt= con.prepareStatement(query);
			pstmt.setDate(1, new java.sql.Date(dt.getTime()));
			rs = pstmt.executeQuery();
			while(rs.next()) {
				CFMReportDO rep = new CFMReportDO();
				rep.setDate(rs.getDate("day"));
				rep.setUser(rs.getString("author"));
				rep.setActionId(rs.getInt("action_id"));
				rep.setStartTime(rs.getTimestamp("min"));
				rep.setEndTime(rs.getTimestamp("max"));
				rep.setCount(rs.getInt("count"));
				rep.setAvgProcessingTime(rs.getString("processingtime"));
				rep.setAvgWaitingTime(rs.getString("waitingTime"));
				ret.add(rep);
			}
		} catch (SQLException e) {
			e.printStackTrace();
//			logger.error("Can't get statistic for " + actionId +" / " + status,e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return ret;
	}
	 
}
