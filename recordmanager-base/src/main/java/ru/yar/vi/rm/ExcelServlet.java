package ru.yar.vi.rm;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ru.yar.vi.rm.user.form.PaymentForm;


public class ExcelServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6167927923129792960L;



	public void doGet(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
	throws ServletException, IOException
	{
		httpservletresponse.addHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
		httpservletresponse.addDateHeader("Last-Modified", new Date().getTime());
		httpservletresponse.addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
		httpservletresponse.addHeader("Cache-Control", "post-check=0, pre-check=0");
		httpservletresponse.addHeader("Pragma", "no-cache");
		httpservletresponse.addHeader("Content-Disposition", "attachment; filename=\"name_of_excel_file.xml\"");
		httpservletresponse.setContentType("application/vnd.ms-excel");
		HttpSession httpsession = httpservletrequest.getSession();
		PaymentForm form = (PaymentForm) httpsession.getAttribute("PaymentForm");
		if(form == null) {
			
		}
			
	}

}
