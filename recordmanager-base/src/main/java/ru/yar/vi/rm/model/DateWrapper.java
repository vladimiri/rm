package ru.yar.vi.rm.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import pro.deta.detatrak.CacheContainer;

public class DateWrapper {
	public static final Logger log = Logger.getLogger(DateWrapper.class);
	private static final SimpleDateFormat sdfRu = new SimpleDateFormat("dd/MM/yyyy");
	private static final SimpleDateFormat sdfRu1 = new SimpleDateFormat("dd.MM.yyyy");
    private Date date;


    public DateWrapper(Object date) {
        if (date == null) {
            this.date = null;
            return;
        }
        if (date instanceof Long) {
            this.date = new Date((Long) date);
        } else if (date instanceof Date) {
        	this.date = (Date) date;
        } else if (date instanceof String) {
        	String value = (String) date;
        	try {
        		this.date = sdfRu1.parse(value);
        		return;
        	} catch (ParseException e) {
        		log.error("Can't convert specified value ["+date+"] to a Date", e);
        	}

        	try {
        		this.date = sdfRu.parse(value);
        		return;
        	} catch (ParseException e) {
        		log.error("Can't convert specified value ["+date+"] to a Date");
        	}

        	try {
        		SimpleDateFormat sdf = new SimpleDateFormat(CacheContainer.getInstance().getConfig("formatDate"));
        		this.date = sdf.parse(value);
        	} catch (ParseException e) {
        		log.error("Can't convert specified value ["+date+"] to a Date", e);
        	}
        	
        	if(this.date == null)
                throw new IllegalArgumentException("Can't convert string: " + value +" to Date.");
        } else {
            throw new IllegalArgumentException("Not supported number class: " + date.getClass().getCanonicalName());
        }
    }

    public Date getDate() {
        return date;
    }
    
    public java.sql.Date getSqlDate() {
    	if(date != null)
    		return new java.sql.Date(date.getTime());
    	else 
    		return null;
    }

	public static Date getDate(String string) {
    	try {
    		return sdfRu1.parse(string);
    	} catch (ParseException e) {
    		log.error("Can't convert specified value ["+string+"] to a Date", e);
    		return null;
    	}
	}
}
