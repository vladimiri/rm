package ru.yar.vi.rm.user.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class ReportForm extends ActionForm {
	private String action;
	private String smsTemplate = "habahaba";
	private String[] selectedIds = new String[] {};
	private Map<Integer,ActionDO> actions = new HashMap<Integer, ActionDO>();
	private List<RegionDO> regions = null;
	
	private List<BaseDO> objects = null;
	private List<BaseDO> days = null;
	private List<StoredRecordDO> records = null;
	private static final long serialVersionUID = -5455906165515967152L;
	private int objectId;
	private int dayId;
	
	public List<BaseDO> getDays() {
		return days;
	}
	public void setDays(List<BaseDO> days) {
		this.days = days;
	}

	public int getDayId() {
		return dayId;
	}
	public void setDayId(int dayId) {
		this.dayId = dayId;
	}
	public int getObjectId() {
		return objectId;
	}
	public void setObjectId(int objectId) {
		this.objectId = objectId;
	}
	public List<StoredRecordDO> getRecords() {
		return records;
	}
	public void setRecords(List<StoredRecordDO> records) {
		this.records = records;
	}
	public List<BaseDO> getObjects() {
		return objects;
	}
	public void setObjects(List<BaseDO> objects) {
		this.objects = objects;
	}
	public List<RegionDO> getRegions() {
		return regions;
	}
	public void setRegions(List<RegionDO> regions) {
		this.regions = regions;
	}
	public Map<Integer, ActionDO> getActions() {
		return actions;
	}
	public void setActions(Map<Integer, ActionDO> actions) {
		this.actions = actions;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getSmsTemplate() {
		return smsTemplate;
	}
	public void setSmsTemplate(String smsTemplate) {
		this.smsTemplate = smsTemplate;
	}
	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.action = null;
		this.objectId = 0;
	}
	public String[] getSelectedIds() {
		return selectedIds;
	}
	public void setSelectedIds(String[] selectedIds) {
		this.selectedIds = selectedIds;
	}
	
	public ActionDO getAction(int actionId) {
		ActionDO act = actions.get(actionId);
		if(act == null)
			act = new ActionDO(actionId,"Unknown action");
		return act;
	}
}
