package ru.yar.vi.rm;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

public class TeeServletOutputStream extends ServletOutputStream {
	private ServletOutputStream os;
	private OutputStream targetStream;

	public TeeServletOutputStream(ServletOutputStream one, OutputStream two) {
		os = one;
		targetStream = two;
	}

	@Override
	public void write(int arg0) throws IOException {
		os.write(arg0);
		targetStream.write(arg0);
	}

	public void flush() throws IOException {
		os.flush();
		targetStream.flush();
	}

	public void close() throws IOException {
		os.close();
		this.targetStream.close();
	}

	@Override
	public boolean isReady() {
		return os.isReady();
	}

	@Override
	public void setWriteListener(WriteListener arg0) {
		os.setWriteListener(arg0);
	}

}