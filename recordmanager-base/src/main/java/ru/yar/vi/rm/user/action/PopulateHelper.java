package ru.yar.vi.rm.user.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.CacheContainer;
import pro.deta.detatrak.validator.CustomValidator;
import pro.deta.detatrak.validator.ValidationResult;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.DateKeyFormatConverter;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.DateBaseDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RecordDO;
import ru.yar.vi.rm.data.ValidatorDO;
import ru.yar.vi.rm.exception.NoLinesAvailableException;
import ru.yar.vi.rm.model.FreeSpaceModel;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.UserForm;

public class PopulateHelper {
	private static final Logger logger = Logger.getLogger(PopulateHelper.class);

	public void refreshFreeDays(HttpServletRequest request, UserForm uf, ActionMessages ams) {
		HDAO hdao = new HDAO(request);
		ActionDO action = (ActionDO) hdao.getById(ActionDO.class, uf.getActionId());
		OfficeDO office = (OfficeDO) hdao.getById(OfficeDO.class, uf.getOfficeId());
		
		List<RecordDO> outList = new ArrayList<RecordDO>();
		boolean success = false;
		for (RecordDO rec : uf.getRecordList()) {
			rec.setNoObjects(false);
			try {
				populateDays(uf,rec,office,action);
				success = true;
				scheduleFilter(request,uf,action,rec,ams);
			} catch (NoLinesAvailableException e) {
				rec.setNoObjects(true);
			}
			outList.add(rec);
		}
		if(success)
			for (RecordDO recordDO : outList) {
				recordDO.setNoObjects(true);
			}
		
		if(!outList.isEmpty())
			uf.setRecordList(outList);
	}

	private void scheduleFilter(HttpServletRequest request, UserForm uf, ActionDO action,RecordDO rec, ActionMessages ams) {
		ValidationResult validationResult = new ValidationResult(false,ams,action);
		for (ValidatorDO validator : action.getValidator()) {
			try {
				if(!StringUtils.isEmpty(validator.getClazz())) {
					Class cls = Class.forName(validator.getClazz());
					if(CustomValidator.class.isAssignableFrom(cls)) {
						CustomValidator valid = (CustomValidator) cls.newInstance();
						valid.filterSchedule(uf,request, validator,rec,validationResult);
					} else {
						logger.debug("Class " + validator.getClazz() +" is not an subclass of ScheduleFilter");
					}
				}
			} catch (Exception e) {
				logger.error("Can't find for action " + action +" custom validator."+ e.getMessage(),e);
				validationResult.setBlock();
				validationResult.getMessages().add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("validation.uncaught_exception"));
			}
		}
	}
	
	
	public static void populateDays(UserForm uf,RecordDO rec,OfficeDO office, ActionDO action) throws NoLinesAvailableException {
		List<DateBaseDO> days = new ArrayList<DateBaseDO>();

		boolean isPossibleToRecordCurrentDay = Boolean.valueOf(CacheContainer.getInstance().getConfig(Constants.CURRENT_DAY_RECORD));
		int daysToRegister = uf.getSelectedAction().getQueueLength();
		//		int daysToRegisterCheckFree = Integer.valueOf(CacheContainer.getInstance().getConfig(Constants.DAYS_TO_REGISTER_CHECK_FREE));
		
		Calendar cal = UserHelper.getCurrentCalendar();
		if(uf.isCurrentDay()) {
			isPossibleToRecordCurrentDay = true;
			daysToRegister = 1;
			cal.add(Calendar.HOUR_OF_DAY, 1); // should be removed
		}

		FreeSpaceModel model = uf.getModel(rec.getObjectType(), cal.getTime());
		
		if(!isPossibleToRecordCurrentDay) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
		} else {
			model.setCurrentDay(true);
		}

		for(int i=0;i<daysToRegister;i++) {
			java.util.Date day = cal.getTime();
			DateBaseDO obj = new DateBaseDO();
			obj.setName(UserHelper.formatDate(day));
			obj.setId(new NumberWrapper(DateKeyFormatConverter.getFormat().format(day)).getInteger());
			obj.setDate(day);
			List<FreeSpaceDO> fsList = getFreeSpaceList(uf, model, day,office);
			if(fsList != null && fsList.size() > 0) {
				// день, в котором есть записи или же он совпадает с предыдущим выбранным
				obj.setFsList(fsList);
				days.add(obj);
				if(rec.getDay() == 0)
					rec.setDay(obj.getId());
			}
			cal.add(Calendar.DAY_OF_MONTH, 1);
			model.setCurrentDay(false);
		}
		rec.getOfficeDays().put((long)uf.getOfficeId(), days);
	}

	private static List<FreeSpaceDO> getFreeSpaceList(UserForm uf, FreeSpaceModel model,
			java.util.Date day,OfficeDO office) throws NoLinesAvailableException {
		return model.getFreeSpaceList(uf.getSelectedAction(),uf.getCriteria(),day,uf.getCustomerId(),office);
	}
}
