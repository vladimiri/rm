package ru.yar.vi.rm.user.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.DisplayRecordDO;



public class DisplayForm extends ActionForm {
	private List<DisplayRecordDO> objectRecord = new ArrayList<DisplayRecordDO>();
	private List<String> pictures = null;
	private static final long serialVersionUID = -5455906165515967152L;
	private int random;
	private Date date;
	private String objectId;
	private boolean horizontal = false;
	private boolean showNo = false;
	private boolean toUppercase;
	private int limit=0;
	private int limitName=0;
	private int hour = 0;
	private int min=0;
	private int mon=0;
	private int day=0;
	private int year=0;
		
	
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getLimitName() {
		return limitName;
	}

	public void setLimitName(int limitName) {
		this.limitName = limitName;
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public boolean isShowNo() {
		return showNo;
	}

	public void setShowNo(boolean showNo) {
		this.showNo = showNo;
	}

	public boolean isEmpty() {
		for (DisplayRecordDO object : objectRecord) {
			if(object.getRecords() != null && !object.getRecords().isEmpty())
				return false;
		}
		return true;
	}

	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getRandom() {
		return random;
	}
	public void setRandom(int random) {
		this.random = random;
	}
	public List<DisplayRecordDO> getObjectRecord() {
		return objectRecord;
	}
	public void setObjectRecord(List<DisplayRecordDO> objectRecord) {
		this.objectRecord = objectRecord;
	}
	public List<String> getPictures() {
		return pictures;
	}
	public void setPictures(List<String> pictures) {
		this.pictures = pictures;
	}

	public int getMon() {
		return mon;
	}

	public void setMon(int mon) {
		this.mon = mon;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public boolean isToUppercase() {
		return toUppercase;
	}

	public void setToUppercase(boolean toUppercase) {
		this.toUppercase = toUppercase;
	}
}
