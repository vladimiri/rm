package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ModuleConfig;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.user.form.UserForm;



public class PreStep1Action extends Action {
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;

		int siteId = UserHelper.getCurrentSiteId(request);
		uf.fillSiteId(siteId);
		ModuleConfig mc = (ModuleConfig) request.getAttribute("org.apache.struts.action.MODULE");
		uf.setModule(mc.getPrefix());
		populateForm(uf,siteId);
		if(uf.isCurrentDay()) {
			if (mapping.findForward("current") != null)
				return mapping.findForward("current");
			uf.setCurrentDay(false);
		}
		return mapping.findForward("success");
	}

	protected void populateForm(UserForm uf,int siteId) {
		DictDAO dao = new DictDAO();
		try {
			uf.setCustomers(dao.getCustomers(siteId));
			uf.setRegions(dao.getRegions(siteId));
			uf.setActions(dao.getActions(siteId,uf.getSecurity()));
		} finally {
			dao.disconnect();
		}
		uf.setRecordList(null);
	}
}
