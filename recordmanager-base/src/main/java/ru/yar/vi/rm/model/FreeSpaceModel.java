package ru.yar.vi.rm.model;

import java.util.Date;
import java.util.List;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.exception.NoLinesAvailableException;

public interface FreeSpaceModel {
	
	public List<FreeSpaceDO> getFreeSpaceList(ActionDO actionId,int criteriaId,Date day,int customerId,OfficeDO office) throws NoLinesAvailableException;
	public void setCurrentDay(boolean current);
}
