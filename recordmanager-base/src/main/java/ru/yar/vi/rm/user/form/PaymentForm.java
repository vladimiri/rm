package ru.yar.vi.rm.user.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PaymentParamDO;

public class PaymentForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9137474935169226133L;

	private List<PaymentParamDO> paymentParam = new ArrayList<PaymentParamDO>();
	private List<OfficeDO> offices = new ArrayList<OfficeDO>();
	private List<BaseDO> actions = new ArrayList<BaseDO>();
	private int paymentId = 0;
	private int officeId = 0;
	private int actionId;
	
	private String bik;
	private String account;
	private String bank;
	private String receiverAccount;
	private String receiverName;
	private String inn;
	private String kpp;
	private String personalAcc;
	private String type;
	private String kbk;
	private String base;
	private String okato;
	private String period;
	private String payer;
	private String address;
	private String payerInn;
	private String status;
	private String paymentAccount;
	private String cardno;
	private Double amount;
	private String reason;
	private String cccode;
	

	public String getBik() {
		return bik;
	}
	public void setBik(String bik) {
		this.bik = bik;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getReceiverAccount() {
		return receiverAccount;
	}
	public void setReceiverAccount(String receiverAccount) {
		this.receiverAccount = receiverAccount;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	public String getInn() {
		return inn;
	}
	public void setInn(String inn) {
		this.inn = inn;
	}
	public String getKpp() {
		return kpp;
	}
	public void setKpp(String kpp) {
		this.kpp = kpp;
	}
	public String getPersonalAcc() {
		return personalAcc;
	}
	public void setPersonalAcc(String personalAcc) {
		this.personalAcc = personalAcc;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getKbk() {
		return kbk;
	}
	public void setKbk(String kbk) {
		this.kbk = kbk;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getOkato() {
		return okato;
	}
	public void setOkato(String okato) {
		this.okato = okato;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.payer = payer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPayerInn() {
		return payerInn;
	}
	public void setPayerInn(String payerInn) {
		this.payerInn = payerInn;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaymentAccount() {
		return paymentAccount;
	}
	public void setPaymentAccount(String paymentAccount) {
		this.paymentAccount = paymentAccount;
	}
	public String getCardno() {
		return cardno;
	}
	public void setCardno(String cardno) {
		this.cardno = cardno;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCccode() {
		return cccode;
	}
	public void setCccode(String cccode) {
		this.cccode = cccode;
	}

	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public List<OfficeDO> getOffices() {
		return offices;
	}
	public void setOffices(List<OfficeDO> offices) {
		this.offices = offices;
	}
	public int getOfficeId() {
		return officeId;
	}
	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}
	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public List<BaseDO> getActions() {
		return actions;
	}
	public void setActions(List<BaseDO> actions) {
		this.actions = actions;
	}
	public List<PaymentParamDO> getPaymentParam() {
		return paymentParam;
	}
	public void setPaymentParam(List<PaymentParamDO> paymentParam) {
		this.paymentParam = paymentParam;
	}
	
}
