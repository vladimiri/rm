package ru.yar.vi.rm.user.form;

import org.apache.struts.action.ActionForm;

public class EmptyForm extends ActionForm {
	/**
	 * Конфигурационные параметры для записи текущего дня, передаются в URL при инициализации.
	 */
	private static final long serialVersionUID = -5455906165515967152L;
	
	private int officeId = 0;
	private int customerId = 0;
	private int actionId = 0;
	private int criteriaId = 0;
	private int regionId = 0;
	private String forward;

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public int getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(int criteriaId) {
		this.criteriaId = criteriaId;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

}
