package ru.yar.vi.rm.user.form;

import ru.yar.vi.rm.data.Security;



public class SecuredUserForm extends UserForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6370078886293332724L;
	
	public SecuredUserForm() {
		this.security = Security.OPER;
	}
}
