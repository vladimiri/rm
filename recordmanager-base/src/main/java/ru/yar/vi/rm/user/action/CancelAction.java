package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.config.ModuleConfig;

import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.user.form.CancelForm;



public class CancelAction extends Action {
	public static final Logger logger = Logger.getLogger(CancelAction.class);
	
	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		CancelForm cf = (CancelForm) form;
		
		ModuleConfig mc = (ModuleConfig) request.getAttribute("org.apache.struts.action.MODULE");
		cf.setModule(mc.getPrefix());
		
		if(cf.getKey() != null) {
			String author = ""; 
			if(request.getUserPrincipal() != null)
				author = request.getUserPrincipal().getName();
			author += ";"+request.getRemoteHost();
			RecordDAO dao = null;
			try {
				dao = new RecordDAO(request);
				dao.beginTransaction();
				StoredRecordDO ret = dao.cancelRecord(cf.getKey(),author,request.getRemoteHost());
				if(ret == null) {
					throw new Exception("Record can't be cancelled by key:"+cf.getKey() +" author: " + author+" from host:" + request.getRemoteHost());
				}
				dao.commit();
				if(ret != null)
					logger.error("---DELETED--- record by " + author + "\n" + UserHelper.debugHeaderNames(request) +" \nRecord: " + ret +"\n---ENDDELETE---\n");
				cf.setDeleted(ret);
			} catch (Exception e) {
				if(dao != null)
					dao.rollback();
				logger.error("Error while cancelling", e);
		        ActionMessages ams = new ActionMessages();
		        ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("message.cancel.failure"));
		        saveErrors(request, ams);
		        return mapping.getInputForward();
			}
		}
		return mapping.findForward("success");
	}
	
	public ActionForward getInputCaptchaError(HttpServletRequest request,ActionMapping mapping) {
        ActionMessages ams = new ActionMessages();
        ams.add(ActionMessages.GLOBAL_MESSAGE, new ActionMessage("FAILURE_CAPTCHA"));
        saveErrors(request, ams);
        return mapping.getInputForward();
	}
}
