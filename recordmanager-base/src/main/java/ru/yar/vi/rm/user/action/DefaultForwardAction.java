package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.user.form.EmptyForm;

public class DefaultForwardAction extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		EmptyForm f = (EmptyForm) form;
		if(StringUtils.isEmpty(f.getForward()))
			return mapping.findForward("default");
		else
			return mapping.findForward(f.getForward());
	}
}
