package ru.yar.vi.rm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.ConfigDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.OperatorDO;
import ru.yar.vi.rm.data.PaymentParamDO;
import ru.yar.vi.rm.data.Security;



public class DictDAO extends DAO {
	private static final String ID = "id";
	public static final Logger logger = Logger.getLogger(DictDAO.class);
	
	public DictDAO() {
		connect();
	}

	public List<PaymentParamDO> getPaymentParam(int actionId) {
		List<PaymentParamDO> result = new ArrayList<PaymentParamDO>();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt= con.prepareStatement("select * from payment_param where action_id = ? order by id");
			pstmt.setInt(1, actionId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				PaymentParamDO data = new PaymentParamDO();
				fillPaymentParam(rs, data);
				result.add(data);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve object_num list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return result;
	}

	private void fillPaymentParam(ResultSet rs, PaymentParamDO data)
			throws SQLException {
		data.setId(rs.getInt(ID));
		data.setActionId(rs.getInt("action_id"));
		data.setName(rs.getString("name"));
		data.setBik(rs.getString("bik"));
		data.setAccount(rs.getString("account"));
		data.setBank(rs.getString("bank"));
		data.setReceiverAccount(rs.getString("receiver_account"));
		data.setReceiverName(rs.getString("receiver_name"));
		data.setInn(rs.getString("inn"));
		data.setKpp(rs.getString("kpp"));
		data.setKbk(rs.getString("kbk"));
		data.setPurpose(rs.getString("purpose"));
		data.setAmount(rs.getDouble("amount"));
	}
	
	public int getNumByObject(int objectId) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select * from object_num where object_id = ?");
			pstmt.setInt(1, objectId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return rs.getInt("num");
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve object_num list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return 0;
	}

	
	public OperatorDO getOperator(int code) {
		OperatorDO op = new OperatorDO();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select * from staff where code = ?");
			pstmt.setInt(1, code);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				op.setCode(code);
				op.setLevel(rs.getString("level"));
				op.setPosition(rs.getString("position"));
				op.setName(rs.getString("name"));
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve action list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return op;
	}
	
	
	
	public List<BaseDO> getActions(int siteId,Security security) {
		List<BaseDO> list = new ArrayList<BaseDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select id,name from actions a,site_action sa where (security = ? or security = 0 ) and a.id = sa.action_id and sa.site_id = ? order by sorter");
			pstmt.setInt(1, security.getValue());
			pstmt.setInt(2, siteId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BaseDO a = new BaseDO();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve action list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}

	public List<BaseDO> getCustomers(int siteId) {
		List<BaseDO> list = new ArrayList<BaseDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select id,name from customers a,site_customer sc where sc.site_id = ? and sc.customer_id = a.id order by sorter");
			pstmt.setInt(1, siteId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BaseDO a = new BaseDO();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve customer list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}

	
	public List<ObjectTypeDO> getObjectTypes() {
		List<ObjectTypeDO> list = new ArrayList<ObjectTypeDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select id,name,type from object_type order by id");
			rs = pstmt.executeQuery();
			while(rs.next()) {
				ObjectTypeDO a = populateObjectType(rs);
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve object type list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	public ObjectTypeDO getObjectType(int id) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt= con.prepareStatement("select id,name,type from object_type where id = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				return populateObjectType(rs);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve object type list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}

	private ObjectTypeDO populateObjectType(ResultSet rs) throws SQLException {
		ObjectTypeDO a = new ObjectTypeDO();
		a.setId(rs.getInt(1));
		a.setName(rs.getString(2));
		a.setType(rs.getString(3));
		return a;
	}
	
	public List<BaseDO> getRegions(int siteId) {
		List<BaseDO> list = new ArrayList<BaseDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select id,name from regions r, site_region sr where r.id = sr.region_id and sr.site_id = ? order by sorter");
			pstmt.setInt(1, siteId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BaseDO a = new BaseDO();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	/**
	@Deprecated
	public String getHomeOfficeSchedule(int regionId,int officeId) {
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("select regions,office_id from region_office where regions like '%,'||?||',%' and office_id = ? ");
			pstmt.setString(1, ""+regionId);
			pstmt.setInt(2, officeId);
			rs = pstmt.executeQuery();
			if(rs.next()) 
				return null;
			close(rs);
			close(pstmt);
			pstmt = con.prepareStatement("select schedule from office where id = (select office_id from region_office where regions like '%,'||?||',%') ");
			pstmt.setString(1, ""+regionId);
			rs = pstmt.executeQuery();
			if(rs.next()) 
				return rs.getString(1);
			
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}
	*/

	public List<BaseDO> getCriterias(int actionId,int security) {
		List<BaseDO> list = new ArrayList<BaseDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select c.id,c.name from action_criteria ac, criteria c where ac.action_id = ? and ac.criteria_id = c.id and (security = 0 or security = ?) order by id");
			pstmt.setInt(1, actionId);
			pstmt.setInt(2, security);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BaseDO a = new BaseDO();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve criteria list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	

	


	
	public List<OfficeDO> getOffices(int security) {
		List<OfficeDO> list = new ArrayList<OfficeDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select id,name,schedule,security,time_diff,okato from office where (security = ? or security = 0 ) order by id");
			pstmt.setInt(1, security);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				OfficeDO a = new OfficeDO();
				fillOfficeDO(rs, a);
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}


	private void fillOfficeDO(ResultSet rs, OfficeDO a) throws SQLException {
		a.setId(rs.getInt(ID));
		a.setName(rs.getString("name"));
		a.setSchedule(rs.getString("schedule"));
		a.setSecurity(rs.getInt("security"));
		a.setTimeDiff(rs.getInt("time_diff"));
		a.setOkato(rs.getString("okato"));
	}
	
	public OfficeDO getOffice(int id) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select id,name,schedule,security,time_diff,okato from office where id = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				OfficeDO a = new OfficeDO();
				fillOfficeDO(rs, a);
				return a;
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}

	
	
	public BaseDO getConfig(int id) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select * from config where id = ?");
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				ConfigDO a = new ConfigDO();
				a.setId(rs.getInt(ID));
				a.setName(rs.getString("name"));
				a.setValue(rs.getString("value"));
				return a;
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve config list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return null;
	}
//
//	public List<OfficeDO> getOffices(Security security,int region, int customer,int action) {
//		List<OfficeDO> list = new ArrayList<OfficeDO>();
//		ResultSet rs = null;
//		PreparedStatement pstmt = null;
//		try {
//			pstmt = con.prepareStatement("select office.id,office.name,schedule,office.security,office.time_diff,office.okato,max(region_office.default_office_id) as default_office_id "+
//"from office,region_office where (region_office.regions like '%,'||?||',%' or region_office.regions is null or region_office.regions = '' or region_office.regions = ',')"+
//"and (region_office.office_id = office.id or region_office.office_id = 0)" +
//"and (region_office.security = ? or region_office.security = 0 ) " +
//"and (region_office.actions like '%,'||?||',%' or region_office.actions is null or  region_office.actions = '' or  region_office.actions = ',' ) " +
//"and (region_office.customer_id = ? or region_office.customer_id = 0 ) " +
//" group by office.id,office.name,schedule,office.security,office.time_diff,office.okato " +
//"order by id");
//			pstmt.setString(1, ""+region);
//			pstmt.setInt(2, security.getValue());
//			pstmt.setString(3, ""+ action);
//			pstmt.setInt(4, customer);
//			rs = pstmt.executeQuery();
//			while(rs.next()) {
//				OfficeDO a = new OfficeDO();
//				fillOfficeDO(rs, a);
//	            a.setDefaultOfficeId(rs.getInt("default_office_id"));
//				list.add(a);
//			}
//		} catch (SQLException e) {
//			logger.error("Can't retrieve offices list",e);
//		} finally {
//			close(rs);
//			close(pstmt);
//		}
//		return list;
//	}

	
	
		public String[] stringToArr(String str ) {
		if(str == null)
			return new String[0];
		String vList[] = str.split(",");
		return vList;
	}
	
	public List<BaseDO> getObjectsOffice() {
		List<BaseDO> list = new ArrayList<BaseDO>();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select object.id,object.name || ' ' || office.name from object,office where office_id = office.id order by office.id,object.id");
			rs = pstmt.executeQuery();
			while(rs.next()) {
				BaseDO a = new BaseDO();
				a.setId(rs.getInt(1));
				a.setName(rs.getString(2));
				list.add(a);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve objects in office list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return list;
	}
	
	
	public String getObjectName(int objectId) {
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			pstmt = con.prepareStatement("select name from object where id = ?");
			pstmt.setInt(1, objectId);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return rs.getString(1);
			}
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(rs);
			close(pstmt);
		}
		return "OBJECT_NOT_FOUND";
	}
	
		

	
	public int updateObjectType(ObjectTypeDO object) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			if(object.getId() > 0)
				pstmt = con.prepareStatement("update object_type set name = ?, type=? where id = ?");
			else
				pstmt = con.prepareStatement("insert into object_type (id,name,type) values (nextval('object_seq'),?,?)");
			pstmt.setString(1, object.getName());
			pstmt.setString(2, object.getType());
			if(object.getId() > 0)
				pstmt.setInt(3, object.getId());
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Can't retrieve object list",e);
		} finally {
			close(pstmt);
		}
		return 0;
	}
	
	public int updateOffice(OfficeDO office) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = connect();
			if(office.getId() > 0)
				pstmt = con.prepareStatement("update office set name = ?, schedule=?,security=?,okato = ? where id = ?");
			else
				pstmt = con.prepareStatement("insert into office (id,name,schedule,security,okato) values (nextval('office_seq'),?,?,?,?)");
			pstmt.setString(1, office.getName());
			pstmt.setString(2, office.getSchedule());
			pstmt.setInt(3, office.getSecurity());
			pstmt.setString(4, office.getOkato());
			if(office.getId() > 0)
				pstmt.setInt(5, office.getId());
			return pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Can't retrieve region list",e);
		} finally {
			close(pstmt);
		}
		return 0;
	}
	
	

	
	
		



}
