package ru.yar.vi.rm.user.action;

import java.util.Map;

public interface Mappable {
	public Map<String, String> getMap() throws Exception;
}
