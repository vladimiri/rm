package ru.yar.vi.rm.dao;

import java.util.List;

import org.hibernate.QueryException;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.BooleanType;
import org.hibernate.type.Type;

class PostgreSQLFullTextSearchFunction implements SQLFunction {

	@SuppressWarnings("unchecked")
	public String render(Type paramType, List args,
			SessionFactoryImplementor paramSessionFactoryImplementor)
			throws QueryException {
		if (args.size() != 3) {
			throw new IllegalArgumentException(
					"The function must be passed 2 arguments");
		}

		String ftsConfig = (String) args.get(0);
		String field = (String) args.get(1);
		String value = (String) args.get(2);

		String fragment = null;
		if (ftsConfig == null) {
			fragment = field + " @@  plainto_tsquery(" + value + ")";
		} else {
			fragment = field + " @@  plainto_tsquery(" + ftsConfig + ", "
					+ value + ")";
		}

		// fragment = "" + field + " @@ " + "to_tsquery("+ value + ")";
		return fragment;

	}

	public Type getReturnType(Type columnType, Mapping mapping)
			throws QueryException {
		return new BooleanType();
	}

	public boolean hasArguments() {
		return true;
	}

	public boolean hasParenthesesIfNoArguments() {
		return false;
	}

}