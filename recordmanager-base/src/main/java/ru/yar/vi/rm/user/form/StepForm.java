package ru.yar.vi.rm.user.form;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public abstract class StepForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1478914678453721801L;
	private int step;
	private int totalSteps;
	private LinkedList<Integer> prevStep = new LinkedList<Integer>();
	private String next;
	private String prev;
	private String ready;
	private boolean alreadyValidated = false;

	public LinkedList<Integer> getPrevStep() {
		return prevStep;
	}

	public void setPrevStep(LinkedList<Integer> prevStep) {
		this.prevStep = prevStep;
	}
	
	public String getReady() {
		return ready;
	}

	public void setReady(String ready) {
		this.ready = ready;
	}

	public StepForm(int i) {
		totalSteps = i;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	@Override
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.prev = null;
		this.next = null;
		this.ready = null;
		this.alreadyValidated = false;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public void reset(int total) {
		step=0;
		totalSteps = total;
	}

	abstract public int validateStep(ActionMapping mapping, HttpServletRequest request, ActionErrors ae,int step);
	
	public final ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		return super.validate(mapping, request);
	}
	
	public final int nextStep() {
		return step+1;
	}
	
	public final int currentStep() {
		return step;
	}

	public int firstStep() {
		return 0;
	}

	
	public int finalStep() {
		return -2;
	}


}
