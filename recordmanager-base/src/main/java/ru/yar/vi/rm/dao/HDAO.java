package ru.yar.vi.rm.dao;

import java.io.Serializable;
import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.exception.SQLGrammarException;

import pro.deta.detatrak.JPAFilter;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.SiteDO;
import ru.yar.vi.rm.data.StoredRecordDO;
import ru.yar.vi.rm.model.NumberWrapper;

public class HDAO {
	public static final Logger logger = Logger.getLogger(HDAO.class);
	protected EntityManager session;
	protected EntityTransaction transaction;

	public HDAO(ServletRequest req) {
		session = (EntityManager) req.getAttribute(Constants.ENTITY_MANAGER_ATTRIBUTE);
	}

	public HDAO(EntityManager req) {
		session = req;
	}

	public HDAO(boolean openNew) {
		session = JPAFilter.createEntityManager();
		beginTransaction();
	}

	public void update(Object o) {
		if (transaction == null)
			beginTransaction();
		session.persist(o);
	}

	public void beginTransaction() {
		transaction = session.getTransaction();
		if (!transaction.isActive())
			transaction.begin();
	}

	public void commit() {
		if (transaction != null) {
			try {
				if (transaction.isActive())
					transaction.commit();
				else {
					try {
						throw new Exception("Transaction is not active and can't be committed");
					} catch (Exception e) {
						logger.error("Transaction is not active", e);
					}
				}
			} catch (Exception e) {
				if (e instanceof SQLGrammarException) {
					SQLGrammarException sg = (SQLGrammarException) e;
					SQLException sge = sg.getSQLException();
					if (sge instanceof BatchUpdateException) {
						BatchUpdateException bue = (BatchUpdateException) sge;
						logger.error("UpdateException ", bue.getNextException());
					} else
						logger.error("SQLGrammarException", ((SQLGrammarException) e).getSQLException());
				}
				logger.error("Error while calling commit.", e);
				throw new RuntimeException();
			}
		}
	}

	public void rollback() {
		if (transaction != null) {
			try {
				if (!transaction.isActive())
					transaction.rollback();
				else {
					try {
						throw new Exception("Transaction is not active and can't be rolledback");
					} catch (Exception e) {
						logger.error("Transaction is not active", e);
					}
				}
			} catch (Exception e) {
				if (e instanceof SQLGrammarException) {
					SQLGrammarException sg = (SQLGrammarException) e;
					SQLException sge = sg.getSQLException();
					if (sge instanceof BatchUpdateException) {
						BatchUpdateException bue = (BatchUpdateException) sge;
						logger.error("UpdateException ", bue.getNextException());
					} else
						logger.error("SQLGrammarException", ((SQLGrammarException) e).getSQLException());
				}
				logger.error("Error while calling rollback.", e);
				throw new RuntimeException();
			}
		}
	}

	public <T> T getById(Class<T> cl, String id) {
		Integer i = new NumberWrapper(id).getInteger();
		if (i != null)
			return session.find(cl, i);
		else
			return session.find(cl, id);
	}

	public <T> T getById(Class<T> cl, Integer id) {
		return session.find(cl, id);
	}

	public <T> T getById(Class<T> cl, Serializable id) {
		return session.find(cl, id);
	}

	public void delete(Class<?> cl, Serializable id) {
		Object o = session.find(cl, id);
		session.remove(o);
	}

	public <T> List<T> getAll(Class<T> cl) {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(cl);
		Root<T> rootEntry = cq.from(cl);
		CriteriaQuery<T> all = cq.select(rootEntry);
		TypedQuery<T> allQuery = session.createQuery(all);

		return allQuery.getResultList(); // @TODO add order by id
	}

	public List<ObjectDO> getOfficeObjects(int officeId) {
		Query q = session.createQuery("from ObjectDO where office.id = :officeId order by name,id");
		q.setParameter("officeId", officeId);
		return q.getResultList();
	}

	public EntityManager getSession() {
		return session;
	}

	public List<StoredRecordDO> getRecords(String query, String parameter, String parameterKey) throws Exception {
		Query q = session.createQuery(query, StoredRecordDO.class);
		q.setParameter("value", parameter);
		if (parameterKey != null)
			q.setParameter("param", parameterKey);
		return q.getResultList();
	}

	public List getQueryResult(String query, int parameter) {
		Query q = null;
		q = session.createQuery(query);
		q.setParameter("value", parameter);
		return q.getResultList();
	}

	public void disconnect() {
		try {
			if (transaction != null) {
				try {
					if (transaction.isActive())
						transaction.rollback();
				} catch (Exception e) {
					logger.error("Error while rollback transaction", e);
				}
				transaction = null;
			}
			if (session != null)
				session.close();
			session = null;
		} catch (HibernateException e) {
			logger.error("Error while closing hibernate session", e);
		}
	}

	public void evict(Object arg0) {
		session.detach(arg0);
	}

	public void clear() {
		session.clear();
	}

	public <T> T merge(T entity) {
		return session.merge(entity);
	}

	public <T> T find(Class<T> class1, Object pk) {
		return session.find(class1, pk);
	}

	public void persist(Object merge) {
		session.persist(merge);
	}

	public List<OfficeDO> getOffices(int regionId, int actionId) {
		Query q = session.createQuery(
				"select o from OfficeDO o left join o.serviceRegionList reg left join o.serviceActionList act "
						+ "where (reg.id = :regionId or reg is null) "
						+ "and (act.id  = :actionId or act is null) order by o.id");
		q.setParameter("regionId", regionId);
		q.setParameter("actionId", actionId);
		List<OfficeDO> result = q.getResultList();
		return result;
	}

	public List<OfficeDO> getSmartOfficeList(int regionId, int actionId, int siteId) {
		Query q = session.createNativeQuery("select distinct office_id from object o "
				+ "join (select object_id from schedule s left join schedule_action sa on sa.schedule_id = s.id "
				+ "where sa.action_id = :actionId or sa.action_id is null " + " union all "
				+ "select child_id from object_relation obr where parent_id in ( "
				+ "select object_id from schedule s left join schedule_action sa on sa.schedule_id = s.id "
				+ "where sa.action_id = :actionId or sa.action_id is null " + ")) so on so.object_id = o.id " + "where "
				+ "o.type_id in (select type_id from actions a join action_object_type_item aoti on aoti.action_id = a.id "
				+ "join object_type_item oti on oti.id = aoti.object_type_item_id " + "where a.id = :actionId)");
		q.setParameter("actionId", actionId);
		@SuppressWarnings("unchecked")
		List<Integer> result = q.getResultList();
		List<OfficeDO> officeList = new ArrayList<>();
		SiteDO site = session.find(SiteDO.class, siteId);
		if (site != null) {
			for (OfficeDO office : site.getOffices()) {
				result.forEach(officeId -> {
					if (officeId == office.getId()) {
						boolean serviceRegionAccepted = office.getServiceRegionList().isEmpty()
								|| office.getServiceRegionList().stream().filter(e -> e.getId() == regionId).findFirst()
										.isPresent();
						boolean serviceActionAccepted = office.getServiceActionList().isEmpty()
								|| office.getServiceActionList().stream().filter(e -> e.getId() == actionId).findFirst()
										.isPresent();

						if (serviceRegionAccepted && serviceActionAccepted)
							officeList.add(office);
					}

				});
			}
		}
		return officeList;
	}
}
