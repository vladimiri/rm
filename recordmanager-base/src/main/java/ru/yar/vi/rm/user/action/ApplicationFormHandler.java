package ru.yar.vi.rm.user.action;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.struts.action.DynaActionForm;

import ru.yar.vi.rm.user.form.ApplicationForm;

public class ApplicationFormHandler {
	BufferedReader br = null;
	private static final int BIK_LENGTH = 9;
	private static final String BIK = "BIK";
	private static final int ACCOUNT_LENGTH = 20;
	private static final String ACCOUNT = "ACC";
	private static final String BANK = "BANK";
	private static final int BANK_LENGTH = 45;
	private static final String RCVACC = "RCVACC";
	private static final String RCVNAME = "RCVNAME";
	private static final int RCVNAME_LENGTH = 160;
	private static final String INN = "INN";
	private static final int INN_LENGTH = 12;
	private static final int INN_LENGTH_SHORT = 10;
	private static final String KPP = "KPP";
	private static final int KPP_LENGTH = 9;
	private static final String PERSACC = "PERSACC";
	private static final int PERSACC_LENGTH = 16;
	private static final String TYPE = "TYPE";
	private static final int TYPE_LENGTH = 2;
	private static final String KBK = "KBK";
	private static final int KBK_LENGTH = 20;	
	private static final String BASE = "BASE";
	private static final int BASE_LENGTH = 2;
	
	private static final String OKATO = "OKATO";
	private static final int OKATO_LENGTH = 11;
	private static final String PERIOD = "PERIOD";
	private static final int PERIOD_LENGTH = 10;
	private static final String PAYER = "PAYER";
	private static final int PAYER_LENGTH = 78;
	private static final String ADDR = "ADDR";
	private static final int ADDR_LENGTH = 78;
	private static final String PAYERINN = "PAYERINN";
	private static final String STATUS = "STATUS";
	private static final int STATUS_LENGTH = 2;
	private static final String PYMACC = "PYMACC";
	private static final String CARDNO = "CARDNO";
	private static final int CARDNO_LENGTH = 16;
	
	private static final String AMOUNT = "AMOUNT";
	private static final int AMOUNT_LENGTH = 9;
	private static final String AMOUNTK = "AMOUNTK";
	private static final int AMOUNTK_LENGTH = 2;

	private static final String REASON = "REASON";
	private static final int REASON_LENGTH = 210;
	
	private static final String CCCODE = "CCCODE";
	private static final int CCCODE_LENGTH = 5;
	
	
	
	public ApplicationFormHandler(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
		br = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(fileName),"UTF-8"),140000);
	}
	
	public void handle(ApplicationForm pf,OutputStream os) throws Exception {
		Map<String,String> resultMap = getMap(pf);
		String strLine;
		OutputStreamWriter out = new OutputStreamWriter(os, "UTF-8");
		while ((strLine = br.readLine()) != null)   {
			strLine = replaceValues(strLine, resultMap);
			out.write(strLine+"\n");
		}
		out.flush();
	}
	final Pattern pattern =	Pattern.compile("\\$([\\w\\d\\_]*)", Pattern.DOTALL);
	
	public String replaceValues(final String template, final Map<String, String> values){
		final StringBuffer sb = new StringBuffer();
		final Matcher matcher = pattern.matcher(template);
		while(matcher.find()){
			final String key = matcher.group(1);
			String replacement = values.get(key);
			if(replacement == null || "null".equalsIgnoreCase(replacement) || "".equalsIgnoreCase(replacement)){
				replacement = "";
			}
			matcher.appendReplacement(sb, replacement);
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	private Map<String, String> getMap(ApplicationForm pf) throws Exception {
		return pf.getMap();
	}
	

	
	private Map<String, String> int2Map(int intValue, String amount2, int amountLength) throws Exception {
		return string2Map(String.format("%"+amountLength+"s", ""+intValue),amount2,amountLength,0 );
	}
	private Map<String, String> int2MapLead0(int intValue, String amount2, int amountLength) throws Exception {
		return string2Map(String.format("%0"+amountLength+"d", intValue),amount2,amountLength,0 );
	}

	private Map<String, String> string2Map(String source, String mapCode,int maxLen,int exactLen) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		if(source == null || "".equalsIgnoreCase(source))
			return map;
		if(exactLen > 0) {
			if(source.length() != exactLen) {
				throw new Exception("String " + source+" is not " + exactLen +" chars length.");
			}
		}
		char[] arr = source.toCharArray();
		int i=1;
		for (char c : arr) {
			if(i> maxLen)
				break;
			map.put(mapCode+i++, ""+c);
		}
		return map;
	}
}
