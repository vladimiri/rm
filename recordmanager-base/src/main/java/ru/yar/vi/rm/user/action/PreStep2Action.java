package ru.yar.vi.rm.user.action;

import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import pro.deta.detatrak.util.DETACaptcha;
import pro.deta.detatrak.util.SecurityUtil;
import pro.deta.security.SecurityElement;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.HDAO;
import ru.yar.vi.rm.dao.RecordDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.CustomFieldDO;
import ru.yar.vi.rm.data.CustomerDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.RegionDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.user.form.EmptyForm;
import ru.yar.vi.rm.user.form.UserForm;

public class PreStep2Action extends Action {
	public static final Logger logger = Logger.getLogger(PreStep2Action.class);

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		UserForm uf = (UserForm) form;
		int siteId = UserHelper.getCurrentSiteId(request);
		if(logger.isDebugEnabled())
			debugHeaderNames(request);

		RecordDAO dao = new RecordDAO(request);   
		populate(uf,dao,siteId);
		if(uf.getSelectedAction() == null) {
			logger.error("Error while looking for action " + uf.getActionId() +" in form: " + uf);
			ActionMessages ams = new ActionMessages();
			ams.add(ActionMessages.GLOBAL_MESSAGE,new ActionMessage("session.timeout"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}

		if(!uf.isAgreed() && uf.getSecurity() != Security.OPER && !uf.isCurrentDay()) {
			ActionMessages ams = new ActionMessages();
			ams.add("agreed",new ActionMessage("label.agreement.required"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}

		if(uf.getOffices() == null || uf.getOffices().size() == 0) {
			ActionMessages ams = new ActionMessages();
			ams.add("regionId",new ActionMessage("label.office.missed"));
			saveErrors(request, ams);
			return mapping.getInputForward();
		}

		EmptyForm eForm = (EmptyForm) request.getAttribute("EmptyForm");

		if(eForm != null) {
			if(eForm.getOfficeId() > 0) {
				for (OfficeDO office : uf.getOffices()) {
					if(office.getId() == eForm.getOfficeId()) {
						uf.getOffices().clear();
						uf.getOffices().add(office);
						break;
					}
				}
			}
			if(eForm.getActionId() > 0)
				uf.setActionId(eForm.getActionId());
			if(eForm.getCriteriaId() > 0)
				uf.setCriteria(eForm.getCriteriaId());
			if(eForm.getRegionId() > 0)
				uf.setRegionId(eForm.getRegionId());
			if(eForm.getCustomerId() > 0)
				uf.setCustomerId(eForm.getCustomerId());
		}

		ActionForward af = mapping.findForward("success.action." + uf.getActionId());
		if(af != null) {
			return af;
		}
		if(SecurityUtil.hasPermission((ServletRequest)request, SecurityElement.MAIN_MODULE, uf.getModule())) { 
			uf.setCaptcha(new DETACaptcha());
			uf.getCaptcha().init(request);
		}
		return mapping.findForward("success");
	}

	protected void populate(UserForm uf, RecordDAO dao2, int siteId) {
		DictDAO dao = new DictDAO();
		try {
			if(UserHelper.isEmpty(uf.getCustomers()))
				uf.setCustomers(dao.getCustomers(siteId));

			uf.setSelectedCustomer(dao2.getById(CustomerDO.class, uf.getCustomerId()));
			uf.setSelectedRegion(dao2.getById(RegionDO.class, uf.getRegionId()));
			uf.setSelectedAction(dao2.getById(ActionDO.class, uf.getActionId()));

			if(uf.getSelectedAction() != null) {
				List<CustomFieldDO> fields = uf.getFields();
				fields.clear();
				Collection<CustomFieldDO> fieldList = uf.getSelectedAction().getField();
				for (CustomFieldDO customFieldDO : fieldList) {
					if(customFieldDO.getCustomerId() == 0 || customFieldDO.getCustomerId() == uf.getCustomerId())
						fields.add(customFieldDO);
				}
			}
			List<OfficeDO> offices = getOfficeList(dao2, uf);
			uf.setOffices(offices);
			if(uf.getOfficeId() == 0 && offices != null && offices.size() > 0) {
				Integer defaultOffice = offices.get(0).getDefaultOfficeId();
				if(defaultOffice != null)
					uf.setOfficeId(defaultOffice);
			}
			if((!checkOfficeAvailable(uf.getOfficeId(),offices) || uf.getOfficeId() == 0) 
					&& offices != null && offices.size() > 0)
				uf.setOfficeId(offices.get(0).getId());

		} finally {
			dao.disconnect();
		}
	}

	private boolean checkOfficeAvailable(int officeId, List<OfficeDO> offices) {
		for (OfficeDO officeDO : offices) {
			if(officeDO.getId() == officeId)
				return true;
		}
		return false;
	}

	public List<OfficeDO> getOfficeList(HDAO dao, UserForm uf) {
		/*
		 * Для юрид лиц список офисов обслуживания офис определяется на основе региона регистрации.
		 * Также офис определяется для физ лиц, для записи на получение водительских удостоверений. 
		 */
		return dao.getSmartOfficeList(uf.getRegionId(),uf.getActionId(),uf.getSiteId());
	}

	@SuppressWarnings("rawtypes")
	private void debugHeaderNames(HttpServletRequest request) {
		Enumeration names = request.getHeaderNames();

		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Enumeration values = request.getHeaders(name); // support multiple values
			if (values != null) {
				while (values.hasMoreElements()) {
					String value = (String) values.nextElement();
					logger.debug(name + ": " + value);
				}
			}
		}
	}
}
