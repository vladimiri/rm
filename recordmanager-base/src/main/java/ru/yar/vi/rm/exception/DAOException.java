package ru.yar.vi.rm.exception;

public class DAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7657048833808888449L;
	

	public DAOException(String message) {
		super(message);
	}


}
