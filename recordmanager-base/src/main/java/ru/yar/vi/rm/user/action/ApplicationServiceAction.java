package ru.yar.vi.rm.user.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.dao.GenCodeDAO;
import ru.yar.vi.rm.data.GenCodeDO;
import ru.yar.vi.rm.model.NumberWrapper;
import ru.yar.vi.rm.user.form.ApplicationServiceForm;



public class ApplicationServiceAction extends Action {

	public ActionForward execute(
			ActionMapping mapping,
			ActionForm form,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		ApplicationServiceForm pf = (ApplicationServiceForm) form;
		GenCodeDAO dao = new GenCodeDAO(request);
		
		if(pf.isRefresh()) {
			GenCodeDO gc = (GenCodeDO) dao.getById(GenCodeDO.class, new NumberWrapper(pf.getAction()).getInteger());
			pf.setSubActionList(dao.getGenCode("SUB_ACTION_"+ gc.getName()));
		}
		return mapping.getInputForward();
	}

}
