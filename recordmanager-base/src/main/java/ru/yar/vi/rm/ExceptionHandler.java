package ru.yar.vi.rm;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.config.ExceptionConfig;


public class ExceptionHandler extends org.apache.struts.action.ExceptionHandler {
	private final Logger logger = Logger.getLogger(ExceptionHandler.class);
	
	@Override
	public ActionForward execute(Exception arg0, ExceptionConfig arg1,
			ActionMapping arg2, ActionForm arg3, HttpServletRequest arg4,
			HttpServletResponse arg5) throws ServletException {
		logger.error("Error while processing", arg0);
		arg4.setAttribute("exception", arg0);
		logger.error("Exception happened", arg0);
		ActionForward af = super.execute(arg0, arg1, arg2, arg3, arg4, arg5);
//		af = arg2.findForward("mapT");
		return af;//af.setModule("self")
	}
	

}
