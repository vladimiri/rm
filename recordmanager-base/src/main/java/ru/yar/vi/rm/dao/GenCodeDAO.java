package ru.yar.vi.rm.dao;

import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletRequest;

import ru.yar.vi.rm.data.GenCodeDO;

public class GenCodeDAO extends HDAO {

	public GenCodeDAO(ServletRequest req) {
		super(req);
	}
	
	public List<GenCodeDO> getGenCode(String genType) throws Exception {
		Query q = session.createQuery("from GenCodeDO where genType = :genType order by sorter,name,id");
		q.setParameter("genType", genType);
		@SuppressWarnings("unchecked")
		List<GenCodeDO> list = q.getResultList();
		return list;
	}

	public GenCodeDO getGenCode(String genType, String genName) {
		Query q = session.createQuery("from GenCodeDO where genType = :genType and name = :genName");
		q.setParameter("genType", genType);
		q.setParameter("genName", genName);
		return (GenCodeDO) q.getSingleResult();
	}
	
	public GenCodeDO getGenCodeById(int id) {
		Query q = session.createQuery("from GenCodeDO where id = :id ");
		q.setParameter("id", id);
		return (GenCodeDO) q.getSingleResult();
	}
}
