package ru.yar.vi.rm;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import ru.yar.vi.rm.dao.ConfigDAO;

public class CustomCharacterEncodingFilter implements Filter {


	public void init(FilterConfig config) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) 
			throws IOException, ServletException {
		request.setCharacterEncoding("windows-1251");
		response.setCharacterEncoding("windows-1251");
		chain.doFilter(request, response);
	}

	public void destroy() {
	}
}
