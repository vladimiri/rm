package ru.yar.vi.rm.user.form;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.BaseDO;
import ru.yar.vi.rm.data.ObjectDO;
import ru.yar.vi.rm.data.StoredRecordDO;



public class ListForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3970648054225625951L;
	private String name;

	private String action;
	private int day;
	private List<BaseDO> days;
	private List<StoredRecordDO> recordList;
	private List<ObjectDO> objects;
	private Map<Integer,ActionDO> actions = new HashMap<Integer, ActionDO>();
	private boolean onlyNew;


	public List<ObjectDO> getObjects() {
		return objects;
	}

	public void setObjects(List<ObjectDO> objects) {
		this.objects = objects;
	}

	public List<StoredRecordDO> getRecordList() {
		return recordList;
	}

	public void setRecordList(List<StoredRecordDO> recordList) {
		this.recordList = recordList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public List<BaseDO> getDays() {
		return days;
	}

	public void setDays(List<BaseDO> days) {
		this.days = days;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		action = "";
		onlyNew = false;
	}

	public boolean isOnlyNew() {
		return onlyNew;
	}

	public void setOnlyNew(boolean onlyNew) {
		this.onlyNew = onlyNew;
	}

	public Map<Integer, ActionDO> getActions() {
		return actions;
	}

	public void setActions(Map<Integer, ActionDO> actions) {
		this.actions = actions;
	}
}
