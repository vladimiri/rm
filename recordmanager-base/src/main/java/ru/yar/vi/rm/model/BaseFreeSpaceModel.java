package ru.yar.vi.rm.model;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import pro.deta.detatrak.CacheContainer;
import ru.yar.vi.rm.Constants;
import ru.yar.vi.rm.UserHelper;
import ru.yar.vi.rm.dao.DictDAO;
import ru.yar.vi.rm.dao.FreeSpaceDAO;
import ru.yar.vi.rm.data.ActionDO;
import ru.yar.vi.rm.data.FreeObjectDO;
import ru.yar.vi.rm.data.FreeSpaceDO;
import ru.yar.vi.rm.data.ObjectTypeDO;
import ru.yar.vi.rm.data.OfficeDO;
import ru.yar.vi.rm.data.PeriodDO;
import ru.yar.vi.rm.data.Security;
import ru.yar.vi.rm.exception.DAOException;
import ru.yar.vi.rm.exception.NoLinesAvailableException;


public class BaseFreeSpaceModel implements FreeSpaceModel {
	private Security security;
	public final Logger logger = Logger.getLogger(BaseFreeSpaceModel.class);
	private ObjectTypeDO objectType;
	protected FreeSpaceDAO dao = new FreeSpaceDAO();
	private boolean currentDay = false;
	private Date actualTimestamp;

	public BaseFreeSpaceModel(Security security,ObjectTypeDO objectType, Date currentTimestamp) {
		this.security = security;
		this.objectType = objectType;
		this.actualTimestamp = currentTimestamp;
	}

	public List<FreeObjectDO> getLinesAvailable(int lineType,int officeId,int customerId,int actionId){
		return dao.getLinesId(lineType,officeId,0);
	}

	public List<FreeSpaceDO> getFreeSpaceList(ActionDO actionId,Date day,OfficeDO office) throws NoLinesAvailableException {
		return getFreeSpaceList(actionId, 0, day, 0, office);
	}
	
	public List<FreeSpaceDO> getFreeSpaceList(ActionDO action,int criteriaId,Date day,int customerId,OfficeDO office) throws NoLinesAvailableException {
		int fraudInterval = getFraudInterval(action);
		DateFormat dateFormat = DateFormat.getDateInstance();
		List<FreeSpaceDO> suitableSpaces = new ArrayList<FreeSpaceDO>();
		int actionId = action.getId();
		CacheContainer cc = CacheContainer.getInstance();
		try {
			List<FreeSpaceDO> list = new ArrayList<FreeSpaceDO>();

			HashMap<String,String> objectTypeNames = new HashMap<String, String>();
			DictDAO dict = new DictDAO();
			try {
				dict.connect();
				List<ObjectTypeDO> types = dict.getObjectTypes();
				for (ObjectTypeDO objectTypeDO : types) {
					objectTypeNames.put(objectTypeDO.getType(), objectTypeDO.getName());
				}
			} finally {
				dict.disconnect();
			}

			List<FreeObjectDO> objectList = getLinesAvailable(objectType.getId(),office.getId(),customerId,actionId);
			if(objectList.isEmpty())
				throw new NoLinesAvailableException();
			List<FreeObjectDO> activeLineList = new ArrayList<FreeObjectDO>(); 
			for (FreeObjectDO freeObjectDO : objectList) {
				int duration = getDuration(freeObjectDO,day,actionId,criteriaId,objectType.getId(),customerId);
				freeObjectDO.setDuration(duration);
				if(duration > 0)
					activeLineList.add(freeObjectDO);
				else
					logger.debug("Not found duration for " + dateFormat.format(day) + " action/criteriaId/object/objectType: " + actionId+"/"+criteriaId+"/"+freeObjectDO +"/"+objectType);
			}
			objectList.clear();
			objectList = null;

			Calendar cal = UserHelper.getCurrentCalendar();
			cal.setTime(day);

			List<PeriodDO> homeOfficeScheduleList = null;

			if(office.getSchedule() != null && !"".equalsIgnoreCase(office.getSchedule()))
				homeOfficeScheduleList = UserHelper.parsePeriod(office.getSchedule(), cal);

			List<FreeObjectDO> periodLineList = new ArrayList<FreeObjectDO>();
			for (FreeObjectDO freeLineDO : activeLineList) {
				String period = getSchedule(freeLineDO,day,actionId,criteriaId,customerId);
				if(period != null) {
					List<PeriodDO> periodList = UserHelper.parsePeriod(period, cal);

					if(office.getOverlapPeriods() != null && office.getOverlapPeriods() && 
							homeOfficeScheduleList != null && homeOfficeScheduleList.size() > 0) {
						periodList = UserHelper.overlapPeriods(homeOfficeScheduleList, periodList);
					}
					periodList = filterWeekendPeriod(freeLineDO,day,cal,periodList);

					if(periodList != null && periodList.size() > 0) {
						freeLineDO.getPeriods().addAll(periodList);
						periodLineList.add(freeLineDO);
					}
				}
			}
			activeLineList.clear();
			activeLineList = null;

			cal = UserHelper.getCurrentCalendar();
			int maxMins = cal.getActualMaximum(Calendar.MINUTE)+1;
			int minMins = cal.getActualMinimum(Calendar.MINUTE);

			DateFormat df = DateFormat.getTimeInstance();
			
			for (FreeObjectDO freeLineDO : periodLineList) {

				int priority = cc.getPriority(actionId,freeLineDO.getObjectId());

				for(PeriodDO period: freeLineDO.getPeriods()) {
					Calendar end = UserHelper.getCurrentCalendar();
					cal.setTime(period.getStart());
					end.setTime(period.getEnd());
					int currentMins = cal.get(Calendar.MINUTE);
					while(cal.get(Calendar.HOUR_OF_DAY) <= end.get(Calendar.HOUR_OF_DAY)) {
						logger.debug("Regular check " + df.format(cal.getTime()) +" / " + df.format(end.getTime()));
						int hour = cal.get(Calendar.HOUR_OF_DAY);
						int lineId = freeLineDO.getObjectId();
						FreeSpaceDO space = new FreeSpaceDO();
						space.setDay(day);
						space.setObjectId(lineId);
						space.setDuration(freeLineDO.getDuration());
						space.setHour(hour);
						space.setCurrent(currentMins);
						space.setOfficeId(freeLineDO.getOfficeId());
						space.setPriority(priority);
						space.setObjectName(freeLineDO.getName());
						space.setObjectType(objectType);
						if(hour == end.get(Calendar.HOUR_OF_DAY)) {
							space.setEnd(end.get(Calendar.MINUTE));
						} else {
							space.setEnd(maxMins);
						}
						cal.add(Calendar.HOUR_OF_DAY, 1);
						logger.debug(space.toString());
						// followed logic to find the holes in the schedule.
						boolean hasCancelled = dao.getCancelledFreeSpaces(day, lineId, hour, fraudInterval, actualTimestamp);
						if(hasCancelled) {
							logger.debug("Cancelled space found in "+day +" " +lineId +" "+ hour+ " "+ fraudInterval);
							List<FreeSpaceDO> filCancelled = dao.getFreeSpaces(day, lineId, hour,fraudInterval, actualTimestamp);
							//							List<FreeSpaceDO> result = new ArrayList<FreeSpaceDO>();
							int endTime = currentMins;
							for (FreeSpaceDO fs : filCancelled) {
								if(endTime < fs.getCurrent() ) {
									FreeSpaceDO free = new FreeSpaceDO();
									free.setCurrent(endTime);
									free.setEnd(fs.getCurrent());
									FreeSpaceDO overlap = UserHelper.overlapPeriod(free,space);// && space.getEnd() >= free.getEnd() && space.getCurrent() <= free.getCurrent();
									if(free.getEnd() - free.getCurrent() >= freeLineDO.getDuration() && overlap != null ) {
										free.setCurrent(overlap.getCurrent());
										free.setEnd(overlap.getEnd());
										free.setDay(day);
										free.setObjectId(lineId);
										free.setDuration(freeLineDO.getDuration());
										free.setHour(hour);
										free.setOfficeId(freeLineDO.getOfficeId());
										free.setPriority(priority+1);
										free.setCancelled(true);
										free.setObjectName(freeLineDO.getName());
										free.setObjectType(objectType);
										list.add(free);
									}
								}
								endTime = fs.getEnd();
							}
							logger.info("Hole searching finished;");
						}
						list.add(space);
						currentMins = minMins;
					}
				}
			}

			logger.debug("Starting to actualize.");
			// filling the actual busy time from RECORD table.
			for (FreeSpaceDO space : list) {
				if(space.isCancelled())
					continue;
				HashMap<Integer, Integer> freeMins = dao.getCurrentFreeValues(day, space.getObjectId(),space.getHour(),space.getEnd(),fraudInterval, actualTimestamp);
				Integer current = freeMins.get(space.getHour()); 
				if(current != null) {
					int end1 = current;
					if(space.getCurrent() < end1)
						space.setCurrent(end1);
				}
				logger.debug(space.toString());
			}


			logger.debug("Starting to suit.");
			// determine if space is enough to record
			Calendar now = UserHelper.getCurrentCalendar();
			String currentDay = dateFormat.format(now.getTime());
			Calendar nowHour = UserHelper.getCurrentCalendar();
			nowHour.set(Calendar.MINUTE, 0);
			nowHour.set(Calendar.SECOND, 0);
			nowHour.set(Calendar.MILLISECOND, 0);

			for (FreeSpaceDO space : list) {
				int end = space.getEnd();
				int current = space.getCurrent();
				int duration = space.getDuration();

				String spaceDay = dateFormat.format(space.getDay());
				// Если проверяем запись на текущий день - отфильтровываем место для записи которое меньше текущего времени.
				if(spaceDay.equalsIgnoreCase(currentDay)) {
					Calendar cal1 = UserHelper.getCurrentCalendar();
					cal1.setTime(space.getDay());
					cal1.set(Calendar.HOUR_OF_DAY, space.getHour());
					cal1.set(Calendar.MINUTE, 0);
					cal1.set(Calendar.SECOND,0);
					cal1.set(Calendar.MILLISECOND, 0);//nowHour.getTime()
					if(cal1.compareTo(nowHour) == 0) {
						// если анализируется период текущего часа, то возьмём максимально доступное время начала записи.
						current = Math.max(space.getCurrent(), now.get(Calendar.MINUTE));
						space.setCurrent(current);
					} else if(cal1.compareTo(now) <= 0)
						continue;
				}

				if(current + duration <= end)
					suitableSpaces.add(space);
				if(logger.isDebugEnabled())
					logger.debug(space.toString());
			}

			list.clear();
			list = null;

			for (FreeSpaceDO space : suitableSpaces) {
				space.updateId();
				if(logger.isDebugEnabled())
					logger.debug(space.toString());
			}
			Collections.sort(suitableSpaces);
			if(security == Security.OPER)
				return suitableSpaces;
			
			int currentHour = -1;
			if(isCurrentDay() && !action.isMergeCurrentHour())
				currentHour = now.get(Calendar.HOUR_OF_DAY);
			suitableSpaces = mergeSpace(suitableSpaces,currentHour);
			logger.debug("Finished list.");

		} finally {
			dao.disconnect();
		}
		return suitableSpaces;
	}

	private int getFraudInterval(ActionDO action) {
		String intervalToPrevent = CacheContainer.getInstance().getConfig(Constants.INTERVAL_TO_PREVENT_EARLY_RESTORE);

		int fraudInterval = 20;
		if(intervalToPrevent != null)
			fraudInterval = Integer.valueOf(intervalToPrevent);
		fraudInterval = new Random().nextInt(fraudInterval);
		if(action.getMinimumRestore()!= null)
			fraudInterval += action.getMinimumRestore();
		return fraudInterval;
	}

	private List<FreeSpaceDO> mergeSpace(List<FreeSpaceDO> suitableSpaces, int currentHour) {
		List<FreeSpaceDO> resultSpaceList = new ArrayList<>();

		// merge all lines in one list. The order is not important, but the priority is.
		TreeMap<Integer,FreeSpaceDO> hashMap = new TreeMap<Integer, FreeSpaceDO>();
		for (FreeSpaceDO space : suitableSpaces) {
			int hour = space.getHour();
			if(hour == currentHour) {
				resultSpaceList.add(space);
				continue;
			}
			FreeSpaceDO spaceInHour = hashMap.get(hour);
			if(spaceInHour == null) {
				hashMap.put(space.getHour(), space);
			} else {
				//  Если у старой меньше приоритет или у новой записи меньше current, т.е. раньше
				if(spaceInHour.getPriority() < space.getPriority() || space.getCurrent() < spaceInHour.getCurrent())
					hashMap.put(space.getHour(), space);
			}
		}
		suitableSpaces.clear();
		Set<Integer> ref = hashMap.keySet();
		for (Integer integer : ref) {
			resultSpaceList.add(hashMap.get(integer));
		}
		hashMap.clear();
		return resultSpaceList;
	}

	private List<PeriodDO> filterWeekendPeriod(FreeObjectDO freeLineDO, Date day,Calendar cal,
			List<PeriodDO> periodList) {
		String weekends;
		try {
			weekends = dao.getWeekendHours(freeLineDO.getObjectId(),day);
			if(freeLineDO.getParentObjectId() > 0 && weekends == null)
				weekends = dao.getWeekendHours(freeLineDO.getParentObjectId(),day);
		} catch (DAOException e) {
			logger.error("Error while filtering weekend schedule", e);
			return null;
		}
		if(weekends == null) {
			return periodList;
		}
		List<PeriodDO> weekendList = UserHelper.parsePeriod(weekends, cal);
		weekendList = UserHelper.joinPeriod(weekendList);
		for(PeriodDO per: weekendList) {
			List<PeriodDO> p1 = new ArrayList<PeriodDO>();
			p1.add(per);
			List<PeriodDO> l1= UserHelper.invertPeriod(p1);
			periodList = UserHelper.overlapPeriods(periodList, l1);
		}
		return periodList;
	}

	private String getSchedule(FreeObjectDO freeLineDO, Date day, int actionId, int vehicleId,int customerId) {
		String period = null;
		if(Security.OPER == security) {
			period = getSchedule(freeLineDO, day, security, actionId, vehicleId, customerId);
		}
		if(period != null)
			return period;
		period = getSchedule(freeLineDO, day, Security.ALL, actionId, vehicleId, customerId);
		return period;
	}

	private String getSchedule(FreeObjectDO freeLineDO, Date day, Security security,int actionId, int vehicleId,int customerId) {
		String schedule;
		Calendar cal = UserHelper.getCurrentCalendar();
		cal.setTime(day);
		// due to difference in meaning in western countries (SUNDAY=1) we should modify dow value.

		//Date day,int lineId,int securityId,int dow,int actionId,int vehicleId,int customerId
		schedule = dao.getSchedule(day, freeLineDO.getObjectId(), security, actionId, vehicleId,customerId );
		
		
		if(schedule == null && freeLineDO.getParentObjectId() > 0)
			schedule = dao.getSchedule(day, freeLineDO.getParentObjectId(), security, actionId, vehicleId,customerId );
		//		if(schedule != null)
		return schedule;

		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, actionId, vehicleId,0 );
		if(schedule != null)
			return schedule;
		 */
		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, actionId, 0,customerId );
		if(schedule != null)
			return schedule;
		 */
		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, actionId, 0,0 );
		if(schedule != null)
			return schedule;
		 */
		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, 0, vehicleId,0 );
		if(schedule != null)
			return schedule;
		 */
		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, 0, 0,customerId );
		if(schedule != null)
			return schedule;
		 */
		/*		schedule = dao.getSchedule(day, freeLineDO.getLineId(), security, 0, 0, 0 );
		if(schedule != null)
			return schedule;
		 */
		//	return null;
	}

	private int getDuration(FreeObjectDO freeLineDO,Date day, int actionId,int criteriaId, int lineType,int customerId) {
		int duration = 0;
		int objectId = freeLineDO.getObjectId();
		duration = dao.getDuration(day, actionId, criteriaId, objectId,customerId,lineType,freeLineDO.getOfficeId());
		if(duration == 0 && freeLineDO.getParentObjectId() > 0)
			duration = dao.getDuration(day, actionId, criteriaId, freeLineDO.getParentObjectId(),customerId,lineType,freeLineDO.getOfficeId());
		return duration;
	}

	public boolean isCurrentDay() {
		return currentDay;
	}

	public void setCurrentDay(boolean currentDay) {
		this.currentDay = currentDay;
	}
}
